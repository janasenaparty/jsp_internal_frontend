import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewpasswordRoutingModule } from './newpassword-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewpasswordComponent } from './newpasswordmodule/newpassword.component';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [NewpasswordComponent],
  imports: [
    CommonModule,
    NewpasswordRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ]
})
export class NewpasswordModule { }
