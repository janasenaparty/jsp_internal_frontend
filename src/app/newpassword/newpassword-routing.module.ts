import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewpasswordComponent } from './newpasswordmodule/newpassword.component';

const routes: Routes = [
  {
    path: '',
    component: NewpasswordComponent,
    data: { title: "Newpassword" }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewpasswordRoutingModule { }
