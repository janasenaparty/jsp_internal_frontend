import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/Services/auth.service';  



@Component({
  selector: 'app-newpassword',
  templateUrl: './newpassword.component.html',
  styleUrls: ['./newpassword.component.css']
})
export class NewpasswordComponent implements OnInit {

 
  
  
  headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

  // errorMsg: string='';
  // newError: boolean=false
  // newcError: boolean=false
  submitted = false;


  constructor(private FB:FormBuilder, private http:HttpClient , private router:Router , private service:AuthService) { }


  newpassform = this.FB.group(
  {
    newpassuser:  ['',[Validators.required, Validators.minLength(6), Validators.maxLength(16)]],
    confirmPassword: ['', Validators.required,],
    // acceptTerms: [false, Validators.requiredTrue]
    
  },
  {
    // validator: MustMatch('password', 'confirmPassword')
  }
  
  );
  
  ngOnInit(): void {
  }

  get f(): { [key: string]: AbstractControl } {
    return this.newpassform.controls;
  }

  onSubmit(): void {
    this.submitted = true;

    if (this.newpassform.invalid) {
      return;
    }

    // console.log(JSON.stringify(this.loginform.value, null, 2));
  }
  
  isValidnewpass() {
  
  if (this.newpassform.valid) {

  const body =`password=${this.newpassform.value.newpassuser}
  &business_id=101
  &token=c2VsbHV0ZXNlY3JldA`
  
  // this.service.newpass_form(body).subscribe((res:any)=>{

  // console.log(this.newpassform.value);

  // if (res) {
  // console.log(res)
  // this.router.navigate(['/industry'])
  // }
  // })

  } else{
    // console.log('invalid credentials');
    // this.newpassform.controls.newpassuser.status==="INVALID"?this.newError=true:this.newError=false
    // this.newpassform.controls.newpassuser.status==="INVALID"?this.newcError=true:this.newcError=false

  }
  };
  
  
}