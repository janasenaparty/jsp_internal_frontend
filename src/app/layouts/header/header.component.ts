import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/Services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isValidUser = false;
  constructor(private toastr:ToastrService, private router:Router,private service:AuthService ) { }
   userName : any;
   ngOnInit(): void {
    // this.isValidUser = localStorage.getItem("isValidUser") ? true : false
      if( localStorage.getItem('isName') ){
         this.userName = localStorage.getItem('isName');
      }
  }
  // logOut() {
  //   // this.isValidUser = false
  //   // window.localStorage.clear()
  //   // this.router.navigate(['login'])
  // }

  logOut() {

    
    this.service.isLoggedOut().subscribe((res:any)=>{

   if(!res.serviceError){
     
     this.toastr.success(res.serviceResult)
    console.log(res)
    window.localStorage.clear()
    this.router.navigate(['/login'])

    localStorage.setItem('isValidUser', "false")
  
     this.isValidUser = false;
   }
      

    
    
  
    })
  // }
  };


}
