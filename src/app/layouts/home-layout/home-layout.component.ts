import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { AuthService } from 'src/app/Services/auth.service';

@Component({
  selector: 'app-home-layout',
  templateUrl: './home-layout.component.html',
  styleUrls: ['./home-layout.component.css']
})
export class HomeLayoutComponent implements OnInit {
  roleId:any
  constructor(private router: Router,private authservice: AuthService) { }

  ngOnInit() {
    this.roleId = this.authservice.userRoleId.subscribe(data=>{
      return data
    });

  }

}
