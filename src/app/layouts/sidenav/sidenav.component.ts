import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from 'src/app/Services/auth.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  isRole:any;
  @Input() roleId:any;
  role ='AD';
  mySidenav:any;
  constructor(private authservice: AuthService) {this.isRole = this.authservice.getRoleId; }
 get isRoleId(){
 return this.authservice.getRoleId;
 }
  ngOnInit(): void {
    console.log("ROle",this.authservice.getRoleId)
    if( localStorage.getItem('isRole') ){
      this.isRole = localStorage.getItem('isRole');
      console.log(this.isRole);
     
   }
  }

  // openNav() {
  //   document.getElementById("mySidenav").style.width = "250px";
  //   document.getElementById("main").style.marginLeft = "250px";
  // }
  // closeNav(){
  //   document.getElementById("mySidenav").style.width = "0";
  //   document.getElementById("main").style.marginLeft= "0";
  // }
}
