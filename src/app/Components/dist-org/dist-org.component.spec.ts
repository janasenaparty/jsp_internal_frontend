import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DistOrgComponent } from './dist-org.component';

describe('DistOrgComponent', () => {
  let component: DistOrgComponent;
  let fixture: ComponentFixture<DistOrgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DistOrgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DistOrgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
