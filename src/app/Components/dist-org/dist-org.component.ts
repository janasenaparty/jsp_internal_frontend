import { Breadcrumb } from './../../model/breadcrumb.model';
import { BreadcrumbService } from './../../Services/breadcrumb.service';

import { Component, OnInit } from '@angular/core';
import * as d4 from 'd3';
import { Observable, combineLatest, map, of, startWith } from 'rxjs';
import { FormControl } from '@angular/forms';
import  distdata from './transformed_eg_data.json';
// import * as data from './transformed_state_data.json';
import data  from '../janasena-org-data/transformed_state_data.json';
// import dropDownData from './villageGramPanchayatMapping.json';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-dist-org',
  templateUrl: './dist-org.component.html',
  styleUrls: ['./dist-org.component.css']
})
export class DistOrgComponent implements OnInit {
  data3: any;
  constructor(private readonly http: HttpClient, private router: Router, private readonly breadcrumbService: BreadcrumbService) { }

  ngOnInit(): void {
    this.data3 = this.getFlattenedData(distdata);
  }
  getFlattenedData(dataHierarchy:any) {
    const descendants = d4.hierarchy(dataHierarchy).descendants();

    descendants.forEach((d:any, i) => {
      d.id = d.id || 'id' + i;
      d.data.id = d.id || 'id' + i;
    });

    return descendants
      .map((d, i) => [d.parent?.data?.id, d.data])
      .map(([parentId, data]) => {
        const copy = { ...data };
        delete copy.children;
        return { ...copy, ...{ parentId } };
      });
  }
}
