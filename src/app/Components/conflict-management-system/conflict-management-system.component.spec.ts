import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConflictManagementSystemComponent } from './conflict-management-system.component';

describe('ConflictManagementSystemComponent', () => {
  let component: ConflictManagementSystemComponent;
  let fixture: ComponentFixture<ConflictManagementSystemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConflictManagementSystemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConflictManagementSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
