import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TgSummaryReportsComponent } from './tg-summary-reports.component';

describe('TgSummaryReportsComponent', () => {
  let component: TgSummaryReportsComponent;
  let fixture: ComponentFixture<TgSummaryReportsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TgSummaryReportsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TgSummaryReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
