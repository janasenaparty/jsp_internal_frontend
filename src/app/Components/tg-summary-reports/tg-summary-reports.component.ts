import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-tg-summary-reports',
  templateUrl: './tg-summary-reports.component.html',
  styleUrls: ['./tg-summary-reports.component.css']
})
export class TgSummaryReportsComponent implements OnInit {

  summary_counts_url='/api/portal/admin/kriya/summary_counts';

  export_url='/api/portal/admin/send_telangana_daily_report';
  volunteerReportId:any;
  memberReportId:any;
  renewalReportId:any;
  // reportName:any;
  reportName1:any;
  reportName2:any;
  reportName3:any;
  constructor(private toastr:ToastrService,  private http:HttpClient) { }

  ngOnInit(): void {
    this.volunteerReportId =23;
    this.memberReportId =19;
    this.renewalReportId =22;
    this.reportName1 ="TG Volunteer Summary Report ";
    this.reportName2 ="TG Members Summary Report ";
    this.reportName3 ="TG Renewal Summary Report ";

   

  }

  exportData(){
    let url = this.export_url;
    return this.http.get(url)
    .subscribe((res:any) => {
    if(res){
        this.toastr.success(res.serviceResult.message,'Export Success',{timeOut: 5000, })
      } 
  })
  }
}
