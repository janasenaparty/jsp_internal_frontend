import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KVReferenceComponent } from './kv-reference.component';

describe('KVReferenceComponent', () => {
  let component: KVReferenceComponent;
  let fixture: ComponentFixture<KVReferenceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KVReferenceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KVReferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
