import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmandateDonationsComponent } from './emandate-donations.component';

describe('EmandateDonationsComponent', () => {
  let component: EmandateDonationsComponent;
  let fixture: ComponentFixture<EmandateDonationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmandateDonationsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EmandateDonationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
