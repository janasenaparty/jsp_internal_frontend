import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NRIDonationsComponent } from './nri-donations.component';

describe('NRIDonationsComponent', () => {
  let component: NRIDonationsComponent;
  let fixture: ComponentFixture<NRIDonationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NRIDonationsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NRIDonationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
