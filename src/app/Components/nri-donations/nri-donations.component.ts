import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nri-donations',
  templateUrl: './nri-donations.component.html',
  styleUrls: ['./nri-donations.component.css']
})
export class NRIDonationsComponent implements OnInit {
  nri_donations_counts_url='/api/portal/admin/getNRIDonationsCounts';
  
  reportId:any;
  reportName:any;
  todayDonation:any;
  totalDonation:any;
  NriTodayDonation:any;
  NriTotalDonation:any;
  constructor(private http:HttpClient) { }

  ngOnInit(): void {
    this.reportId =40;
    this.reportName =" NRI Donation Dashboard";
    this.getnridonationsCount();
  }
  getnridonationsCount(){
    let url = this.nri_donations_counts_url;
    return this.http.get(url)
    .subscribe((res: any) => {
      if (res) { 
         
         this.NriTodayDonation  =  res.serviceResult.today_amount;
          this.NriTotalDonation = res.serviceResult.total_amount;
       
       
         
      }

  })
  }

}
