import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-voters-data',
  templateUrl: './voters-data.component.html',
  styleUrls: ['./voters-data.component.css']
})
export class VotersDataComponent implements OnInit {

  reportId:any;
  reportName:any;
  constructor() { }

  ngOnInit(): void {
    this.reportId =25;
    this.reportName ="Voters Data";
 
  }

}
