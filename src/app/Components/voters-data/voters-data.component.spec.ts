import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VotersDataComponent } from './voters-data.component';

describe('VotersDataComponent', () => {
  let component: VotersDataComponent;
  let fixture: ComponentFixture<VotersDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VotersDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VotersDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
