import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhaseThreeMembershipComponent } from './phase-three-membership.component';

describe('PhaseThreeMembershipComponent', () => {
  let component: PhaseThreeMembershipComponent;
  let fixture: ComponentFixture<PhaseThreeMembershipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhaseThreeMembershipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhaseThreeMembershipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
