import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-phase-three-membership',
  templateUrl: './phase-three-membership.component.html',
  styleUrls: ['./phase-three-membership.component.css']
})
export class PhaseThreeMembershipComponent implements OnInit {
  reportId:any;
  reportName:any;
  constructor() { }

  ngOnInit(): void {
    this.reportId =32;
    this.reportName ="Kriya Memberships Phase-3 ";
  }

}
