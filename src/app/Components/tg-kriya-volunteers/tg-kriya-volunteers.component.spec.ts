import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TgKriyaVolunteersComponent } from './tg-kriya-volunteers.component';

describe('TgKriyaVolunteersComponent', () => {
  let component: TgKriyaVolunteersComponent;
  let fixture: ComponentFixture<TgKriyaVolunteersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TgKriyaVolunteersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TgKriyaVolunteersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
