import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KriyaInsuranceFormComponent } from './kriya-insurance-form.component';

describe('KriyaInsuranceFormComponent', () => {
  let component: KriyaInsuranceFormComponent;
  let fixture: ComponentFixture<KriyaInsuranceFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KriyaInsuranceFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KriyaInsuranceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
