import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { formatDate } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { TabularreportService } from 'src/app/Services/tabularreport.service';
interface JsonFormValidators {
  min?: number;
  max?: number;
  required?: boolean;
  requiredTrue?: boolean;
  email?: boolean;
  minLength?: boolean;
  maxLength?: boolean;
  pattern?: string;
  nullValidator?: boolean;
}
interface JsonFormControlOptions {
  min?: string;
  max?: string;
  step?: string;
  icon?: string;
}
interface JsonFormControls {
  name: string;
  label: string;
  value: string;
  type: string;
  options?: JsonFormControlOptions;
  required: boolean;
  validators: JsonFormValidators;
}
export interface JsonFormData {
  controls: JsonFormControls[];
}

@Component({
  selector: 'app-kriya-insurance-form',
  templateUrl: './kriya-insurance-form.component.html',
  styleUrls: ['./kriya-insurance-form.component.css'],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class KriyaInsuranceFormComponent implements OnInit {
  @Input() jsonFormData: any;
  selectedValue:string ='';
  selectedType: string = 'mobile';
  InsurenceDetails:any;
  kifId:number;
  constructor(
    private FB:FormBuilder,
     private router:Router,
      private toastr:ToastrService,
      private tabularService: TabularreportService,
      private route: ActivatedRoute){
  this.kifId =Number(this.route.snapshot.paramMap.get('id'));
  this.tabularService.getInsurencDetailsById(this.kifId).subscribe((data)=>{
    this.InsurenceDetails = data.serviceResult;
     
    this.editinsuranceform.patchValue( data.serviceResult);
    this.editinsuranceform.patchValue({
      intimation_date:formatDate(this.InsurenceDetails.intimation_date,'yyyy-MM-dd','en'),
      accident_date:formatDate(this.InsurenceDetails.accident_date,'yyyy-MM-dd','en'),
    })
  })

  }
  editinsuranceform = this.FB.group(
    {
      accident_date:  [0, Validators.required],
      intimation_date:  [0, Validators.required],
      remarks:  ['', Validators.required],
      claim_status:  ['', Validators.required]
    });
  insuranceform = this.FB.group(
    {
     id: ['', Validators.required],
      kriya_member_id:  ['', Validators.required],
      jsp_id:  ['', Validators.required],
      name:  ['', Validators.required],
      mobile:  ['', Validators.required],
      aadhar_number:  ['', Validators.required],
      nominee_name:  ['', Validators.required],
      added_by:  ['', Validators.required],
      poc_name:  ['', Validators.required],
      poc_mobile:  ['', Validators.required],
      // category:  ['', Validators.required],
      // email:  [''],
      poc_relation:  ['', Validators.required],
      // gender:  ['', Validators.required],
      accident_date:  [0, Validators.required],
      intimation_date:  [0, Validators.required],
      district_name:  [0, Validators.required],
      constituency_name:  [0, Validators.required],
      // mandal:  [0, Validators.required],
      // voter_id:  ['', Validators.required],
      // state_id:  ['', Validators.required],
     
      // petition_files:  ['', Validators.required],
      remarks:  [''],
      // address:  ['', Validators.required],
      // village:  ['']
     

    }
  );
  ngOnInit(): void {
  }
 
  unsubcribe: any

  // public fields: any[] = [
  
  //   {
  //     type: 'text',
  //     name: 'kriya_member_id',
  //     label: 'JSP ID',
  //     value: '',
  //     required: true,
  //   },
  //   {
  //     type: 'text',
  //     name: 'poc_name',
  //     label: 'Point of contact Name',
  //     value: '',
  //     required: true,
  //   },
  //   {
  //     type: 'text',
  //     name: 'poc_mobile',
  //     label: 'Point of contact Number',
  //     value: '',
  //     required: true,
  //   },
  //   {
  //     type: 'text',
  //     name: 'poc_relation',
  //     label: 'Point of contact Relation',
  //     value: '',
  //     required: true,
  //   },
  //   {
  //     type: 'date',
  //     name: 'accident_date',
  //     label: 'Date Of Accident',
  //     value: '',
  //     required: true,
  //   },
  //   {
  //     type: 'date',
  //     name: 'intimation_date',
  //     label: 'Date Of Intimation',
  //     value: '',
  //     required: true,
  //   },
  //   {
  //     type: 'text',
  //     name: 'remarks',
  //     multiline: true,
  //     label: 'Accident Description',
  //     placeholder:"Accident Description",
  //     value: '',
  //     required: true,
  //   },

  //   // not there in api
  //     // {
  //   //   type: 'text',
  //   //   name: 'name',
  //   //   label: 'Name',
  //   //   value: '',
  //   //   required: true,
  //   // },
  //   // {
  //   //   type: 'text',
  //   //   name: 'mobile_number',
  //   //   label: 'Mobile Number',
  //   //   value: '',
  //   //   required: true,
  //   // },

  //  /* {
  //     type: 'file',
  //     name: 'picture',
  //     label: 'Picture',
  //     required: true,
  //     onUpload: this.onUpload.bind(this)
  //   }, */
  //   // {
  //   //   type: 'dropdown',
  //   //   name: 'assembly',
  //   //   label: 'Assembly',
  //   //   value: 'in',
  //   //   required: true,
  //   //   options: [
  //   //     { key: 'in', label: 'India' },
  //   //     { key: 'us', label: 'USA' }
  //   //   ]
  //   // },
  //   //  {
  //   //   type: 'dropdown',
  //   //   name: 'district',
  //   //   label: 'District',
  //   //   value: 'in',
  //   //   required: true,
  //   //   options: [
  //   //     { key: 'in', label: 'India' },
  //   //     { key: 'us', label: 'USA' }
  //   //   ]
  //   // },
  //   // {
  //   //   type: 'text',
  //   //   name: 'nominee_name',
  //   //   label: 'Nominee Name',
  //   //   value: '',
  //   //   required: true,
  //   // },
  //   // {
  //   //   type: 'text',
  //   //   name: 'Volunteer_name',
  //   //   label: 'Volunteer Name',
  //   //   value: '',
  //   //   required: true,
  //   // },
    
   
  //   // {
  //   //   type: 'text',
  //   //   name: 'place_of_accident',
  //   //   label: 'Place of Accident',
  //   //   value: '',
  //   //   required: true,
  //   // },
    
  //   // {
  //   //   type: 'text',
  //   //   name: 'policy_number',
  //   //   label: 'Policy Number',
  //   //   value: '',
  //   //   required: true,
  //   // },
  //   // {
  //   //   type: 'text',
  //   //   name: 'policy_period',
  //   //   label: 'Policy period',
  //   //   value: '',
  //   //   required: true,
  //   // },
  //  /* {
  //     type: 'radio',
  //     name: 'country',
  //     label: 'Country',
  //     value: 'in',
  //     required: true,
  //     options: [
  //       { key: 'm', label: 'Male' },
  //       { key: 'f', label: 'Female' }
  //     ]
  //   }, 
  //   {
  //     type: 'checkbox',
  //     name: 'hobby',
  //     label: 'Hobby',
  //     required: true,
  //     options: [
  //       { key: 'f', label: 'Fishing' },
  //       { key: 'c', label: 'Cooking' }
  //     ]
  //   } */
  // ];
 
  submitInsurenceForm(event:any){
   this.tabularService.addInsurenceForm(event).subscribe((res)=>{
    if(res.serviceResult){
      this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-center'  });
        this.router.navigate(['/kriya_insurance_dashboard']);
    }else {
      this.toastr.error(res.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center'  })
    }
   })
  }

  // viewInsurance(){
  //   let rowData = this.params;
  //   // alert("hey view button works ")
 
 
  //     this.http.get(`/api/portal/claims/get_claim_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
  //        this.trservice.insuranceData.next(res.serviceResult)
  //       this.insuranceData=res.serviceResult
  //       document.getElementById('previewInsuranceData')?.click()
  //   })
    
    
  // }
  onChangeGetDetails(){
    this.searchInsurenceDetailsById(this.selectedType,this.selectedValue)
  }
  searchInsurenceDetailsById(id: string,value: string){
    this.tabularService.getInsDetailsById(id,value).subscribe((res)=>{
      // this.insuranceform.patchValue(
      //   {
      //     kriya_member_id:res.serviceResult.kriya_member_id,
      //     poc_mobile:res.serviceResult.poc_mobile,
      //     mobile:res.serviceResult.mobile,
      //     mobile:res.serviceResult.mobile
        // });
      this.insuranceform.patchValue(res.serviceResult);
    });
  }
  updateInsurenceForm(event: any){
    this.tabularService.updateInsurance(event,this.InsurenceDetails.id).subscribe((res)=>{
      if(res.serviceResult){
        this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-center'  });
          this.router.navigate(['/kriya_insurance_dashboard']);
      }else {
        this.toastr.error(res.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center'  })
      }
    })
  }
  // onUpload(e:any) {
  //   console.log(e);

  // }

  // getFields() {
  //   return this.fields;
  // }

  ngDistroy() {
    
  }
}
