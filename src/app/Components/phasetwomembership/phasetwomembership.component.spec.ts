import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhasetwomembershipComponent } from './phasetwomembership.component';

describe('PhasetwomembershipComponent', () => {
  let component: PhasetwomembershipComponent;
  let fixture: ComponentFixture<PhasetwomembershipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhasetwomembershipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhasetwomembershipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
