
import { HttpClient } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { SurveyCreatorModel } from "survey-creator-core";
// import { JsonFormComponent } from '../../components/json-form/json-form.component';
import { JsonFormComponent, JsonFormData } from '../json-form/json-form.component';
const creatorOptions = {
  showLogicTab: true,
  isAutoSave: true
};

const defaultJson = {
  pages: [{
    name: "Name",
    elements: [{
      name: "FirstName",
      title: "Enter your first name:",
      type: "text"
    }, {
      name: "LastName",
      title: "Enter your last name:",
      type: "text"
    }]
  }]
};

@Component({
  selector: 'survey-creator-component',
  templateUrl: './survey-creator.component.html',
  styleUrls: ['./survey-creator.component.css']
})
export class SurveyCreatorComponent implements OnInit {
  surveyCreatorModel:any;
  formData: any;
  // surveyCreatorModel: SurveyCreatorModel;
  constructor(private http: HttpClient) {}
  ngOnInit() {

    this.http
    .get('/assets/my-form.json')
    .subscribe((formData: any) => {
      this.formData = formData;
    });
    const creator = new SurveyCreatorModel(creatorOptions);
    this.surveyCreatorModel = creator;
    creator.text = window.localStorage.getItem("survey-json") || JSON.stringify(defaultJson);

    creator.saveSurveyFunc = (saveNo: number, callback: Function) => { 
      window.localStorage.setItem("survey-json", creator.text);
      callback(saveNo, true);
      // saveSurveyJson(
      //     "https://your-web-service.com/",
      //     creator.JSON,
      //     saveNo,
      //     callback
      // );
    };
    this.surveyCreatorModel = creator;
  }
}


function saveSurveyJson(arg0: string, JSON: any, saveNo: number, callback: Function) {
  throw new Error("Function not implemented.");
}
// function saveSurveyJson(url: string | URL, json: object, saveNo: number, callback: Function) {
//   fetch(url, {
//     method: 'POST',
//     headers: {
//       'Content-Type': 'application/json;charset=UTF-8'
//     },
//     body: JSON.stringify(json)
//   })
//   .then(response => {
//     if (response.ok) {
//       callback(saveNo, true);
//     } else {
//       callback(saveNo, false);
//     }
//   })
//   .catch(error => {
//     callback(saveNo, false);
//   });
// }


































// import { Component, OnInit } from "@angular/core";
// import { Model, NumericValidator } from "survey-core";
// import { SurveyCreatorModel } from "survey-creator-core";
// import './components/appStyles.css';
// // import styles from './components/appStyles.module.css';

// // import { json } from "../../../../src/assets/json";
// import "./survey.component.css";
// import "survey-core/defaultV2.min.css";
// // import { themeJson } from "../../../../src/assets/theme";
// const creatorOptions = {
//   showLogicTab: true,
//   isAutoSave: true
// };
// const surveyJson = {
//   "elements": [{
//     "name": "question1",
//     "type": "text",
//     "isRequired": true,
//     "requiredErrorText": "Value cannot be empty"
//   }]
// }
// // const survey = new Model(surveyJson);
// // const question = survey.getQuestionByName("question1")
// // question.validators.push(new NumericValidator({ text: "Value must be a number" }));
// // const defaultJson = {
// //   pages: [{
// //     name: "Name",
// //     elements: [{
// //       name: "FirstName",
// //       title: "Enter your first name:",
// //       type: "text"
// //     }, {
// //       name: "LastName",
// //       title: "Enter your last name:",
// //       type: "text"
// //     }]
// //   }]
// // };

// @Component({
//   selector: 'survey-creator-component',
//   templateUrl: './survey-creator.component.html',
//   styleUrls: ['./survey-creator.component.css']
// })
// export class SurveyCreatorComponent implements OnInit {
//   surveyCreatorModel!: SurveyCreatorModel;
//   model: any;
//   ngOnInit() {

//     // const survey = new Model(json);
//     // You can delete the line below if you do not use a customized theme
//     // survey.applyTheme(themeJson);
//     // survey.onComplete.add((sender, options) => {
//         // console.log(JSON.stringify(sender.data, null, 3));
//     // });
//     // this.model = survey;
//     // const creator = new SurveyCreatorModel(creatorOptions);
//     // creator.text = window.localStorage.getItem("survey-json") || JSON.stringify(defaultJson);

//     // creator.saveSurveyFunc = (saveNo: number, callback: Function) => { 
//     //   window.localStorage.setItem("survey-json", creator.text);
//     //   callback(saveNo, true);
//     //   saveSurveyJson(
//     //       "https://your-web-service.com/",
//     //       creator.JSON,
//     //       saveNo,
//     //       callback
//     //   );
//     // };
//     // this.surveyCreatorModel = creator;
//   }
// }
// // function saveSurveyJson(arg0: string, JSON: any, saveNo: number, callback: Function) {
// //   throw new Error("Function not implemented.");
// // }

