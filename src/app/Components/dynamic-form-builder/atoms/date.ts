import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

// text,email,tel,textarea,password, 
@Component({
    selector: 'datefield',
    template: `
      <div [formGroup]="form">
        <input  *ngIf="!field.multiline" [class.is-invalid]="isDirty && !isValid" type="date"
         [formControlName]="field.name" [id]="field.name"  class="form-control" [placeholder]="field.placeholder" /> 
      </div> 
    `
})
export class DateFieldComponent {
    @Input() field:any = {};
    @Input() form!:FormGroup;
    get isValid() { return this.form.controls[this.field.name].valid; }
    get isDirty() { return this.form.controls[this.field.name].dirty; }
  
    constructor() {

    }
}