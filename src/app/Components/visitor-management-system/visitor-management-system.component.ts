import { Component, OnInit } from '@angular/core';
import { TabularreportService } from 'src/app/Services/tabularreport.service';

@Component({
  selector: 'app-visitor-management-system',
  templateUrl: './visitor-management-system.component.html',
  styleUrls: ['./visitor-management-system.component.css']
})
export class VisitorManagementSystemComponent implements OnInit {
  isRole:any;
  reportId:any;
  reportName:any;
  constructor(private trservice:TabularreportService ) { 
  
  }

  ngOnInit(): void {
    this.reportId =29;
      this.reportName ="Visitors Management System ";
 
      if( localStorage.getItem('isRole') ){
        this.isRole = localStorage.getItem('isRole');
        console.log(this.isRole)
       
       
     }
  }

}
