import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitorManagementSystemComponent } from './visitor-management-system.component';

describe('VisitorManagementSystemComponent', () => {
  let component: VisitorManagementSystemComponent;
  let fixture: ComponentFixture<VisitorManagementSystemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisitorManagementSystemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitorManagementSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
