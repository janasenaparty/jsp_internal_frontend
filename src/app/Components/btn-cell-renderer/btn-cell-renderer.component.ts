import { Component, OnDestroy } from "@angular/core";

import { ICellRendererAngularComp } from "@ag-grid-community/angular";
import { ICellRendererParams } from "@ag-grid-community/core";

@Component({
  selector: "btn-cell-renderer",
  template: `
  `
})

// <div *ngIf="params?.data.status=='Active'" class="mt-5 submit-btn text-end">
// <h2 style="color:green">Accepted</h2>
// </div>
// <div   *ngIf="params?.data.status=='Suspended'" class="mt-5 submit-btn text-end">
// <h5 style="color:rgb(248, 171, 27)" >Rejected</h5>
// </div>
//    <button (click)="btnClickedHandler($event)">Click me!</button>
export class BtnCellRenderer implements ICellRendererAngularComp, OnDestroy 
{
  refresh(params: ICellRendererParams): boolean {
    throw new Error("Method not implemented.");
  }
  private params: any;
   private data: any;


  // agInit(params: any): void {
  //   this.params = params;
  // }
  agInit(params: { value: any; }) {
    this.params = params;
    this.data = params.value;
  }

  btnClickedHandler(e:any) {
    this.params.clicked(this.params.value);
  }

  ngOnDestroy() {
    // no need to remove the button click handler
    // https://stackoverflow.com/questions/49083993/does-angular-automatically-remove-template-event-listeners
  }
}



