import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-map-upi-transactions',
  templateUrl: './map-upi-transactions.component.html',
  styleUrls: ['./map-upi-transactions.component.css']
})
export class MapUpiTransactionsComponent implements OnInit {
  // @Input("hasRole") hasRole =true;
  SERVER_URL = "/api/portal/admin/map_kriya_member_payment_status";
  uploadForm:any=FormGroup
  document: any;
  isRole: any;

  ngOnInit() {
    this.uploadForm = this.formBuilder.group({
      profile: ['']
    });

    
  }
  constructor(private toastr:ToastrService, private formBuilder: FormBuilder, private httpClient: HttpClient) { }
  
 onFileSelect(event:any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadForm.get('profile').setValue(file);
    }
  }
  onSubmit() {
    // debugger;
    const formData = new FormData();
    formData.append('file', this.uploadForm.get('profile').value);

    this.httpClient.post<any>(this.SERVER_URL,formData).subscribe(
      
      // (res) => alert(res.serviceResult),
      (res) => this.toastr.success('res.serviceResult','Message',{timeOut: 4000, }),
      (err) => alert(err),
 
    
    );
  }

}
