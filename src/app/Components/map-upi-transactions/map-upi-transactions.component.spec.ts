import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapUpiTransactionsComponent } from './map-upi-transactions.component';

describe('MapUpiTransactionsComponent', () => {
  let component: MapUpiTransactionsComponent;
  let fixture: ComponentFixture<MapUpiTransactionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapUpiTransactionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapUpiTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
