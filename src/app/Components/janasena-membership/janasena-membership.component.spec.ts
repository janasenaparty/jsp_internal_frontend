import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JanasenaMembershipComponent } from './janasena-membership.component';

describe('JanasenaMembershipComponent', () => {
  let component: JanasenaMembershipComponent;
  let fixture: ComponentFixture<JanasenaMembershipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JanasenaMembershipComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JanasenaMembershipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
