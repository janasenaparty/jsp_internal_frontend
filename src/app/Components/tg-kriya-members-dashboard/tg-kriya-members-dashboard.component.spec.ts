import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TgKriyaMembersDashboardComponent } from './tg-kriya-members-dashboard.component';

describe('TgKriyaMembersDashboardComponent', () => {
  let component: TgKriyaMembersDashboardComponent;
  let fixture: ComponentFixture<TgKriyaMembersDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TgKriyaMembersDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TgKriyaMembersDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
