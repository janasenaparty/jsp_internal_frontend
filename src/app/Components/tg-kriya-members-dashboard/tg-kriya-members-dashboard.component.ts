import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { TabularreportService } from 'src/app/Services/tabularreport.service';

@Component({
  selector: 'app-tg-kriya-members-dashboard',
  templateUrl: './tg-kriya-members-dashboard.component.html',
  styleUrls: ['./tg-kriya-members-dashboard.component.css']
})
export class TgKriyaMembersDashboardComponent implements OnInit {
  summary_counts_url='/api/portal/admin/kriya/summary_counts_telangana';
  grandTotalRecords:any;
  overAllRecords:any;
  phaseTwoRecords:any;
  renewalRecords:any;
  reportId:any;
  reportName:any;
  phasethreetotal:any;
  constructor(private toastr:ToastrService,private trservice: TabularreportService , private http:HttpClient) { }
  // showSuccess() {
  //   this.toastr.success('Hello world!', 'Toastr fun!');
  // }
  ngOnInit(): void {
    this.reportId =17;
    this.reportName ="TG Kriya Members Dashboard";
    // this.subscription = source.subscribe(val => this.GetSummaryCount());

    this.getSummaryCount()

 
  }
  getSummaryCount(){
    let url = this.summary_counts_url;
    return this.http.get(url)
    .subscribe((res: any) => {
      if (res) { 
          this.overAllRecords = res.serviceResult.overall;
          console.log(this.overAllRecords);
          this.phaseTwoRecords = res.serviceResult.phase2;
          this.renewalRecords = res.serviceResult.renewal;
          this.grandTotalRecords  = (res.serviceResult.phase2.payment_verification_pending+res.serviceResult.phase2.verfication_completed+ res.serviceResult.renewal.total)
         this.phasethreetotal = ( res.serviceResult.phase2.verfication_completed+res.serviceResult.phase2.payment_verification_pending)
       
         
      }

  })
  }

}
