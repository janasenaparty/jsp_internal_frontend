import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-qrcode',
  templateUrl: './qrcode.component.html',
  styleUrls: ['./qrcode.component.css']
})
export class QrcodeComponent implements OnInit {
  scanResult:any;
  SERVER_URL = "/api/portal/admin/get_qrcode_members";
  uploadForm:any=FormGroup
  document: any;
  ngOnInit() {
    this.uploadForm = this.formBuilder.group({
      profile: ['']
    });
  }
  constructor(private formBuilder: FormBuilder, private httpClient: HttpClient) { }
  
 onFileSelect(event:any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadForm.get('profile').setValue(file);
    }
  }
  onSubmit() {
    debugger;
    const formData = new FormData();
    formData.append('file', this.uploadForm.get('profile').value);

    this.httpClient.post<any>(this.SERVER_URL, formData).subscribe(
      
      (res) => this.scanResult=res.serviceResult.url,
     
      (err) => alert(err),
 
    
    );
  }

}
