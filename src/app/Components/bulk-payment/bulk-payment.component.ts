import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bulk-payment',
  templateUrl: './bulk-payment.component.html',
  styleUrls: ['./bulk-payment.component.css']
})
export class BulkPaymentComponent implements OnInit {

  reportId:any;
  reportName:any;
  constructor() { }

  ngOnInit(): void {
    this.reportId =35;
    this.reportName ="Kriya Bulk Payments ";
  }

}
