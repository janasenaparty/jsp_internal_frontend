import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KmVotersMappingComponent } from './km-voters-mapping.component';

describe('KmVotersMappingComponent', () => {
  let component: KmVotersMappingComponent;
  let fixture: ComponentFixture<KmVotersMappingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KmVotersMappingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KmVotersMappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
