import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-summary-reports',
  templateUrl: './summary-reports.component.html',
  styleUrls: ['./summary-reports.component.css']
})
export class SummaryReportsComponent implements OnInit {
  summary_counts_url='/api/portal/admin/kriya/summary_counts';

  export_url='/api/portal/admin/send_daily_report';
  volunteerReportId:any;
  memberReportId:any;
  renewalReportId:any;
  topfiveReportId :any;
  // reportName:any;
  reportName1:any;
  reportName2:any;
  reportName3:any;
  reportName4:any;

  constructor(private toastr:ToastrService,  private http:HttpClient) { }

  ngOnInit(): void {
    this.volunteerReportId =16;
    this.memberReportId =14;
    this.renewalReportId =15;
    this.topfiveReportId = 36;
    this.reportName1 ="Volunteer Summary Report ";
    this.reportName2 ="Members Summary Report ";
    this.reportName3 ="Renewal Summary Report ";
    this.reportName4 ="Top-5 Volunteer Report ";

   

  }

  exportData(){
    let url = this.export_url;
    return this.http.get(url)
    .subscribe((res:any) => {
    if(res){
        this.toastr.success(res.serviceResult.message,'Export Success',{timeOut: 5000, })
      } 
  })
  }

}
