import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RenewalmembershipComponent } from './renewalmembership.component';

describe('RenewalmembershipComponent', () => {
  let component: RenewalmembershipComponent;
  let fixture: ComponentFixture<RenewalmembershipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RenewalmembershipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RenewalmembershipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
