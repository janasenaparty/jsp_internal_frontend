import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KmLastTwoYearsComponent } from './km-last-two-years.component';

describe('KmLastTwoYearsComponent', () => {
  let component: KmLastTwoYearsComponent;
  let fixture: ComponentFixture<KmLastTwoYearsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KmLastTwoYearsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(KmLastTwoYearsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
