import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-km-last-two-years',
  templateUrl: './km-last-two-years.component.html',
  styleUrls: ['./km-last-two-years.component.css']
})
export class KmLastTwoYearsComponent implements OnInit {
  reportId:any;
  reportName:any;
  constructor() { }

  ngOnInit(): void {
    this.reportId =43;
    this.reportName ="Kriya Members Last 2 Years";
  }

}
