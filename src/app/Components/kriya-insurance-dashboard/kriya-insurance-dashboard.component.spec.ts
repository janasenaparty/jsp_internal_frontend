import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KriyaInsuranceDashboardComponent } from './kriya-insurance-dashboard.component';

describe('KriyaInsuranceDashboardComponent', () => {
  let component: KriyaInsuranceDashboardComponent;
  let fixture: ComponentFixture<KriyaInsuranceDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KriyaInsuranceDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KriyaInsuranceDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
