import { Component, OnInit } from '@angular/core';
import { ChartData, ChartType } from 'chart.js';
import { ChartsService } from 'src/app/Services/charts.service';

@Component({
  selector: 'app-doughnut-chart',
  templateUrl: './doughnut-chart.component.html',
  styleUrls: ['./doughnut-chart.component.css']
})
export class DoughnutChartComponent implements OnInit {
  test:any=[]



  constructor(private chartService:ChartsService) {
  
  }

  ngOnInit() {
  
    this.chartService.getCharts(11).subscribe((res : any)=>{
    
      if (res.serviceResult) { 
        
        // { this.toastr.info('Data Filtered','Export',{timeOut: 4000, })  }
        this.doughnutChartData.labels =res.serviceResult[0].labels;
        console.log(res.serviceResult[0])
        console.log( 'res-->', this.doughnutChartData.labels )
        // this.test =this.doughnutChartData.labels
     this.doughnutChartData.datasets[0] = res.serviceResult[0].datasets[0].data;
       this.test = this.doughnutChartData.datasets[0]
     console.log( 'res1-->',  this.doughnutChartData.datasets[0] )
    }
    });
  }

  public doughnutChartLabels: string[] = [];
  public doughnutChartData: ChartData<'doughnut'> = {
    labels: [],
    
    datasets: [
      { data: [this.test] }
    ]
  };
  public doughnutChartType: ChartType = 'doughnut';

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

}
