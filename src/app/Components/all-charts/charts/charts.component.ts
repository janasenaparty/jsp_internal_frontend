import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChartsService } from 'src/app/Services/charts.service';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit {

  public chartTypes: string[] = ['line', 'bar', 'doughnut', 'pie', 'polarArea'];

  constructor(private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
    private chartsService: ChartsService) { }

  ngOnInit(): void {

  }

  selectChartType(event: any) {
    this.chartsService.chartType = event.target.value;
    switch (event.target.value) {
      case 'line':
        this.router.navigate(['/charts/line-chart']);
        break;
      case 'bar':
        this.router.navigate(['/charts/bar-chart']);
        break;
      case 'doughnut':
        this.router.navigate(['/charts/doughnut-chart']);
        break;
      case 'pie':
        this.router.navigate(['/charts/pie-chart']);
        break;
      case 'polarArea':
        this.router.navigate(['/charts/polararea-chart']);
        break;
      default:
        this.router.navigate(['/charts']);
        break;
    }
  }

}
