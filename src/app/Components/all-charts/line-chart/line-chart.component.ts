import { Component, Input, ViewChild } from '@angular/core';
import { ChartConfiguration, ChartEvent, ChartData, ChartOptions, ChartType } from 'chart.js';
import { ChartsService } from 'src/app/Services/charts.service';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'app-line',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent {
  @Input("reportId") reportId: any;

  // reportId:any;
  lineChartType: any = 'line';

  constructor(private chartsService: ChartsService) { }

  ngOnInit(): void {
    this.reportId = 12;
    this.lineChartType = this.chartsService.chartType;
   
      this.chartsService.getCharts(this.reportId).subscribe((res: any) => {
        console.log(res);
      });
  }

  getChartData(reportId: any) {
    this.chartsService.getCharts(reportId).subscribe((res: any) => {
      console.log(res);
      if (res.data.status) {
        // this.columnsMetadata = res.data.metadata;
        // this.columnsMetadata.push({headerName: 'Action', field: 'Action', cellRenderer: this.buttonCellRenderer})
        // this.columnsMetadata.push( {headerName: 'select', field: 'select', cellRenderer: this.selectCellRenderer})
        /* this.columnsMetadata.push({
      
        field: "athlete",
        cellRenderer: "btnCellRenderer",
        cellRendererParams: {
          clicked: function() {
            alert(` was clicked`);
          }
        },
        minWidth: 150
      }); */
        // this.frameworkComponents = {
        //   btnCellRenderer: BtnCellRenderer
        // };

      }

    })
  }


  

  public lineChartData: ChartConfiguration['data'] = {
    datasets: [
      {
        data: [65, 59, 80, 81, 56, 55, 40],
        label: 'Series A',
        backgroundColor: 'rgba(148,159,177,0.2)',
        borderColor: 'rgba(148,159,177,1)',
        pointBackgroundColor: 'rgba(148,159,177,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(148,159,177,0.8)',
        fill: 'origin',
      }
      
    ],
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July']
  };

  public lineChartOptions: ChartConfiguration['options'] = {
    elements: {
      line: {
        tension: 0.5
      }
    },
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      x: {},
      'y-axis-0':
      {
        position: 'left',
        grid: {
          color: 'rgba(255,0,0,0.3)',
        },
        ticks: {
          color: 'black'
        }
      }
    },

    plugins: {
      legend: { display: true },
    }
  };

  // events
  public chartClicked({ event, active }: { event?: ChartEvent, active?: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event?: ChartEvent, active?: {}[] }): void {
    console.log(event, active);
  }


}
