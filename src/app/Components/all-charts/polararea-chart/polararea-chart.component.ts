import { Component, OnInit } from '@angular/core';
import { ChartData, ChartType } from 'chart.js';

@Component({
  selector: 'app-polararea-chart',
  templateUrl: './polararea-chart.component.html',
  styleUrls: ['./polararea-chart.component.css']
})
export class PolarareaChartComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

// PolarArea
public polarAreaChartLabels: string[] = [ 'Download Sales', 'In-Store Sales', 'Mail Sales', 'Telesales', 'Corporate Sales' ];
public polarAreaChartData: ChartData<'polarArea'> = {
  labels: this.polarAreaChartLabels,
  datasets: [ {
    data: [ 300, 500, 100, 40, 120 ],
    label: 'Series 1'
  } ]
};
public polarAreaLegend = true;

public polarAreaChartType: ChartType = 'polarArea';

// events
public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
  console.log(event, active);
}

public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
  console.log(event, active);
}

}
