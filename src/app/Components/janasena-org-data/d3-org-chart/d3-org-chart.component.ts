
import {
  OnChanges,
  Component,
  OnInit,
  Input,
  ViewChild,
  ElementRef,
  Inject
} from '@angular/core';

import { OrgChart } from 'd3-org-chart';
import * as d3 from 'd3';
import { MatDialog} from '@angular/material/dialog';
import { DialogOverviewExampleDialog } from './dialog-overview-example-dialog';

import rawData from '../transformed_eg_data.json';
@Component({
  selector: 'app-d3-org-chart',
  templateUrl:'./d3-org-chart.component.html',
  styleUrls: ['./d3-org-chart.component.css']
})
export class D3OrgChartComponent implements OnInit, OnChanges {
@ViewChild('chartContainer') chartContainer: ElementRef | undefined;
 @Input() data: any[] = [];
   chart: any;
   name = '';
   tags = '';
   assembly = '';
   district = '';
   designation = '';

constructor(private dialog: MatDialog) {}

ngOnInit() {}

ngAfterViewInit() {
  if (!this.chart) {
    this.chart = new OrgChart();
  }
  this.updateChart();
}

ngOnChanges() {
  this.updateChart();
}

expandAll() {
  this.chart && this.chart.expandAll().fit();
}
collapseAll() {
  this.chart && this.chart.collapseAll().fit();
}
clearInput(fieldName:any) {
  // @ts-ignore
  this[fieldName] = null;
  this.filterChartByProperty();
}
getFlattenedData(nodes:any) {
  let descendants = d3.hierarchy(rawData).descendants();

    let filteredDescendants : any[] = [];
    nodes.forEach((n:any) => {
      let paths = d3.hierarchy(rawData).path(n).filter(f => f);
      paths.forEach(p => {
        if(!filteredDescendants.find(f => f.data === p.data)) {
          filteredDescendants.push(p);
        }
      })
    });
    descendants = filteredDescendants;

  descendants.forEach((d:any, i) => {
    d.id = d.id || 'id' + i;
    d.data.id = d.id || 'id' + i;
  });

  return descendants
    .map((d:any, i) => [d.parent?.data?.id, d.data])
    .map(([parentId, data]) => {
      const copy = { ...data };
      delete copy.children;
      return { ...copy, ...{ parentId } };
    });
}

filterDescendants(fieldName:any, descendants:any[]) {
  // Get input value
  // @ts-ignore
  const value = this[fieldName] && this[fieldName].trim();
 return fieldName === 'tags' ?descendants.filter((d:any) => !value || (d.data.tags && d.data.tags.find((t:any) => t.toLowerCase().includes(value.toLowerCase() ))))
  : descendants.filter((d:any) => !value || (d.data[fieldName] && d.data[fieldName].toLowerCase().includes(value.toLowerCase())));
}
filterChartByProperty() {
  let descendants = d3.hierarchy(rawData).descendants();
  let nodes = this.filterDescendants('name', descendants);
  nodes = this.filterDescendants('district', nodes);
  nodes = this.filterDescendants('tags', nodes);
  nodes = this.filterDescendants('designation', nodes);
  nodes = this.filterDescendants('assembly', nodes);
  // Get chart nodes
  descendants = this.getFlattenedData(nodes);
  // // Mark all previously expanded nodes for collapse
  // data.forEach((d) => (d._expanded = false));
  // // Loop over data and check if input value matches any name
  // data.forEach((d) => {
  //   if (value != '' && d[fieldName] && d[fieldName].toLowerCase().includes(value.toLowerCase()) ) {
  //     // If matches, mark node as highlighted
  //     d._highlighted = true;
  //     d._expanded = true;
  //   }
  // });
  // Update data and rerender graph
  this.chart.data(descendants).render().fit();

  // console.log('filtering chart', e.srcElement.value);
}
// filterChartByTags(e:any) {
//   // Get input value
//   const value = e.srcElement.value;

//   let descendants = d3.hierarchy(rawData).descendants();
//   const nodes = descendants.filter((d:any) => d.data.tags && d.data.tags.find((t:any) => t.toLowerCase().includes(value.toLowerCase() )));

//   // Get chart nodes
//   descendants = this.getFlattenedData(nodes);

//   // Update data and rerender graph
//   this.chart.data(descendants).render().fit();

//   console.log('filtering chart', e.srcElement.value);
// }
openDialog(data:any) {

  const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
    data: data,
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log('The dialog was closed');
  });
}
updateChart() {
  if (!this.data) {
    return;
  }
  if (!this.chart) {
    return;
  }
  this.chart
    .container(this.chartContainer?.nativeElement)
    .data(this.data)
    .rootMargin(100)
        .nodeWidth((d: any) => 210)
        .nodeHeight((d: any) => 180)
        .childrenMargin((d: any) => 130)
        .compactMarginBetween((d: any) => 75)
        .compactMarginPair((d:any) => 80)
        .onNodeClick((d: any) => { d.data.designation && this.openDialog(d.data); })
        .linkUpdate(function (d: any, i: any, arr: any[]) {
            // @ts-ignore
          d3.select(this)
            .attr('stroke', (d:any) =>
              d.data._upToTheRootHighlighted ? '#152785' : 'lightgray'
            )
            .attr('stroke-width', (d:any) =>
              d.data._upToTheRootHighlighted ? 5 : 1.5
            )
            .attr('stroke-dasharray', '4,4');

          if (d.data._upToTheRootHighlighted) {
              // @ts-ignore
            d3.select(this).raise();
          }
        })
        .nodeContent(function (d: any, i: any, arr: any, state: any) {
          const colors = [
            '#6E6B6F',
            '#18A8B6',
            '#F45754',
            '#96C62C',
            '#BD7E16',
            '#802F74',
          ];
          const color = colors[d.depth % colors.length];
          const imageDim = 80;
          const lightCircleDim = 95;
          const outsideCircleDim = 110;

          return `
              <div style="background-color:white; position:absolute;width:${
                d.width
              }px;height:${d.height}px;">
                 <div style="background-color:${color};position:absolute;margin-top:-${outsideCircleDim / 2}px;margin-left:${d.width / 2 - outsideCircleDim / 2}px;border-radius:100px;width:${outsideCircleDim}px;height:${outsideCircleDim}px;"></div>
                 <div style="background-color:#ffffff;position:absolute;margin-top:-${
                   lightCircleDim / 2
                 }px;margin-left:${d.width / 2 - lightCircleDim / 2}px;border-radius:100px;width:${lightCircleDim}px;height:${lightCircleDim}px;"></div>
                 <img src=" ${
                   d.data.imgUrl
                 }" style="position:absolute;margin-top:-${imageDim / 2}px;margin-left:${d.width / 2 - imageDim / 2}px;border-radius:100px;width:${imageDim}px;height:${imageDim}px;" />
                 <div class="card" style="top:${
                   outsideCircleDim / 2 + 10
                 }px;position:absolute;height:30px;background-color:#3AB6E3;">
                    <div style="background-color:${color};text-align:center;padding:10px 5px 0px 5px;color:#ffffff;font-weight:bold;font-size:16px">
                        ${d.data.name}
                    </div>
                    <div style="background-color:#F0EDEF;height:28px;text-align:center;padding-top:10px;color:#424142;font-size:16px">
                        ${d.data.designation || ''}
                    </div>
                 </div>
             </div>
`;
        })
        .render();
}
}
