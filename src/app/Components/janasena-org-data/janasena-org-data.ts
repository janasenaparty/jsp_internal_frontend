import { TabularreportService } from 'src/app/Services/tabularreport.service';
import { Breadcrumb } from './../../model/breadcrumb.model';
import { BreadcrumbService } from './../../Services/breadcrumb.service';

import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3';
import { Observable, combineLatest, map, of, startWith } from 'rxjs';
import { FormControl } from '@angular/forms';
import  distdata from './transformed_eg_data.json';
// import * as data from './transformed_state_data.json';
import data  from './transformed_state_data.json';
import dropDownData from './villageGramPanchayatMapping.json';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'janasena-org-data',
  templateUrl: './janasena-org-data.html',
  styleUrls: ['./janasena-org-data.scss'],
  providers: []
})
export class JanasenaOrgData implements OnInit {
  breadcrumbs$: Observable<Breadcrumb[]>;
  // searchByName$: Observable<any[]>;
  filter: FormControl;
  data2: any;
  data3: any;
  data5:any
  currentDistrict: any;
  committees = [
    'State',
    'District',
    'IT',
    'Corporation',
    'Doctors',
    'Legal',
    'Matsyakaara',
    'Program'
  ];
  districts: any[] = [...new Set(dropDownData.map((item:any) => item.districtName))]
  .map((d:any) => {
    return {
      value: d,
      display: d
    }
  });
  subDistricts : { value: any; display: any; }[]=[];
  currentSubDistrict: any;
  villages: { value: any; display: any; }[] = [];
  currentVillage: any;

  constructor(private readonly http: HttpClient, private router: Router, private readonly breadcrumbService: BreadcrumbService, private trservice:TabularreportService) {
    this.filter = new FormControl('');
    this.breadcrumbs$ = this.breadcrumbService.breadcrumbs$;

    // this.searchByName$ = new Observable<any[]>();
    this.breadcrumbs$.subscribe(values => {
      console.log(values);
      this.currentDistrict = values[1] && decodeURIComponent(values[1].label);
      this.currentDistrict && this.onDistrictChange(this.currentDistrict);
      this.currentSubDistrict = values[2] && decodeURIComponent(values[2].label);
      this.currentSubDistrict && this.onSubDistrictChange(this.currentSubDistrict);
      this.currentVillage = values[3] && decodeURIComponent(values[3].label);
    });

    this.fetchCommitteeData('State Committee', 'State');
  }

  async ngOnInit() {
    this.data2 = this.getFlattenedData(data);
    this.data3 = this.getFlattenedData(distdata);
    // this.data5 = this.fetchCommitteeData()
    this.data5 = this.getFlattenedData(await this.fetchCommitteeData('State Committee', 'State'));
  }

  decodeString(val:any) {
    return decodeURIComponent(val);
  }

  onDistrictChange(e:any) {
    console.log('ditrict change');
    this.subDistricts = [...new Set(dropDownData.filter((item:any) => item.districtName === e).map((it:any) => it.subDistrictName))]
    .map(d => {
      return {
        value: d,
        display: d
      }
    });
    this.currentSubDistrict = null;
    this.onSubDistrictChange(this.currentSubDistrict);
    // this.router.navigate([`/jsp_org_data/${e}`]);
  }

  onSubDistrictChange(e:any) {
    console.log('sub district change');
    this.villages = [...new Set(dropDownData.filter((item:any) => item.districtName === this.currentDistrict && item.subDistrictName === e).map((it:any) => it.villageName))]
    .map(d => {
      return {
        value: d,
        display: d
      }
    });
    this.currentVillage = null;
    // this.router.navigate([`/jsp_org_data/${this.currentDistrict}/${e}`]);
  }

  onVillageChange(e:any) {
    console.log('village change');
    // this.router.navigate([`/jsp_org_data/${this.currentDistrict}/${this.currentSubDistrict}/${e}`]);
  }

  routeTo() {
    this.router.navigate([`/jsp_org_data/${this.currentDistrict}/${this.currentSubDistrict}/${this.currentVillage}`]);
  }

  getFlattenedData(dataHierarchy:any) {
    const descendants = d3.hierarchy(dataHierarchy).descendants();

    descendants.forEach((d:any, i) => {
      d.id = d.id || 'id' + i;
      d.data.id = d.id || 'id' + i;
    });

    return descendants
      .map((d, i) => [d.parent?.data?.id, d.data])
      .map(([parentId, data]) => {
        const copy = { ...data };
        delete copy.children;
        return { ...copy, ...{ parentId } };
      });
  }
  async fetchCommitteeData(committeeName: string, committeeLevel: string) {
    const response: any = await this.trservice.getCommitteeData(committeeName, committeeLevel).toPromise();
    return response?.serviceResult;
  }
}
