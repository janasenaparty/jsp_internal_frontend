import { Breadcrumb } from './../../model/breadcrumb.model';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';

// Import PrimeNG modules
// import { OrganizationChartModule } from 'primeng/organizationchart';
// import { TreeModule } from 'primeng/tree';
// import { ToastModule } from 'primeng/toast';
// import { MessageService } from 'primeng/api';
import { NodeService } from '../../Services/nodeservice';
// import { ButtonModule } from 'primeng/button';
import { D3OrgChartComponent } from './d3-org-chart/d3-org-chart.component';
import { DialogOverviewExampleDialog } from './d3-org-chart/dialog-overview-example-dialog';
import { JanasenaOrgData } from './janasena-org-data';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDialogModule} from "@angular/material/dialog";
import { MatIconModule } from '@angular/material/icon';

import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatOptionModule } from '@angular/material/core';

@NgModule({
  imports: [
    // OrganizationChartModule,
    // ToastModule,
    // TreeModule,
    // ButtonModule,
    CommonModule,
    MatFormFieldModule, MatInputModule, MatDialogModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatSelectModule,
    MatOptionModule

  ],
  declarations: [JanasenaOrgData, D3OrgChartComponent, DialogOverviewExampleDialog],
  exports: [ JanasenaOrgData, D3OrgChartComponent, DialogOverviewExampleDialog ],
    providers: [NodeService],// MessageService]
})

export class JanasenaOrgDataModule {}
