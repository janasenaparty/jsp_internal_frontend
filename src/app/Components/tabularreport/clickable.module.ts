import {NgModule} from "@angular/core";

import { CommonModule } from '@angular/common';

import {ClickableComponent} from "./clickable.component";
import {ClickableParentComponent} from "./clickable.parent.component";

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        ClickableComponent,
        ClickableParentComponent
    ],
    exports: [
        ClickableParentComponent
    ]
})
export class ClickableModule {
}