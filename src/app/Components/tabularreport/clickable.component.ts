import {Component, EventEmitter, Input, Output} from "@angular/core";

@Component({
    selector: 'ag-clickable',
    template: `
    <span (click)="click()" [ngStyle]="getStyles()"  type="button" [class]="getButtonClass()">{{cell.buttonText}} 
    <i [class]="getIconClass()"></i> </span>`
})
export class ClickableComponent {
    @Input() cell: any;
    @Output() onClicked = new EventEmitter<boolean>();

    click(): void {
        this.onClicked.emit(this.cell);
    }
    getButtonClass() {
        return this.cell.btnNgClass;
    }

    getIconClass() {
        return this.cell.iconNgClass;
    }

    getStyles() {
        return this.cell.customStyles
    }
}