import { Component, Input, OnInit,ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TabularreportService } from 'src/app/Services/tabularreport.service';
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-alpine.css";
import { HttpClient } from '@angular/common/http';


import { BtnCellRenderer } from "../btn-cell-renderer/btn-cell-renderer.component";
import { CellCustomComponent } from './actionButtonsComponent';
import { CellCustomComponent1 } from './button-cell-renderer/buttoncellrendererComponent';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from 'src/app/Services/common.service';
import { HttpBackend } from '@angular/common/http';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl} from '@angular/forms';
import { KriyavolunteersService } from 'src/app/Services/kriyavolunteers.service';
import { ClickableParentComponent } from "./clickable.parent.component";

@Component({
  selector: 'app-tabularreport',
  templateUrl: './tabularreport.component.html',
  styleUrls: ['./tabularreport.component.css']
})
export class TabularreportComponent implements OnInit {
  @ViewChild('closebutton') closebutton:any;
  @ViewChild('closebuttonEditvolunteer') closebuttonEditvolunteer:any;
  @ViewChild('closebuttonAddvolunteer') closebuttonAddvolunteer:any;
  @ViewChild('closebuttonAddBVolunteer') closebuttonAddBVolunteer:any;
  @ViewChild('closebuttonAddBpayment') closebuttonAddBpayment:any;
  @ViewChild('closebuttonEditVisitor') closebuttonEditVisitor:any;
  @ViewChild('closebuttonScheduleVisitor') closebuttonScheduleVisitor:any;
  @ViewChild('closebuttonAddLeader') closebuttonAddLeader:any;
  @ViewChild('closebuttonAddReason') closebuttonAddReason:any;
  @ViewChild('closebuttonAddEmployee') closebuttonAddEmployee:any;
  @ViewChild('closebuttoncms') closebuttoncms:any;
  @ViewChild('closebuttoncmsedit') closebuttoncmsedit:any;
  // @ViewChild('closebuttonAddVisitor') closebuttonAddVisitor:any;
  
  @ViewChild('closebuttonView') closebuttonview:any;
  // @ViewChild('fileInput', { static: false })
  @ViewChild('fileInput')
  fileInputReference!: ElementRef;
 
  otherDropdowns  =0;

  myInputVariable!: ElementRef;
  public overlayLoadingTemplate:any;
  public noRowsTemplate;
  submitted =false;
  public loadingTemplate;
  config: any; 
  search_Text=''
  public frameworkComponents :any;
  /* pagination */
  public current =1;
  deletIssueDataId:number=0;
  deleteConflictDataId:number=0;
  deletPetionDataId:number=0;
  deletVisitorDataId:number=0;
  deletVolunteerDataId:number=0;
  deletInsuranceDataId:number=0;
  deleteEmployeeDataId:number=0;
  man_id:any;
  vill_id:any;
  cons_id:any;
  memberId:any;
  fileUploadButtonDisable:boolean =true;
  volunteerName: any;
  memberDetails :any;
  isSubmitedForm:boolean =false;
  memberDob:any;
  memberAadharNumber:any;
  memberEmail:any;
  memberMobile:any;
  memberState:any;
  memberStatus:any;
  
  memberDistrict:any;
  memberConstituency:any;
  memberAddress:any;
  memberRemarks:any;
  memberAadharCard:any;
  memberGender:any;
  memberNomineeName:any;
  fileupload:any;
  fileupload2:any;
  fileupload3:any;
  memberNominiAadharNumber:any;
  loading:any;
  type :any;
  user_photo :any;
  aadhar_card:any;
  emp_photo:any;
  receiptData:any;
  public itemsToDisplay: string[] = [];
  public perPage = 100;
  public total =0;
  isChecked:boolean =false;
  /* end */
 public gridApi :any;
  isShowDivIf = false; 
  isVolunteerShowDivIf = false; 
  addVolDiv = false ;
  IsmodelShow:boolean=false;
  myFiles:string [] = [];
  uploadFiles:string [] = [];
  uploadKMSFiles:string [] = [];
  uploadCMSFiles:string [] = [];
  otherOptionValueVisitorForm ='';
  p = 1;
  @Input("reportId") reportId: any; 
  @Input("reportName") reportName: any; 
  @Input("hasActions") hasActions =false;
  @Input("hasFilters") hasFilters =false;
  @Input("hasEdit") hasEdit =false;
  @Input ("hasVolunteerActions") hasVolunteerActions= false;
  @Input ("hasVoterDbActions") hasVoterDbActions= false;
  @Input ("hasInsuranceActions") hasInsuranceActions= false;
  @Input ("hasPetitionActions") hasPetitionActions= false;
  @Input ("hasIssueActions") hasIssueActions= false;
  @Input ("hasConflictActions") hasConflictActions= false;
  @Input ("hasVisitorActions") hasVisitorActions= false;
  @Input ("hasEmployeeActions") hasEmployeeActions= false;

  @Input ("hasMapVoterActions") hasMapVoterActions= false;
  @Input ("hasBulkPaymentActions") hasBulkPaymentActions= false;
  // @Input( ) onResetClick:EventEmitter<any> =new EventEmitter<any>();

  // @Input("hasRole") hasRole =false;
  
  suspended_by:any;
  reason:any;
  file_url:any;
  attachment_urls:any;
  rowsData:any;
  rowData1:any;
  checkbox: boolean =false;
  columnsMetadata : any;
  tablemetadata:any;
  defaultQuery:any;
  activePage:number = 0;  
  totalRecords:number =0;
  recordsPerPage:number =10;
  selectedLimitValue:number =10;
  rid:any;
  eventQuery :any;
  paymentImageUrl:any;
  userData:any;
  voterData:any;
  issueData:any;
  conflictData:any;
  petitionerData:any;
  visitorData:any;
  insuranceData:any;
  employeeData:any;
  volunteerData:any;
  empQrData:any;
  updatedValue:any;
  updatedVoter:any;

  updateRemarks:any;
  memberName:any;
  voterName:any;
  voterAge:any;
 voterHno:any;
 
voterMandal :any;
 voterDistrict :any;
 voterConstituency  :any;
 voterPincode:any;
  voterPollingStationName  :any;
  voterPollingStationNo :any;
   voterRelationType  :any;
voterRelativeName :any;
 voterSno :any;
 voterVoterId  :any;
  voterVoters2022SeqId:any;
  totalItems:any;
  params: any;
  data: any;
  isRole: any;
  isName : any;
  isMail : any;
  context: any;
  leaders:any;
  reasons:any;
  leadersById :any;
  reasonsById :any;
  departMents:any[]=[];
  categories:any[]=[];
  districts:any[]=[];
  districtsForAPTS:any[]=[];
  constituents:any[]=[];
  modals:any[] =[];
  apmandals:any[] =[];
  allmodals:any[]=[];
  apimandals:any[]=[];
  allvillages:any =[];
  allbooths:any =[];
  booth_details :any =[];
  allConstituents:any[]=[];
  numberCountArray: number[] =[];
  isInputVisible: boolean =true;
  dynamicArray:any[]  =[];
  user_photo2: File | undefined;
  scanResult:any;
  selectionConst:any
  selectionDist:any
  // SERVER_URL = "/api/portal/admin/get_qrcode_members";
  uploadForm:any=FormGroup
  document: any;
  // httpClient: any;
  BULKVOLUNTEER_URL="/api/portal/admin/add_bulk_volunteers"
  blobDataURL: string | null = null;
  qrCodeImage: any;
  qrCodeUrl: any;
  /*displayActivePage(activePageNumber:number){  
    this.gridApi.api.showLoadingOverlay();
    this.activePage = activePageNumber;  
    let query:any = this.eventQuery;
    query["max-results"]= <number> this.recordsPerPage;
    query["results-offset"]=activePageNumber;
    this.trservice.getDataByQuery(this.rid,query).subscribe((res : any)=>{
      if (res.serviceResult) { 
        this.rowsData = res.serviceResult.results;
        this.totalRecords = res.serviceResult.totalResults;
        this.gridApi.api.hideOverlay();
    }
    })  
  }  */
  chnageOtherValueDropdown(event:any){
     this.numberCountArray = Array(Number(event.target.value)).fill(1);
    this.otherDropdowns = Number(event.target.value)
  }


  
  constructor(   private toastr:ToastrService, private element: ElementRef,private http: HttpClient,private kvservice : KriyavolunteersService ,private trservice : TabularreportService , private FB:FormBuilder, private httpClient: HttpClient,private router :Router) {
   
    this.loadingTemplate =
    `<span  class="ag-overlay-loading-center"> <h2>Loading...</h2></span>`;
  this.noRowsTemplate =
    `"<span">No Rows To Show</span>"`;
 
  
  this.trservice.paymentImageUrl.subscribe((res:any)=>this.paymentImageUrl=res)
  this.trservice.userData.subscribe((res:any)=>this.userData=res)
  this.trservice.petitionerData.subscribe((res:any)=>this.petitionerData=res)
  this.trservice.issueData.subscribe((res:any)=>this.issueData=res)
  this.trservice.conflictData.subscribe((res:any)=>this.conflictData=res)
  this.trservice.employeeData.subscribe((res:any)=>this.employeeData=res)
  this.kvservice.volunteerData.subscribe((res:any)=>this.volunteerData=res)
  this.trservice.empQrData.subscribe((res:any)=>this.empQrData=res)
  this.trservice.visitorData.subscribe((res:any)=>{
    
    this.visitorData=res;
    this.getConsituents(this.visitorData.district_id);
    this.getModals(this.visitorData.constituency_id);
    // this.getTelangana()
  }
    )
  this.trservice.insuranceData.subscribe((res:any)=>this.insuranceData=res)
  
  this.context = {
    componentParent: this
}
  
   }

   volunteerform = this.FB.group(
    {
      name:  ['', Validators.required],
      mobile:  ['', Validators.required],
      gender:  ['', Validators.required],
      state:  ['', Validators.required],
      district_id:  ['', Validators.required],
      constituency_id:  ['', Validators.required],
      volunteer_reference:  [''],
    }
   )
   suspendMemberform = this.FB.group(
    {
      file_url:  [''],
      suspended_by:  ['', Validators.required],
      reason:  ['', Validators.required],
    }
   )
   editvolunteerform = this.FB.group(
    {
      name:  [''],
      mobile:  [''],
      gender:  [''],
      state:  [''],
      district_id:  [''],
      constituency_id:  [''],
    }
   )
  grievanceform = this.FB.group(
    {

      title:  ['', Validators.required],
      department:  [''],
      fullname:  ['', Validators.required],
      // category:  ['', Validators.required],
      // email:  [''],
      mobile:  ['', Validators.required],
      // gender:  ['', Validators.required],
      district_id:  [0, Validators.required],
      constituency_id:  [0, Validators.required],
      mandal:  [0, Validators.required],
      // voter_id:  ['', Validators.required],
      // state_id:  ['', Validators.required],
     
      // petition_files:  ['', Validators.required],
      remarks:  [''],
      address:  ['', Validators.required],
      village:  ['']
     

    }
  );

  editgrievanceform = this.FB.group(
    {
     
      department:  ['', Validators.required],
      name:  ['', Validators.required],
      mobile:  ['', Validators.required],
      subject:  ['', Validators.required],

      // district_id:  [0, Validators.required],
      // constituency_id:  [0, Validators.required],
      // mandal:  [0, Validators.required],
     
      // petition_files:  ['', Validators.required],
      // remarks:  [''],
      address:  ['', Validators.required],
      // village:  ['']
     

    }
  );
  issueform = this.FB.group(
    {

       name:  ['',Validators.required],
        mobile:  ['',Validators.required],
        state_id:[1],
        district_id:  [0],
        constituency_id:  [0],
        mandal_id:  [0],
        village_or_ward:  [''],
        role:  [''],
        issue_subject:  ['',Validators.required],
        issue_notes:  [''],
        assignee_name:  [''],
        assignee_phone:  [''],
        assignee_role:  [''],
        expected_target_date: [],
        remarks: [''],
        issue_status:['created']
        

    }
  );
  editissueform = this.FB.group(
    {

      name:  ['',Validators.required],
      mobile:  ['',Validators.required],
      state_id:[1],
      district_id:  [0],
      constituency_id:  [0],
      mandal_id:  [0],
      village_or_ward:  [''],
      role:  [''],
      issue_subject:  ['',Validators.required],
      issue_notes:  [''],
      assignee_name:  [''],
      assignee_phone:  [''],
      assignee_role:  [''],
      expected_target_date: [],
      remarks: [''],
      issue_status:['created']
      

    }
  );
  conflictform = this.FB.group(
    {

       name:  ['',Validators.required],
        mobile:  ['',Validators.required],
        state_id:[1],
        district_id:  [0],
        constituency_id:  [0],
        mandal_id:  [0],
        village_id:  [''],
        polling_station_id: [''],
        // role:  [''],
        category:  ['',Validators.required],
        conflict_subject:  ['',Validators.required],
        conflict_notes:  [''],
        assignee_name:  ['',Validators.required],
        assignee_phone:  ['',Validators.required],
        assignment_level:  ['',Validators.required],
        expected_target_date:['',Validators.required],
        remarks: [''],
        issue_status:['created']
        

    }
  );
  editconflictform = this.FB.group(
    {

      //  name:  ['',Validators.required],
      //   mobile:  ['',Validators.required],
      //   state_id:[1],
      //   district_id:  [0],
      //   constituency_id:  [0],
      //   mandal_id:  [0],
      //   village_id:  [''],
      //   polling_station_id:  [''],
      //   // role:  [''],
      //   category:  [''],
      //   conflict_subject:  [''],
        conflict_notes:  [''],
        assignee_name:  [''],
        assignee_phone:  [''],
        assignment_level:  [''],
        attachment_urls: [''],
        // file_url:  [''],
        // expected_target_date:[''],
        // remarks: [''],
        // issue_status:['created']
        

    }
  );
  visitorform = this.FB.group(
    {
      visitor_name:  ['', Validators.required],
      visitor_email:  ['',Validators.email],
      additional_members : [''],
      visitor_phone:  ['', Validators.required],
      approach_type :  [''],
      request_type :  [''],
      visitor_age:  [],
      visitor_gender:  ['', Validators.required],
      district_id:  [0, Validators.required],
      constituency_id:  [0, Validators.required],
      state_id:  ['', Validators.required],

      mandal_id:  [0],
      // voter_id:  ['', Validators.required],
     
      // petition_files:  ['', Validators.required],
      visitor_address:  ['' , Validators.required],
      visting_location:  ['', Validators.required],
      purpose_of_meeting:  ['', Validators.required],
      notes_or_issue:  [''],
      priority:  [''],
      event:  [''],
      whom_to_meet:  ['Sri Pavan Kalyan', Validators.required],
      appointment_date : ['', Validators.required]

    }
  );
 
  editvisitorform = this.FB.group(
    {
      visitor_name:  [''],
      visitor_email:  [''],
    
      visitor_phone:  [''],
      visitor_age:  [''],
      visitor_gender:  [''],
      district_id:  [0],
      constituency_id:  [0],
      state_id:  [''],

      mandal_id:  [0],
      // voter_id:  ['', Validators.required],
     
      // petition_files:  ['', Validators.required],
      visitor_address:  ['' ],
      visting_location:  [''],
      purpose_of_meeting:  [''],
      notes_or_issue:  [''],
      priority:  [''],
      event:  [''],
      whom_to_meet:  [''],
      appointment_date : [''],
      appointment_status : [''],
      approach_type :  [''],
      request_type :  ['']
     
    }
  );
  addleaderform = this.FB.group(
    {
      name :[ '', Validators.required],
      designation : ['',Validators.required],
      email:[ '',Validators.required]
    }
  );
  addreasonform = this.FB.group({
    reason : ['',Validators.required]
  });
  setmeetform = this.FB.group({ 
    visitor_name:  [''],
    visitor_phone:  [''],
    notes_or_issue:  [''],

    appointment_date : ['',Validators.required],
    from_time : ['',Validators.required],
    to_time : ['',Validators.required]
    
  })

    employeeform = this.FB.group({
    firstname:  ['',Validators.required],
    lastname: ['',Validators.required],
    email: ['',Validators.required],  
    mobile:['',Validators.required],
    dob:['',Validators.required],
     department: ['',Validators.required],
     designation :['',Validators.required],
     joinedon :['',Validators.required],
     resignedon :['',Validators.required],
      address: ['',Validators.required],
      aadhar_number:['',Validators.required],
     })

     editemployeeform= this.FB.group({
      firstname:  [''],
      lastname:  [''],
       email:  [''],
        mobile: [''],
        dob: [''],
        department:  [''],
        designation : [''],
        joinedon :[''],
        resignedon :[''],
         address:  [''],
         aadhar_number: [''],
           })

           bulkpaymentform = this.FB.group({
            utr_no:  ['',Validators.required],
            description: ['',Validators.required],
            payment_made_by: ['',Validators.required],  
            approved_by:['',Validators.required],
            payment_receipt:['',Validators.required],
            file: ['',Validators.required],
            
             })
   agInit(params: { value: any; }) {
    this.params = params;
    this.data = params.value;
  }

   editutrform = this.FB.group(
    {

    
      // business_photos :[this.service.tempInvestor?this.service.tempInvestor.business_photos:'', Validators.required],
      // proofs :[this.service.tempInvestor?this.service.tempInvestor.proofs:'', Validators.required],
  
    }
  );
  get visitorName() { return this.visitorform.get('visitor_name'); }
  ngOnInit(): void {
    this.trservice.getTelangana()
    this.uploadForm = this.FB.group({
      profile: ['']
    });
    if( localStorage.getItem('isRole') ){
      this.isRole = localStorage.getItem('isRole');
      this.isName = localStorage.getItem('isName');
      this.isMail = localStorage.getItem('isMail');
      console.log(this.isRole)
      this.trservice.userData.subscribe(res => {
        this.voterData = res;
        console.log(this.voterData , '-----------------------hiiiiiii');
      });
     
   }

   
this.updatedValue='';
this.getDepartmentsData();
this.getDistrictsJson(1); 


// this.getLeaders();
this.getReasons();
// this.getReasonsById('');
// this.getLeadersById('');
  }
   onChangeState(event:any){
       this.getDistrictsJsonValun(event.target.value)
   }
 
     onChangeDistrict(event:any){
      this.getConsituents(event.target.value)
    }
  onChangeConstitute(event:any){
    this.getAPMandals(event.target.value);
    this.getModals(event.target.value)

  //   this.constituencyid = this.seleA
  // console.log("Your Filtered Data Here",this.showRate);
 }
 onChangeMandal(event:any){
  this.getVillages(event.target.value,this.cons_id)
}
onChangeVillage(event:any){
  this.getBooths(event.target.value,this.cons_id,this.man_id,this.vill_id)
}
 getDistrictsJsonValun(stateid:any){
    
  this.trservice.getDistricts().subscribe(
    (response) => { this.districtsForAPTS =response[stateid];
      
     });

}
  getDistrictsJson(stateid:any){
    
    this.trservice.getDistricts().subscribe(
      (response) => { this.districts =response[stateid];
        
       });

  }
  changed(event:any,data:any) {
    this.checkbox = event.target.checked;
    if(this.checkbox)
    {
      this.updatedVoter =data.voter_id
    }
  }


  getVillages(state_id:number,constituency_id:number){
    this.trservice.getVillages(state_id,constituency_id).subscribe((res:any)=>{
     
      this.apimandals =res.serviceResult;
      console.log(this.apimandals,'mdl');
      
    //  for(const mandals of res.serviceResult){
    //   const villageNames=mandals.villages.map((village:any)=>village.village_name)
    //   this.allvillages.push(...villageNames);
    //   console.log(this.allvillages)
    //  }

    const selectedMandal=res.serviceResult.find((mandal:any)=>mandal.mandal_id==this.man_id)
    if(selectedMandal){
      this.allvillages=selectedMandal.villages
      console.log(this.allvillages,'vlg');
      
    }
      
     
     
      // (response) => { 
        // if(state_id){
          // this.constituents =response.constituencies.filter(function(el:any){
          //   return el.district_id == districtid;
        //   });
        // }else{
        //   this.constituents =response.constituencies; 
    }
    )}
    getBooths(state_id:number,constituency_id:number,mandal_id:number,village_id:number){
      this.trservice.getBooths(state_id,constituency_id,mandal_id,village_id).subscribe((res:any)=>{
        
        // this.apimandals =res.serviceResult;
        // console.log(this.apimandals,'mdl');

        this.booth_details =res.serviceResult.booth_details;
      console.log(this.booth_details,'booths');
      // const selectedVillage=res.serviceResult.find((mandal:any)=>vil.village_id==this.vill_id)
      const selectedVillage:any = this.vill_id
      if(selectedVillage){
        this.allbooths=selectedVillage.booth_details
        console.log(this.allbooths,'allbooths');
        
      }
    })
    
  }
  getConsituents(districtid:any){
    this.trservice.getConstituencies().subscribe(
      (response) => { 
        if(districtid){
          this.constituents =response.constituencies.filter(function(el:any){
            return el.district_id == districtid;
          });
        }else{
          this.constituents =response.constituencies; 
        }
        
        
       });
  }
  
  getModals(constitunce:any){
    this.trservice.getModals().subscribe(
      (response) => { 

        this.allmodals =response;
        this.modals =response.filter(function(el:any){
        return el.constituency_id == constitunce;
      });
      console.log( this.modals)
        
       });
  }
  getAPMandals(constitunce:any){
    this.trservice.getAPMandals().subscribe(
      (response) => { 

        this.allmodals =response;
        this.apmandals =response.filter(function(el:any){
        return el.constituency_id == constitunce;
      });
      console.log( this.apmandals)
        
       });
  }
  getDepartmentsData(){
    this.trservice.getDepartments().subscribe(
      (response) => { this.departMents =response.serviceResult; },
      (error) => { console.log(error) }
    );
  }
  getCategoriesBydepment(department:string){
    this.trservice.getCategoriesByDep(department).subscribe(
      (response) => { this.categories =response.serviceResult; },
      (error) => { console.log(error) }
    );
  }
  getCategories(event:any){
    this.getCategoriesBydepment(event.target.value); 
  }
  updateStatus(seq_id:number){
    this.trservice.updatePetitionStatus(seq_id,this.updateStatus).subscribe(
      (response) => {   this.closebuttonview.nativeElement.click();
        this.toastr.success(response.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
        this.reloadTable();
      },
       (error) => {    this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
    );
  }
  updateFilesPitition(seq_id:number){
    this.trservice.uploadPtitionFiles(seq_id,this.uploadFiles).subscribe(
      (response) => {   this.closebuttonview.nativeElement.click();
        this.uploadFiles =[];
        this.toastr.success(response.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
        this.reloadTable();
        this.myInputVariable.nativeElement.value = '';
      },
       (error) => {    this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
    );
  }
  // updateFilesSuspendKM(memberId:number){
  //   this.trservice.uploadKMSFiles(memberId,this.kms_file).subscribe(
  //     (response) => {   this.closebuttonview.nativeElement.click();
  //       this.kms_file = [];
  //       this.toastr.success(response.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
  //       this.reloadTable();
  //       this.myInputVariable.nativeElement.value = '';
  //     },
  //      (error) => {    this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
  //   );
  // } 
  onFileChange(event:any) {
    this.fileUploadButtonDisable =true;
   this.myFiles =[]; 
   this.uploadFiles =[];
   for (var i = 0; i < event.target.files.length; i++) { 
    this.myFiles.push(event.target.files[i]); 
   }
   
   for (var i = 0; i < this.myFiles.length; i++) { 
    this.fileUploadButtonDisable =true;
    const formData = new FormData();
    formData.append("pms_file", this.myFiles[i]);
    this.trservice.uploadFiles(formData).subscribe(
      (response) => {  
        this.uploadFiles.push(response.serviceResult.url);
        this.fileUploadButtonDisable =false;
      },
       (error) => {    this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
    );
   }
    

   
     
}
onKMSFileChange(event:any) {
  this.fileUploadButtonDisable =true;
 this.myFiles =[]; 
 this.uploadKMSFiles =[];
 for (var i = 0; i < event.target.files.length; i++) { 
  this.myFiles.push(event.target.files[i]); 
 }
 
 for (var i = 0; i < this.myFiles.length; i++) { 
  this.fileUploadButtonDisable =true;
  const formData = new FormData();
  formData.append("kms_file", this.myFiles[i]);
  this.trservice.uploadKMSFiles(formData).subscribe(
    (response) => {  
      this.uploadKMSFiles.push(response.serviceResult.url);
      this.file_url =response.serviceResult.url;
      this.fileUploadButtonDisable =false;
    },
     (error) => {    this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
  );
 }
}
onCMSFileChange(event:any) {
  this.fileUploadButtonDisable =true;
 this.myFiles =[]; 
 this.uploadCMSFiles =[];
 for (var i = 0; i < event.target.files.length; i++) { 
  this.myFiles.push(event.target.files[i]); 
 }
 
 for (var i = 0; i < this.myFiles.length; i++) { 
  this.fileUploadButtonDisable =true;
  const formData = new FormData();
  formData.append("kms_file", this.myFiles[i]);
  this.trservice.uploadCMSFiles(formData).subscribe(
    (response) => {  
      this.uploadCMSFiles.push(response.serviceResult.url);
      this.attachment_urls =response.serviceResult.url;
      this.fileUploadButtonDisable =false;
    },
     (error) => {    this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
  );
 }
}
  clearvalue(){
    this.updatedVoter='';   
    this.updatedValue=''; 
    this.updateRemarks=''; 
    this.receiptData='';
    this.conflictData='';
    

    // this.payment_receipt='';
  }
  updateVoterId(){
 
    this.trservice.updateVoterId(this.memberId,this.updatedVoter).subscribe((res:any) =>{ 
   console.log(res)
   this.clearvalue() 
      
     }, 
  
   );
  

// console.log(this.userData?.id);
// console.log(this.userData?.payment_id);
console.log(this.updatedValue);


  }
  public getImageExstendsion(image:any) {
    if (image.endsWith('jpg') || image.endsWith('jpeg') || image.endsWith('png')) {
      return 'image';
    }else{
      return 'doc';
    }
   
  }
  updateUtrId(){

    this.trservice.updateUtrId(this.updatedValue,this.userData?.id,this.userData?.payment_id).subscribe((res:any) =>{ 
   console.log(res)
   this.clearvalue()
      if(res.serviceResult){
        console.log('success')
        this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, })
      }else {
        console.log('error')
        this.toastr.error(res.serviceError,'Error',{timeOut: 4000, })
      }
    }, 
    (err) => {
      if(err.status == 400){
      this.toastr.error(err.serviceError,'Error',{timeOut: 4000, })
      console.log(err.status);
    }
  }
  )
  

console.log(this.userData?.id);
console.log(this.userData?.payment_id);
console.log(this.updatedValue);


  }

  remarksForUtr(){
    this.trservice.updateUtrRemarks(this.updateRemarks,this.userData?.id).subscribe((res:any) =>{ 
      console.log(res)
      this.clearvalue()
         if(res.serviceResult){
           console.log('success')
           this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, })
         }else {
           console.log('error')
           this.toastr.error(res.serviceError,'Error',{timeOut: 4000, })
         }
       }, 
    //    (err) => {
    //      if(err.status == 400){
    //      this.toastr.error(err.serviceError,'Error',{timeOut: 4000, })
    //      console.log(err.status);
    //    }
    //  }
     )
    console.log(this.userData?.id);
    console.log(this.userData?.payment_id);
    console.log(this.updateRemarks);
  }

  searchData(){
  console.log(this.rowsData)
  }
  toggleAddVolunteer(){
   this.addVolDiv = !this.addVolDiv;
  }
  toggleDisplayDivIf() {  
    this.isShowDivIf = !this.isShowDivIf;  
  } 
 
  GetDataByQuery(reportId:any,query:any){  
  this.gridApi.api.showLoadingOverlay();

    this.rid =reportId;
    this.eventQuery =query;
   
    query["max-results"]=this.recordsPerPage;
    query["results-offset"]=0;
    this.trservice.getDataByQuery(reportId,query).subscribe((res : any)=>{
      if (res.serviceResult) { 
        // { this.toastr.info('Data Filtered','Export',{timeOut: 4000, })  }
        this.rowsData = res.serviceResult.results;
        let items =res.serviceResult.totalResults;
        this.totalItems =res.serviceResult.totalResults;
     
        this.totalRecords =Math.ceil(items / this.recordsPerPage) ;
    }
    })
  
  } 
  viewData(params: any, parent: any) {
    // alert(`${JSON.stringify(params.data)} - ${params.col} cellRenderedParams Clicked`);
    let rowData = params;

    // this.trservice.userData.next(rowData.data)
    // console.log(rowData);
    parent.http.get(`/api/portal/admin/get_kriya_member_details?id=${rowData.data.id}`).subscribe((res:any)=>{
        parent.trservice.userData.next(res.serviceResult)
    document.getElementById('previewData')?.click()
    })
  }
  editData(params: any, parent: any){
   
    let rowData = params;
   
    // this.trservice.userData.next(rowData.data)
    console.log(rowData);
    this.http.get(`/api/portal/admin/get_kriya_member_details?id=${rowData.data.id}`).subscribe((res:any)=>{
        this.trservice.userData.next(res.serviceResult)
        document.getElementById('previewData1')?.click()
    })
  }
  suspendMemberData(params: any, parent: any){
    let rowData = params;
   
    // this.trservice.userData.next(rowData.data)
    console.log(rowData);
     this.http.get(`/api/portal/admin/get_kriya_member_details?id=${rowData.data.id}`).subscribe((res:any)=>{
        this.trservice.userData.next(res.serviceResult)
        // this.componentParent.volunteerName = rowData.data.name;
        this.memberDetails = res.serviceResult;
        this.memberId = res.serviceResult.id;
        // this.memberAadharNumber = res.serviceResult.aadhar_number;
        // this.memberEmail = res.serviceResult.email;
        // this.memberMobile = res.serviceResult.mobile;
        // this.memberState = res.serviceResult.state;
        // this.memberDistrict = res.serviceResult.district_name;
        // this.memberConstituency = res.serviceResult.constituency_name;
        // this.memberAddress = res.serviceResult.address;
        // this.memberDob = res.serviceResult.dob;
        // this.memberAadharCard = res.serviceResult.aadhar_card;
        // this.memberGender = res.serviceResult.gender;
        // this.memberNomineeName = res.serviceResult.nominee_name;
        // this.memberNominiAadharNumber = res.serviceResult.nominee_aadhar_number;
        this.memberRemarks = res.serviceResult.remarks;
        console.log(this.memberDetails,"kmsusssssss")

        document.getElementById('previewMemberSuspend')?.click()

    })
    // alert('hey')
  }
  editMemberData(params: any, parent: any){
       let rowData = params;
  //  this.trservice.userData.next(rowData.data)
       console.log(rowData);
       this.http.get(`/api/portal/admin/get_kriya_member_details?id=${rowData.data.id}`).subscribe((res:any)=>{
       this.trservice.userData.next(res.serviceResult)
    // this.componentParent.volunteerName = rowData.data.name;
       this.memberDetails = res.serviceResult;
       this.memberName = res.serviceResult.name;
       this.memberAadharNumber = res.serviceResult.aadhar_number;
       this.memberEmail = res.serviceResult.email;
       this.memberMobile = res.serviceResult.mobile;
       this.memberState = res.serviceResult.state;
       this.memberDistrict = res.serviceResult.district_name;
       this.memberConstituency = res.serviceResult.constituency_name;
       this.memberAddress = res.serviceResult.address;
       this.memberDob = res.serviceResult.dob;
       this.memberAadharCard = res.serviceResult.aadhar_card;
       this.memberGender = res.serviceResult.gender;
       this.memberNomineeName = res.serviceResult.nominee_name;
       this.memberNominiAadharNumber = res.serviceResult.nominee_aadhar_number;
       this.memberRemarks = res.serviceResult.remarks;
       console.log(this.memberDetails)
       document.getElementById('previewMemberDataEdit')?.click()
       })
       }

       voterdb(params: any, parent: any){

     //this._jsonURL.voters.next(_jsonURL.voters)
     // let _jsonURL = this.params;
     //   return this.http.get(`this._jsonURL?id= ${this._jsonURL.voters.id}`).subscribe((res:any)=>{
     //     this.trservice.userData.next(res.serviceResult)
     //})
     // }
     // ;
   
     // this.trservice.userData.next(rowData.data)
    let rowData = params;

    console.log(rowData);
   // this.http.get(`/api/portal/admin/get_kriya_member_mapped_voters?id=${rowData.data.id}`).subscribe((res:any)=>{
      this.http.get(`/api/portal/admin/get_kriya_member_details?id=${rowData.data.id}`).subscribe((res:any)=>{
      this.trservice.userData.next(res.serviceResult)
      this.memberDetails = res.serviceResult;
      this.memberId = res.serviceResult.id;
      this.memberName = res.serviceResult.name;
      this.memberAadharNumber = res.serviceResult.aadhar_number;
      this.memberEmail = res.serviceResult.email;
      this.memberMobile = res.serviceResult.mobile;
      this.memberState = res.serviceResult.state;
      this.memberDistrict = res.serviceResult.district_name;
      this.memberConstituency = res.serviceResult.constituency_name;
      this.memberAddress = res.serviceResult.address;
      this.memberDob = res.serviceResult.dob;
      this.memberAadharCard = res.serviceResult.aadhar_card;
      this.memberGender = res.serviceResult.gender;
      this.memberNomineeName = res.serviceResult.nominee_name;
      this.memberNominiAadharNumber = res.serviceResult.nominee_aadhar_number;
      this.memberRemarks = res.serviceResult.remarks;
      console.log(this.memberDetails)
    //document.getElementById('previewvoterdata')?.click()
      })
      this.http.get(`/api/portal/admin/get_kriya_member_mapped_voters?id=${rowData.data.id}`).subscribe((res:any)=>{
      this.trservice.userData.next(res.serviceResult)
    //this.trservice.voterData.next(res.serviceResult[0]);
      document.getElementById('previewvoterdata')?.click()

      // this.trservice.open(res.serviceResult)
      // this.componentParent.voterData = res.serviceResult[0];
      console.log( this.voterData)
     
      // this.componentParent.memberDetails = res.serviceResult;
      // this.componentParent.voterName = res.serviceResult[0].name;
      // this.componentParent.voterAge = res.serviceResult[0].age;
      // this.componentParent.memberAadharNumber = res.serviceResult.aadhar_number;
      // this.componentParent.memberEmail = res.serviceResult.email;
      // this.componentParent.memberMobile = res.serviceResult.mobile;
      // this.componentParent.memberState = res.serviceResult.state;
      // this.componentParent.voterHno= res.serviceResult[0].house_no;
      // this.componentParent.voterMandal= res.serviceResult[0].mandal;
      // this.componentParent.voterDistrict = res.serviceResult[0].district_name;
      // this.componentParent.voterConstituency = res.serviceResult[0].constituency_name;
      // this.componentParent.voterPincode= res.serviceResult[0].pincode;
      // this.componentParent.voterPollingStationName = res.serviceResult[0].polling_station_name;
      // this.componentParent.voterPollingStationNo = res.serviceResult[0].polling_station_no;
      // this.componentParent.voterRelationType = res.serviceResult[0].relation_type;
      // this.componentParent.voterRelativeName = res.serviceResult[0].relative_name;
      // this.componentParent.voterSno = res.serviceResult[0].sno;
      // this.componentParent.voterVoterId = res.serviceResult[0].voter_id;
      // this.componentParent.voterVoters2022SeqId = res.serviceResult[0].voters2022_seq_id;
      // this.componentParent.memberAddress = res.serviceResult.address;
      // this.componentParent.memberDob = res.serviceResult.dob;
      // this.componentParent.memberAadharCard = res.serviceResult.aadhar_card;
      // this.componentParent.memberGender = res.serviceResult.gender;
      // this.componentParent.memberNomineeName = res.serviceResult.nominee_name;
      // this.componentParent.memberNominiAadharNumber = res.serviceResult.nominee_aadhar_number;
      // this.componentParent.memberRemarks = res.serviceResult.remarks;
      // console.log(this.memberDetails)

    })
  }
  printPetition(params: any, parent: any){
    let rowData = params;

 
    // console.log(rowData);=36
    // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
      this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.seq_id}`).subscribe((res:any)=>{
         this.trservice.petitionerData.next(res.serviceResult)
        this.petitionerData=res.serviceResult
        document.getElementById('previewAknowledgementPrintLater')?.click()
    })
    
    
    
  }
  viewPetition(params: any, parent: any){
    let rowData = params;
 
    // console.log(rowData);=36
    // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
      this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.seq_id}`).subscribe((res:any)=>{
         this.trservice.petitionerData.next(res.serviceResult)
        this.petitionerData=res.serviceResult
        document.getElementById('previewPetitonerData')?.click()
    })
    
    
  }
 deletePetition(params: any, parent: any){
 
      let rowData = params; 
      this.deletPetionDataId=rowData?.data?.seq_id;
      document.getElementById('deletPetitionData')?.click();
  
   
     
      // alert("delet")
    }
  editPetition(params: any, parent: any){
      let rowData = params;
   
      // console.log(rowData);=36
      // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
        this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.seq_id}`).subscribe((res:any)=>{
           this.trservice.petitionerData.next(res.serviceResult)
          this.petitionerData=res.serviceResult;
          this.editgrievanceform.patchValue({ ...res.serviceResult });
          document.getElementById('editPetitionData')?.click();

      })
      
     

      // alert("edit petition working")
    }
    deleteVisitor (params: any, parent: any){
      let rowData = params; 
      this.deletVisitorDataId=rowData?.data?.seq_id;
     
      document.getElementById('deletVisitorData')?.click();
  
     } 
     viewVisitor(params: any, parent: any){
      let rowData = params;
        this.http.get(`/api/portal/admin/get_vms_details_by_id?id=${rowData.data.seq_id}`).subscribe((res:any)=>{
           this.trservice.visitorData.next(res.serviceResult)

          this.visitorData=res.serviceResult
          document.getElementById('previewVisitorData')?.click()
      })
    } 
    editVisitor(params: any, parent: any){
      let rowData = params;
      // document.getElementById('editVissitorData')?.click();
     
      // console.log(rowData);=36api/portal/admin/update_vms_details
      // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
        this.http.get(`/api/portal/admin/get_vms_details_by_id?id=${rowData.data.seq_id}`).subscribe((res:any)=>{
           this.trservice.visitorData.next(res.serviceResult)
          this.visitorData=res.serviceResult;
          this.editvisitorform.patchValue({ ...res.serviceResult });
          if(this.isMail == this.visitorData.created_by){
          document.getElementById('editVisitorData')?.click()}
          else{
          //   this.toastr.showSuccess(message, title){
          //     this.toastr.success(message, title)
          // }
            this.toastr.warning('You Are Not Authorised Member','warning',{timeOut: 4000, positionClass: 'toast-top-center' })
          };
  
      })
     } 
     scheduleMeet(params: any, parent: any){
      let rowData = params;
      // document.getElementById('editVisitorData')?.click();
     
      // console.log(rowData);=36api/portal/admin/update_vms_details
      // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
        this.http.get(`/api/portal/admin/get_vms_details_by_id?id=${rowData.data.seq_id}`).subscribe((res:any)=>{
           this.trservice.visitorData.next(res.serviceResult)
          this.visitorData=res.serviceResult;
          this.setmeetform.patchValue({ ...res.serviceResult });
          document.getElementById('scheduleVisitorMeet')?.click();
  
      })
    }
    viewInsurance(params: any, parent: any){
      let rowData = params;
      // alert("hey view button works ") 
   
        this.http.get(`/api/portal/claims/get_claim_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
           this.trservice.insuranceData.next(res.serviceResult)
          this.insuranceData=res.serviceResult
          document.getElementById('previewInsuranceData')?.click()
      })
      
      
    }
    editInsurance(params: any, parent: any){
      let rowData = params;
  
       this.router.navigate(['kriya_insurance_form','edit',rowData.data.id])
       // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.seq_id}`).subscribe((res:any)=>{
       //    this.trservice.updateInsurance.next(res.serviceResult)
       //   this.petitionerData=res.serviceResult;
       //   this.componentParent.editgrievanceform.patchValue({ ...res.serviceResult });
         document.getElementById('editInsuranceData')?.click();

     // })
     
    

   }
   deleteInsurance(params: any, parent: any){
 
      
    let rowData = params;
    console.log(rowData.data)
    
   this.deletInsuranceDataId=rowData?.data?.id;
   document.getElementById('deletInsuranceData')?.click();

  }
  
  viewVolunteer(params: any, parent: any){
    
    let rowData = params;
 
    // console.log(rowData);=36
    // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
      this.http.get(`/api/portal/admin/get_kriya_volunteer_details?id=${rowData.data.id}`).subscribe((res:any)=>{
      
        this.kvservice.volunteerData.next(res.serviceResult)
        this.volunteerData=res.serviceResult
        console.log(this.volunteerData,'volunteerrrrrrrrrrrrr')
        document.getElementById('previewVolunteerData')?.click()
    })
  }
  EditVolunteer(params: any, parent: any){

    let rowData = params;
   
      this.http.get(`/api/portal/admin/get_kriya_volunteer_details?id=${rowData.data.id}`).subscribe((res:any)=>{
         this.kvservice.volunteerData.next(res.serviceResult)
        this.volunteerData=res.serviceResult;
        // this.getDistrictsJsonValun();
        
        this.getConsituents(res.serviceResult.district_id);
        this.editvolunteerform.patchValue({ ...res.serviceResult });
        document.getElementById('editVolunteerData')?.click()
       
  
    })
  }

  downloadSampleFile() {
    this.httpClient.get('./assets/volunteers.csv',{ responseType: 'blob'})
    .subscribe((res: any) => {
      const url = window.URL.createObjectURL(res);
     var anchor = document.createElement("a");
     anchor.download = "volunteers.csv";
     anchor.href = url;
     anchor.click();
    })
  }
  downloadBulkpaymentSampleFile() {
    this.httpClient.get('./assets/bulk_payments.csv',{ responseType: 'blob'})
    .subscribe((res: any) => {
      const url = window.URL.createObjectURL(res);
     var anchor = document.createElement("a");
     anchor.download = "bulk_payments.csv";
     anchor.href = url;
     anchor.click();
    })
  }
  editVolunteerSubmit(data:any){
    if(this.editvolunteerform.valid)
    {
      this.isSubmitedForm =true;
      this.kvservice.updateVoluneer(data,this.volunteerData.id).subscribe((res:any) =>{ 
       
         if(res.serviceResult)
         this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
  
this.IsmodelShow=false;
      //  this.editemployeeform.reset();
       this.isSubmitedForm =false;
       this.closebuttonEditvolunteer.nativeElement.click();
        this.reloadTable();

      
        },
         (error) => {  this.isSubmitedForm =false;  this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
       );
    }
  }
  viewEmployee(params: any, parent: any){
    let rowData = params;
 
    // console.log(rowData);=36
    // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
      this.http.get(`/api/portal/employees/get_employee_details_by_id?emp_id=${rowData.data.emp_id}`).subscribe((res:any)=>{
         this.trservice.employeeData.next(res.serviceResult)
        this.employeeData=res.serviceResult
        document.getElementById('previewEmployeeData')?.click()
    })
  }
  editEmployee(params: any, parent: any){
    let rowData = params;
    console.log(rowData,"rowwwwwwww1")
    // document.getElementById('editVissitorData')?.click();
   
    // console.log(rowData);=36api/portal/admin/update_vms_details
    // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
      this.http.get(`/api/portal/employees/get_employee_details_by_id?emp_id=${rowData.data.emp_id}`).subscribe((res:any)=>{
         this.trservice.employeeData.next(res.serviceResult)
        this.employeeData=res.serviceResult;
        this.editemployeeform.patchValue({ ...res.serviceResult });
        document.getElementById('editEmployeeData')?.click()
       
  
    })
  }
  qrCode1(params: any, parent: any){
   
    let rowData = params;
    debugger
    console.log(rowData,'rowwwwwwww122')
      this.http.get(`/api/portal/employees/get_employee_qrcode_by_id?emp_id=${rowData.data.emp_id}`).subscribe((blob:any)=>{
      
        // console.log(res.blob())
        const blobUrl = URL.createObjectURL(blob);
      const imgElement = document.getElementById('blob-image') as HTMLImageElement;
      imgElement.src = blobUrl;
        // const blob = new Blob([res], { type: 'image/png' });

        // Create a URL for the blob
        const url = window.URL.createObjectURL(blob);
  
        // Create a download link
        const a = document.createElement('a');
        a.href = url;
        a.download = 'image.png'; // Specify the desired file name
  
        // Trigger the download
        a.click();
  

        // Clean up the URL object to free up resources
        // window.URL.revokeObjectURL(url);

        // this.trservice.empQrData.next(res.serviceResult)
        // this.empQrData=res.serviceResult
        // console.log('qr data',this.empQrData)
        // console.log('qr data',this.empQrData)
      
        // document.getElementById('previewQrData')?.click()
    })
  }
  qrCode(params: any){
    
    let rowData = params;
     this.trservice.getBlobData(rowData.data.emp_id).subscribe(async (blob: Blob) => {
      const reader = new FileReader();
      reader.onloadend = () =>{
        this.qrCodeImage = reader.result as string
      };
      reader.readAsDataURL(blob);
      this.qrCodeUrl = window.URL.createObjectURL(blob);
      document.getElementById('previewQrData')?.click()
     // return await blob.text();
      // Do something with the blob data, e.g., display it in an <img> element
      // const blobUrl = URL.createObjectURL(blob);
      // const imgElement = document.getElementById('blob-image') as HTMLImageElement;
      // imgElement.src = blobUrl;
    });
  
  }
  downloadQrCode()
  {
   const a =document.createElement('a');
   a.href = this.qrCodeUrl;
   a.download = 'qrcode.png';
   a.click();
   window.URL.revokeObjectURL(this.qrCodeUrl);

  }
 
  // qrCode(params: any){
  //     let rowData = params;
  //   this.trservice.getBlobDataFromAPI(rowData.data.emp_id).subscribe(blob => {
  //     this.trservice.blobToDataURL(blob).then((dataURL :any) => {
  //       this.blobDataURL = dataURL;
  //     });
  //   });
  // }

  // qrCode3(blob: Blob): Promise<string> {
  //   return new Promise((resolve, reject) => {
  //     const reader = new FileReader();
  //     reader.onloadend = () => {
  //       resolve(reader.result as string);
  //     };
  //     reader.onerror = reject;
  //     reader.readAsDataURL(blob);
  //   });
  // }

  async readBlobAsText(blob: Blob): Promise<string> {
    return await blob.text();
  }


  employeePhoto(params: any, parent: any){
    
   
    let rowData = params;
      this.http.get(`/api/portal/employees/get_employee_details_by_id?emp_id=${rowData.data.emp_id}`).subscribe((res:any)=>{
         this.trservice.employeeData.next(res.serviceResult)
        this.employeeData=res.serviceResult
        document.getElementById('previewEmployeePhotoData')?.click()
    })
  }
  deleteEmployee(params: any, parent: any){
    let rowData = params;  
    
    this.deleteEmployeeDataId=rowData?.data?.emp_id;  
    document.getElementById('deleteEmployeeData')?.click();
  
  }
  SendLink(params: any, parent: any){
    let rowData = params;
       this.rowData1 = rowData
       console.log(this.params, 'custom2---------------------');
    this.kvservice.SendLink(rowData.data.id).subscribe((res:any) =>{ 
      console.log(res)
         if(res.serviceResult){
           this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, })
         }else {
           console.log('error')
           this.toastr.error(res.serviceError,'Error',{timeOut: 4000, })
         }
       }, 

     ) }
     VolunteerActivate(params: any, parent: any){
      let rowData =params;
      this.kvservice.Suspend(rowData.data.id , 'Active').subscribe((res:any) =>{ 
        console.log(res)
           if(res.serviceResult){
             this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, })
           }else {
             console.log('error')
             this.toastr.error(res.serviceError,'Error',{timeOut: 4000, })
           }
         }, 

       ) }
       VolunteerSuspend(params: any, parent: any){
        let rowData = params;
        this.kvservice.Activate(rowData.data.id , 'Suspend').subscribe((res:any) =>{ 
          console.log(res)
             if(res.serviceResult){
               this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, })
             }else {
               this.toastr.error(res.serviceError,'Error',{timeOut: 4000, })
             }
           }, 

         ) }

         editVData(params: any, parent: any){
          let rowData = params;
         
          // this.trservice.userData.next(rowData.data)
          console.log(rowData);
          this.http.get(`/api/portal/admin/get_kriya_member_details?id=${rowData.data.id}`).subscribe((res:any)=>{
              this.trservice.userData.next(res.serviceResult)
              document.getElementById('previewDataEdit')?.click()
              this.volunteerName = rowData.data.name;
              // this.componentParent.volunteerName = res.serviceResult.name;
          })
          
        }
  getMetaData(reportId:any){ 
    this.trservice.getMetaData(reportId).subscribe((res: any) => {
        if (res.data.status) { 
          var tabularReportComponent = this;
            this.columnsMetadata = res.data.metadata;
            if(this.hasActions){
            this.columnsMetadata.push(
              {
                headerName: "View",
                field: "Action",
                width:  110,
                cellRendererFramework: ClickableParentComponent,
                cellRendererParams: {
                  buttonText: "View",
                  btNgClass: ['btn', 'btn-outline-success'],
                  iconNgClass: ["fa",  "fa-eye"],
                  clicked: (params: any) => {
                    //add string tilt
                    this[`${'viewData'}`](params, this);
                  }
                
                }
              }
              );
            this.columnsMetadata.push({
              headerName: "Edit",
              field: "Action",
              width:  110,
              cellRendererFramework: ClickableParentComponent,
                cellRendererParams: {
                  buttonText: "Edit",
              
                  btNgClass: ['btn', 'btn-outline-warning'],
                  iconNgClass: ["fa",  "fa-edit"],
                  clicked: (params: any) => {
                    this.editData(params, this);
                  },
                 
                  
                }, 
              

            });
            if (this.isRole == 'AD'){
              this.columnsMetadata.push({
                headerName: "Suspend",
                field: "suspendMember" ,
                width:  150,
                cellRendererFramework: ClickableParentComponent,
                  cellRendererParams: {
                    buttonText: "Suspend",
                    btNgClass: ['btn', 'btn-outline-danger'],
                    iconNgClass: ["fa",  "fa-ban"],
                    clicked: (params: any) => {
                      this.suspendMemberData(params, this);
                    }
                  }
              });
            }
           if (this.isRole!== 'UE'){
            this.columnsMetadata.push({
              headerName: "Edit Member",
              field: "editMember" ,
              width:  150,
              cellRendererFramework: ClickableParentComponent,
                cellRendererParams: {
                  buttonText: "Edit Member",
                  btNgClass: ['btn', 'btn-outline-warning'],
                  iconNgClass: ["fa",  "fa-edit"],
                  clicked: (params: any) => {
                    this.editMemberData(params, this);
                  }
                }
            });
          }
            }

            if(this.hasVoterDbActions){
              this.columnsMetadata.push({
                headerName: "Get Mapped Voters",
                field: "voterdb",
                cellRendererFramework: ClickableParentComponent,
                  cellRendererParams: {
                    buttonText: "voterdb",
                    btNgClass: ['btn', 'btn-outline-warning'],
                    iconNgClass: ["fa",  "fa-edit"],
                    clicked: (params: any) => {
                      this.voterdb(params, this);
                    }
                  }
              });
            }
            if(this.hasIssueActions){
              // this.columnsMetadata.push({
              //   headerName: "print/Download Issue",
              //   field: "Printissue" ,
              //   width:  160,
              //   cellRendererFramework: ClickableParentComponent,
              //     cellRendererParams: {
              //       buttonText: "Print/Download",
              //       btNgClass: ['btn', 'btn-outline-success'],
              //       iconNgClass: ["fa",  "fa-download"],
              //       clicked: (params: any) => {
              //         this.printPetition(params, this);
              //       }
              //     }
              // });
              this.columnsMetadata.push({
                headerName: "View/Edit Issue",
                field: "Vieweditissue" ,
                width:  120,
                cellRendererFramework: ClickableParentComponent,
                  cellRendererParams: {
                    buttonText: "view",
                    btNgClass: ['btn', 'btn-outline-success'],
                    iconNgClass: ["fa",  "fa-eye"],
                    clicked: (params: any) => {
                      this.viewIssue(params, this);
                    }
                  }
              });
              this.columnsMetadata.push({
                headerName: "View/Edit Issue",
                field: "Vieweditissue" ,
                width:  120,
                cellRendererFramework: ClickableParentComponent,
                  cellRendererParams: {
                    buttonText: "Edit",
                    btNgClass: ['btn', 'btn-outline-warning'],
                    iconNgClass: ["fa",  "fa-pencil"],
                    clicked: (params: any) => {
                      this.editIssue(params, this);
                    }
                  }
              });
              this.columnsMetadata.push({
                headerName: "Delete Issue",
                field: "Deleteissue" ,
                width:  120,
                cellRendererFramework: ClickableParentComponent,
                  cellRendererParams: {
                    buttonText: "Delete",
                    btNgClass: ['btn', 'btn-outline-danger'],
                    iconNgClass: ["fa",  "fa-trash"],
                    clicked: (params: any) => {
                      this.deleteIssue(params, this);
                    }
                  }
              });
            }
            if(this.hasConflictActions){
              this.columnsMetadata.push({
                headerName: "View/Edit Issue",
                field: "Vieweditissue" ,
                width:  120,
                cellRendererFramework: ClickableParentComponent,
                  cellRendererParams: {
                    buttonText: "view",
                    btNgClass: ['btn', 'btn-outline-success'],
                    iconNgClass: ["fa",  "fa-eye"],
                    clicked: (params: any) => {
                      this.viewConflict(params, this);
                    }
                  }
              }); 
              this.columnsMetadata.push({
                headerName: "View/Edit Conflict",
                field: "Vieweditconflict" ,
                width:  120,
                cellRendererFramework: ClickableParentComponent,
                  cellRendererParams: {
                    buttonText: "Edit",
                    btNgClass: ['btn', 'btn-outline-warning'],
                    iconNgClass: ["fa",  "fa-pencil"],
                    clicked: (params: any) => {
                      this.editConflict(params, this);
                    }
                  }
              });
              this.columnsMetadata.push({
                headerName: "Delete Conflict",
                field: "Deleteconflict" ,
                width:  120,
                cellRendererFramework: ClickableParentComponent,
                  cellRendererParams: {
                    buttonText: "Delete",
                    btNgClass: ['btn', 'btn-outline-danger'],
                    iconNgClass: ["fa",  "fa-trash"],
                    clicked: (params: any) => {
                      this.deleteConflict(params, this);
                    }
                  }
              });
            }

              if(this.hasPetitionActions){
              this.columnsMetadata.push({
                headerName: "print/Download Petition",
                field: "Printpetition" ,
                width:  160,
                cellRendererFramework: ClickableParentComponent,
                  cellRendererParams: {
                    buttonText: "Print/Download",
                    btNgClass: ['btn', 'btn-outline-success'],
                    iconNgClass: ["fa",  "fa-download"],
                    clicked: (params: any) => {
                      this.printPetition(params, this);
                    }
                  }
              });
              this.columnsMetadata.push({
                headerName: "View/Edit Petition",
                field: "Vieweditpetition" ,
                width:  120,
                cellRendererFramework: ClickableParentComponent,
                  cellRendererParams: {
                    buttonText: "view",
                    btNgClass: ['btn', 'btn-outline-success'],
                    iconNgClass: ["fa",  "fa-eye"],
                    clicked: (params: any) => {
                      this.viewPetition(params, this);
                    }
                  }
              });
              this.columnsMetadata.push({
                headerName: "View/Edit Petition",
                field: "Vieweditpetition" ,
                width:  120,
                cellRendererFramework: ClickableParentComponent,
                  cellRendererParams: {
                    buttonText: "Edit",
                    btNgClass: ['btn', 'btn-outline-warning'],
                    iconNgClass: ["fa",  "fa-pencil"],
                    clicked: (params: any) => {
                      this.editPetition(params, this);
                    }
                  }
              });
              this.columnsMetadata.push({
                headerName: "Delete Petition",
                field: "Deletepetition" ,
                // width:  120,
                cellRendererFramework: ClickableParentComponent,
                  cellRendererParams: {
                    buttonText: "Delete",
                    btNgClass: ['btn', 'btn-outline-danger'],
                    iconNgClass: ["fa",  "fa-trash"],
                    clicked: (params: any) => {
                      this.deletePetition(params, this);
                    }
                  }
              });
            }
            if(this.hasVisitorActions){
              if (this.isRole == 'AD') {
                this.columnsMetadata.unshift({
                  headerName: "Delete Visitor",
                  field: "Deletvisitor" ,
                  width:  120,
                  cellRendererFramework: ClickableParentComponent,
                    cellRendererParams: {
                      buttonText: "Delete",
                      btNgClass: ['btn'],
                      iconNgClass: ["fa",  "fa-trash"],
                      customStyles:{padding:'0px', color: '#dd3c4c'},

                      clicked: (params: any) => {
                        this.deleteVisitor(params, this);
                      }
                    }
                });
              }
              console.log(this.isRole,'roleeeeeeeee')
              if (this.isRole ==  "PA" || this.isRole == "VS"|| this.isRole == "AD" ) {
                this.columnsMetadata.unshift({
                  headerName: "View Visitor",
                  field: "Viewvisitor" ,
                  width:  120,
                  cellRendererFramework: ClickableParentComponent,
                    cellRendererParams: {
                      buttonText: "View",
                      btNgClass: ['btn'],
                      iconNgClass: ["fa",  "fa-eye"],
                      customStyles:{padding:'0px', color: '#dd3c4c'},

                      clicked: (params: any) => {
                        this.viewVisitor(params, this);
                      }
                    }
                });
              }
              if (this.isRole == 'PA'||this.isRole == 'AD' || this.isRole == 'VS') {
                this.columnsMetadata.unshift({
                  headerName: "Edit Visitor",
                  field: "Editvisitor" ,
                  width:  120,
                  cellRendererFramework: ClickableParentComponent,
                    cellRendererParams: {
                      buttonText: "Edit",
                      btNgClass: ['btn'],
                      iconNgClass: ["fa",  "fa-edit"],
                      customStyles:{padding:'0px', color: '#ffc107'},

                      clicked: (params: any) => {
                        this.editVisitor(params, this);
                      }
                    }
                });
              }
             if (this.isRole == 'PA'|| this.isRole == 'AD') {
                this.columnsMetadata.unshift({
                  headerName: "Schedule Meet",
                  field: "Schedulemeet" ,
                  width:  120,
                  cellRendererFramework: ClickableParentComponent,

                    cellRendererParams: {
                      // if(this.param.data.appointment_status!=='scedule'),
                      btNgClass: ['btn'],
                      iconNgClass: ["fa",  "fa-clock-o"],
                      customStyles:{padding:'0px',width: '110px', color: '#e5750c'},
                      buttonText: "Schedule",
                    // buttonText: (rowData: any) => {
                   //console.log(rowData.appointment_status,'testttttttttttttttt')
                    // return rowData.appointment_status === '1' ? 'Schedule' : 'Rechedule'},  
                  //  return rowData.appointment_status ! == 'scheduled' ? 'Schedule' : 'Rechedule'},  
                  //  [this.row.data.appointment_status!]== 'scheduled' ? "Schedule" : "Re-Schedule",
                       // return rowData.status === 'Active' ? 'Suspend' : 'Activate'
                     
                       
                   // },
                    clicked: (params: any) => {
                      this.scheduleMeet(params, this);
                    }
                    
                }
              });
               }
            }
            if(this.hasInsuranceActions){
              this.columnsMetadata.push({
                headerName: "View/Edit Insurance",
                field: "Vieweditinsurance" ,
                width:  120,
                cellRendererFramework: ClickableParentComponent,
                  cellRendererParams: {
                    buttonText: "view",
                    btNgClass: ['btn', 'btn-outline-success'],
                    iconNgClass: ["fa",  "fa-eye"],
                    clicked: (params: any) => {
                      this.viewInsurance(params, this);
                    }
                  }
              });
              this.columnsMetadata.push({
                headerName: "View/Edit Insurance",
                field: "Vieweditinsurance" ,
                width:  120,
                cellRendererFramework: ClickableParentComponent,
                  cellRendererParams: {
                    buttonText: "Edit",
                    btNgClass: ['btn', 'btn-outline-warning'],
                    iconNgClass: ["fa",  "fa-edit"],
                    clicked: (params: any) => {
                      this.editInsurance(params, this);
                    }
                  }
              });
              this.columnsMetadata.push({
                headerName: "Delet Insurance",
                field: "Deletinsurance" ,
                width:  120,
                cellRendererFramework: ClickableParentComponent,
                  cellRendererParams: {
                    buttonText: "Delete",
                    btNgClass: ['btn', 'btn-outline-danger'],
                    iconNgClass: ["fa",  "fa-trash"],
                    clicked: (params: any) => {
                      this.deleteInsurance(params, this);
                    }
                  }
              });
            }
            if(this.hasEmployeeActions){
              this.columnsMetadata.push({
                headerName: "View/Edit Employee",
                field: "Vieweditemployee" ,
                width:  120,
                cellRendererFramework: ClickableParentComponent,
                  cellRendererParams: {
                    buttonText: "View",
                    btNgClass: ['btn', 'btn-outline-success'],
                    iconNgClass: ["fa",  "fa-eye"],
                    clicked: (params: any) => {
                      this.viewEmployee(params, this);
                    }
                  }
              });
              this.columnsMetadata.push({
                headerName: "View/Edit Employee",
                field: "Vieweditemployee" ,
                width:  120,
                cellRendererFramework: ClickableParentComponent,
                  cellRendererParams: {
                    buttonText: "Edit",
                    btNgClass: ['btn', 'btn-outline-warning'],
                    iconNgClass: ["fa",  "fa-edit"],
                    clicked: (params: any) => {
                      this.editEmployee(params, this);
                    }
                  }
              });
              this.columnsMetadata.push({
                headerName: "QR-code/Photo",
                field: "Qrcodephoto" ,
                width:  120,
                cellRendererFramework: ClickableParentComponent,
                  cellRendererParams: {
                    buttonText: "Open",
                    btNgClass: ['btn', 'btn-outline-info'],
                    iconNgClass: ["fa",  "fa-image"],
                    clicked: (params: any) => {
                      this.qrCode(params);
                    }
                  }
              });
              this.columnsMetadata.push({
                headerName: "Delete Employee",
                field: "Deletemployee" ,
                width:  120,
                cellRendererFramework: ClickableParentComponent,
                  cellRendererParams: {
                    buttonText: "Delete",
                    btNgClass: ['btn', 'btn-outline-danger'],
                    iconNgClass: ["fa",  "fa-trash"],
                    clicked: (params: any) => {
                      this.deleteEmployee(params, this);
                    }
                  }
              });
            }
            
            if(this.hasVolunteerActions ){
             
              if(this.isRole === 'KM' || this.isRole === 'AD'){
                this.columnsMetadata.push({
                  headerName: "Send Link",
                  field: "sendlink" ,
                  width:  90,
                  cellRendererFramework: ClickableParentComponent,
                    cellRendererParams: {
                      buttonText: "Send",
                      btNgClass: ['btn', 'btn-outline-info'],
                      // iconNgClass: ["fas",  "fa-sms"],
                      clicked: (params: any) => {
                        this.SendLink(params, this);
                      }
                    }
                });
              }
             
              
            if(this.isRole === 'AD'|| this.isRole === 'KM')
            
            {
              this.columnsMetadata.push({
                headerName: "Active/Suspend",

                field: "Actions" ,
                width:  120,
                cellRendererFramework: ClickableParentComponent,
                                  cellRendererParams: {

                    buttonText: (rowData: any) => {
                    //  Boards.status==1 ? "Approved": (Boards.status==2?"Pending":"Rejected") 
                      return rowData.status === 'Active' ? 'Suspend' : 'Activate'
                    },// "Activate",//rowValue.isActive ? 'Activate' : 'Suspend
                    btNgClass:  (rowData: any) => {
                      
                      return rowData.status === 'Active' ? ['btn', 'btn-outline-danger'] : ['btn', 'btn-outline-success']
                    },// ['btn', 'btn-outline-success'],
                    // iconNgClass:  (rowData: any) => {
                    //   return rowData.status === 'Active' ?["fa",  "fa-toggle-off"]:["fa",  "fa-ban"]},
                    clicked: (params: any) => {
                      if(params.data.status === "Active") {
                        this.VolunteerSuspend(params, this);
                        
                      } else {
                        this.VolunteerActivate(params, this);
                      }
                    }
                  }
                
                
               
              });
            }
            // if(this.isRole !== 'AC')
            // if(this.isRole === 'KM' || this.isRole === 'AD')
            // {
            //   this.columnsMetadata.push({
            //     headerName: "Active/Suspend",
            //     field: "Actions" ,
            //     width:  120,
            //     cellRendererFramework: ClickableParentComponent,
            //       cellRendererParams: {
            //         buttonText: "Suspend",
            //         btNgClass: ['btn', 'btn-outline-danger'],
            //         iconNgClass: ["fa",  "fa-ban"],
            //         clicked: (params: any) => {
            //           this.VolunteerSuspend(params, this);
            //         }
            //       }
            //   });
            // }
            // this.columnsMetadata.push({
            //   headerName: "Edit Volunteer Name",
            //   field: "EditVolunteer" ,
            //   width:  150,
            //   cellRendererFramework: ClickableParentComponent,
            //     cellRendererParams: {
            //       buttonText: "Edit Volunteername",
            //       btNgClass: ['btn', 'btn-outline-warning'],
            //       iconNgClass: ["fa",  "fa-edit"],
            //       clicked: (params: any) => {
            //         this.editVData(params, this);
            //       }
            //     }
            // });
            this.columnsMetadata.push({
              headerName: "View Volunteer",
              field: "ViewVolunteer" ,
              width:  120,
              cellRendererFramework: ClickableParentComponent,
                cellRendererParams: {
                  buttonText: "View",
                  btNgClass: ['btn', 'btn-outline-success'],
                  iconNgClass: ["fa",  "fa-eye"],
                  clicked: (params: any) => {
                    this.viewVolunteer(params, this);
                  }
                }
            });
           if(this.isRole === 'KM' || this.isRole === 'AD')
            
         {
            this.columnsMetadata.push({
              headerName: "Edit Volunteer",
              
              field: "EditVolunteer" ,
              width:  120,
              cellRendererFramework: ClickableParentComponent,
                cellRendererParams: {
                  buttonText: "Edit",
                  btNgClass: ['btn', 'btn-outline-warning'],
                  iconNgClass: ["fa",  "fa-edit"],
                  clicked: (params: any) => {
                    this.EditVolunteer(params, this);
                  }
                }
            });
          }
            if(this.isRole ==='AD'){
              this.columnsMetadata.push({
                headerName: "Delete Volunteer",
                field: "deletevolunteer" ,
                width:  120,
                cellRendererFramework: ClickableParentComponent,
                  cellRendererParams: {
                    buttonText: "Delete",
                    btNgClass: ['btn', 'btn-outline-danger'],
                    iconNgClass: ["fa",  "fa-trash"],
                    clicked: (params: any) => {
                      this.deleteVolunteerId(params, this);
                    }
                  }
              });
            }
          }
            


            if(this.hasActions){
              // this.columnsMetadata.push({headerName: 'Action', field: 'Action', cellRendererFramework: CellCustomComponent })
              // this.columnsMetadata.push({headerName: 'Edit', field: 'editMember', cellRendererFramework: CellCustomComponent  })
              // this.columnsMetadata.push({headerName: 'Get Mapped Voters', field: 'voterdb', cellRendererFramework: CellCustomComponent  })

            }
            // this.columnsMetadata.push({headerName: 'Active/Suspend', field: 'Actions', cellRendererFramework: BtnCellRenderer  })
            // this.columnsMetadata.push({headerName: 'Active/Suspend', field: 'Actions', cellRendererFramework: CellCustomComponent1  })
            //correct one
            if(this.hasVolunteerActions){
              // this.columnsMetadata.push({headerName: 'Action', field: 'Action', cellRendererFramework: CellCustomComponent1  })
             //edit volunteer 
              // this.columnsMetadata.push({headerName: 'Edit', field: 'Edit', cellRendererFramework: CellCustomComponent1  })
              // this.columnsMetadata.push( {headerName: 'Active/Suspend', field: 'Actions', cellRendererFramework: CellCustomComponent1})
            }
            if(this.hasVoterDbActions){
              // this.columnsMetadata.push({headerName: 'Get Mapped Voters', field: 'voterdb', cellRendererFramework: CellCustomComponent  })
            }
            if(this.hasInsuranceActions){
              // this.columnsMetadata.push({headerName: 'View/Edit Insurance', field: 'Vieweditinsurance', cellRendererFramework: CellCustomComponent  })
              // this.columnsMetadata.push({headerName: 'Delet Insurance', field: 'Deletinsurance', cellRendererFramework: CellCustomComponent  })
            }
            if(this.hasPetitionActions){
              // this.columnsMetadata.push({headerName: 'print/Download Petition', field: 'Printpetition', cellRendererFramework: CellCustomComponent  })
              // this.columnsMetadata.push({headerName: 'View/Edit Petition', field: 'Vieweditpetition', cellRendererFramework: CellCustomComponent  })
              // this.columnsMetadata.push({headerName: 'Delet Petition', field: 'Deletpetition', cellRendererFramework: CellCustomComponent  })
            }
            if(this.hasVisitorActions){
          // if (this.isRole == 'AD') {this.columnsMetadata.unshift({headerName: 'Delet Visitor', field: 'Deletvisitor', cellRendererFramework: CellCustomComponent  , width:120 })}
          // if (this.isRole === 'PA'|| this.isRole === 'AD'|| this.isRole === 'VS' )  {this.columnsMetadata.unshift({headerName: 'Edit Visitor', field: 'Editvisitor', cellRendererFramework: CellCustomComponent  , width:120 })}
          // if (this.isRole === 'PA'|| this.isRole === 'AD' || this.isRole === 'VS')  {this.columnsMetadata.unshift({headerName: 'View Visitor', field: 'Viewvisitor', cellRendererFramework: CellCustomComponent , width: 120  })}
          // if (this.isRole == 'PA'|| this.isRole == 'AD') { this.columnsMetadata.unshift({headerName: 'Schedule Meet', field: 'Schedulemeet', cellRendererFramework: CellCustomComponent  , width: 120 })}
            }
            if(this.hasEmployeeActions){
              // this.columnsMetadata.push({headerName: 'View/Edit Employee', field: 'Vieweditemployee', cellRendererFramework: CellCustomComponent  })
              // this.columnsMetadata.push({headerName: 'QR-code/Photo', field: 'Qrcodephoto', cellRendererFramework: CellCustomComponent  })
              // this.columnsMetadata.push({headerName: 'Delete Employee', field: 'Deletemployee', cellRendererFramework: CellCustomComponent  })
           
            }
            // if ((this.isName=='pavan garu'&& this.receiptData.created_by=='tech4@janasenaparty.org')||(this.isName=='pavan garu'&& this.receiptData.created_by=='tech4@janasenaparty.org') )
           // this.columnsMetadata.push( {headerName: 'select', field: 'select', cellRenderer: this.selectCellRenderer})
          //   this.columnsMetadata.push({
          
          //   field: "athlete",
          //   cellRenderer: "btnCellRenderer",
          //   cellRendererParams: {
          //     clicked: function() {
          //       alert(` was clicked`);
          //     }
          //   },
          //   minWidth: 150
          // }); 
          // this.frameworkComponents = {
          //   btnCellRenderer: BtnCellRenderer
          // };
           
        }

    })
  }


  
  // selectCellRenderer(params:any) {
  //   return '<span *ngIf="columnsMetadata.status=='Active'" class="mt-5 submit-btn text-end">
  //    <h2 style="color:green">Accepted</h2>
  //    </span>'
  // }
  onChangePageLimit(pagelimit:any){
    var x = pagelimit;
    var y: number = +pagelimit;
     this.recordsPerPage =  y;
 
     this.getQuery(1);
     this.onGoTo(1)
  }
  // deleteRow(){
  //   this.trservice.deleteRow().subscribe((res:any)=>{
  //     window.location.reload()
  //     alert('delete success')

  //  })
    
  // }

  // deleteRow(){
  //   this.trservice.deleteRow().subscribe((res:any)=>{
  //     alert('delete success')
  //     this.refresh()
  //  })
    
  // }
 
  refresh(): void {
    window.location.reload();
}
  onExportClick(payload:any){
    console.log("Export payload",payload)
    this.trservice.getDataExport(this.reportId,payload).subscribe((res : any)=>{
      if (res.data) { this.toastr.info(res.data.message,'Export',{timeOut: 4000, })  }
  })
  }
  selectedRules: any;
  onFilterClick(payload:any){
    this.selectedRules = payload;
    console.log("Filter payload",payload)
    this.GetDataByQuery(this.reportId,payload);

  }
  onResetClick(payload:any){      
    // this.GetDataByQuery(this.reportId,payload);
    this.GetDataByQuery(this.reportId,this.defaultQuery)

  } 
  onGridReady(params:any) {
    this.gridApi= params;
    this.defaultQuery ={"rules":[]}
    this.rowsData =[];
    this.columnsMetadata =[];
    this.GetDataByQuery(this.reportId,this.defaultQuery)
    this.getMetaData(this.reportId);
  }
  pageChange(newPage: number) {
    console.log(newPage)
  }
  public onGoTo(page: number): void {
    this.current = page;
    this.getQuery( this.current);
  }

  public onNext(page: number): void {
    this.current = page + 1;
    this.getQuery( this.current);
  }

  public onPrevious(page: number): void {
    this.current = page - 1;
    this.getQuery( this.current);
  }

 public getQuery(offset:number)
 {
  this.gridApi.api.showLoadingOverlay();
  let query:any = this.eventQuery;
  query["max-results"]= <number> this.recordsPerPage;
  query["results-offset"]=(offset-1)*this.recordsPerPage;
  this.trservice.getDataByQuery(this.rid,query).subscribe((res : any)=>{
    this.loading = false; 

    if (res.serviceResult) { 
      
      this.rowsData = res.serviceResult.results;
      let items =res.serviceResult.totalResults;
      // { this.toastr.info('Data Filtered','Filtered',{timeOut: 4000, })  }

      this.totalRecords =Math.ceil(items / this.recordsPerPage) ;
      this.gridApi.api.hideOverlay();
  }
  })  
 }

 updateVolunteerName(){
   let Volname = this.volunteerName;
   //Make api call to save updated name;
   console.log(Volname)
 }

 updateNomineeMemberDetails(){
  this.trservice.updateNomineeMemberDetails(this.userData?.id,this.memberNomineeName, this.memberNominiAadharNumber).subscribe((res:any) =>{ 
  
    // this.memberState,this.memberDistrict,this.memberConstituency
    console.log(res)
    // this.GetDataByQuery(this.reportId,this.defaultQuery)
// this.onResetClick.emit(this.selectedRules)

       if(res.serviceResult){
         this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-center'  })
       }else {
         this.toastr.error(res.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center'  })
       }
     }, 
 
   )

 }
 SuspendMember(){
  this.loading = true;
  // change updateMemberDetails to ***
  this.trservice.suspendKMS(this.userData?.id,this.suspended_by, this.reason,this.uploadKMSFiles).subscribe((res:any) =>{ 
    this.loading = false; 
    console.log(res)
       if(res.serviceResult){
         this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-center' })
       }else {
         this.toastr.error(res.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' })
       }
     }, 
 
   )
 }
 updateMemberDetails(){
  this.loading = true; 
  this.trservice.updateMemberDetails(this.memberRemarks,this.userData?.id,this.memberDob, this.memberName,this.memberAadharNumber,this.memberEmail,this.memberMobile ,this.memberGender,this.memberAddress).subscribe((res:any) =>{ 
    this.loading = false; 
    // this.memberState,this.memberDistrict,this.memberConstituency
    console.log(res)
       if(res.serviceResult){
 
        // this.GetDataByQuery(this.reportId,this.defaultQuery)

         this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-center' })
       }else {
         this.toastr.error(res.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' })
       }
     }, 
 
   )
}
updateUserPhoto(){
  this.loading = true; 
  this.trservice.updateUserPhoto(this.userData?.id,this.type, this.user_photo,).subscribe((res:any) =>{ 
    this.loading = false; 
    console.log(res)
       if(res.serviceResult){
        this.fileupload ='';
        // this.GetDataByQuery(this.reportId,this.defaultQuery)

         this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-center' })
       }else {
         this.toastr.error(res.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' })
       }
     }, 
 
   )
}
fileChange(event:any) {
  let fileList: FileList = event.target.files;
  if(fileList.length > 0) {
      let file: File = fileList[0];
      let formData:FormData = new FormData();
      formData.append('uploadFile', file, file.name);
       this.user_photo = file;
    //  let headers = new Headers();
      /** In Angular 5, including the header Content-Type can invalidate your request */
    //  headers.append('Content-Type', 'multipart/form-data');
    //  headers.append('Accept', 'application/json');
  
  }
}
aadharChange(event:any) {
 
  let fileList1: FileList = event.target.files;
  if(fileList1.length > 0) {
   
      let file1: File = fileList1[0];
      let formData1:FormData = new FormData();
      formData1.append('uploadFile', file1, file1.name);
       this.aadhar_card = file1;
    //  let headers = new Headers();
      /** In Angular 5, including the header Content-Type can invalidate your request */
    //  headers.append('Content-Type', 'multipart/form-data');
    //  headers.append('Accept', 'application/json');
  
  }
}


updateAadharCard(){
  this.loading = true; 
  
  this.trservice.updateAadharCard(this.userData?.id,this.type, this.aadhar_card,).subscribe((res:any) =>{ 
    console.log(this.userData?.id,this.type, this.aadhar_card)
   
    this.loading = false; 
    console.log(res)
       if(res.serviceResult){
        this.fileupload2 ='';
        // this.GetDataByQuery(this.reportId,this.defaultQuery)

         this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-center' })
       }else {
         this.toastr.error(res.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' })
       }
     }, 
 
   )
}
//  updateMemberDetails(){
//   let Membername = this.memberDetails.name;
//   //Make api call to save updated name;
//   this.memberDob = this.memberDetails.name;
  
//   this.memberName = this.memberDetails.name;
//   this.memberAadharNumber = this.memberDetails.aadhar_number;
//   this.memberEmail = this.memberDetails.email;
//   this.memberMobile = this.memberDetails.mobile;
//   this.memberState = this.memberDetails.state;
//   this.memberDob =this.memberDetails.dob;
//   this.memberAadharCard = this.memberDetails.aadhar_card;
//   this.memberGender = this.memberDetails.gender;
//   this.memberNomineeName = this.memberDetails.nominee_name;
//   this.memberNominiAadharNumber = this.memberDetails.nominee_aadhar_number;

//   console.log(Membername)
//   console.log(this.memberAadharNumber)
// }

insuranceSubmit(){
  document.getElementById('previewinsurancesubmit')?.click()
}

printAcknowledgement(){
 // widow.document.getElementById('print1') 
  window.print();
  this.clearvalue();
  // this.receiptData.clearvalue
  // myWindow.focus();
}
addPetitionSubmit(data:any){
  

   if(this.grievanceform.valid)
  {
    this.isSubmitedForm =true;
    this.trservice.addPMS(data).subscribe((res:any) =>{ 
    
       if(res.serviceResult)
       this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
       document.getElementById('previewAknowledgementPrint')?.click()
      this.receiptData=res.serviceResult.result
      console.log(this.receiptData, '-----------------------hiiiiiiireceipt')
       this.IsmodelShow=false;
       this.grievanceform.reset();
       this.isSubmitedForm =false;
       this.closebutton.nativeElement.click();
        this.reloadTable();
      },
       (error) => {  this.isSubmitedForm =false;  this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
     );
  }
  
     }
     deletPetition(){
   console.log(this.deletPetionDataId)
   this.trservice.deletPetition(this.deletPetionDataId).subscribe((res:any) => {
    if(res.serviceResult){
       this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-center' })
     }else {
       this.toastr.error(res.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' })
     }
   }
    
    // (response) => {response.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' }},
    // (error) => { this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
  );

     }
    
     editPetitionSubmit(data:any){
      if(this.editgrievanceform.valid)
      {
        this.isSubmitedForm =true;
        this.trservice.updatePms(data,this.petitionerData.seq_id).subscribe((res:any) =>{ 
        
           if(res.serviceResult)
           this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
             
           this.IsmodelShow=false; 
           this.isSubmitedForm =false;
           this.closebutton.nativeElement.click();
            this.reloadTable();
          },
           (error) => {  this.isSubmitedForm =false;  this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
         );
      }
        }
   
        
        deletInsurance(){
          console.log(this.deletInsuranceDataId)
          this.trservice.deletInsurance(this.deletInsuranceDataId).subscribe((res:any) => {
           if(res.serviceResult){
              this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-center' })
              this.reloadTable();
           
            }else {
              this.toastr.error(res.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' })
            }
          }
           
           // (response) => {response.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' }},
           // (error) => { this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
         );
       
            }

            addVisitorSubmit(data:any){
             this.submitted = true;
              if(this.visitorform.valid)
             {
              if(data.visting_location =='Other')
              {
                data.visting_location = this.otherOptionValueVisitorForm;
              }
              data.additional_members=this.dynamicArray;
              console.log(this.visitorform.value,"..............hey visitor")
               this.isSubmitedForm =true;
               this.trservice.addVMS(data).subscribe((res:any) =>{ 
                  if(res.serviceResult)
                  this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
                 this.receiptData=res.serviceResult.result
                 this.dynamicArray = [];
                 this.IsmodelShow=false;
                //  this.closebuttonAddVisitor.nativeElement.click();
                 this.visitorform.reset();
                 this.isSubmitedForm =false;
                // document.getElementById('addvisitorModalOpen')?.click()

                  // this.reloadTable();
              
             
                 },
                  (error) => { this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
                );
             }
             
                }
    
                deletVisitor(){
                  console.log(this.deletVisitorDataId)
                  this.trservice.deletVisitor(this.deletVisitorDataId).subscribe((res:any) => {
                   if(res.serviceResult){
                      this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-center' })
                      this.reloadTable();
                    }else {
                      this.toastr.error(res.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' })
                    }
                  }
                   
                   // (response) => {response.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' }},
                   // (error) => { this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
                 );
               
                    }
                    editVisitorSubmit(data:any){
                      if(this.editvisitorform.valid)
                      {
                        this.isSubmitedForm =true;
                        this.trservice.updateVms(data,this.visitorData.seq_id).subscribe((res:any) =>{ 
                        
                           if(res.serviceResult)
                           this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
                           
                  //                   this.editvisitorform.patchValue(
                  //   {
                  //     kriya_member_id:res.serviceResult.kriya_member_id,
                  //     poc_mobile:res.serviceResult.poc_mobile,
                  //     mobile:res.serviceResult.mobile,
                  //   });
                  // this.visitorform.patchValue(res.serviceResult);
            
                  this.IsmodelShow=false;
                         this.editvisitorform.reset();
                         this.isSubmitedForm =false;
                         this.closebuttonEditVisitor.nativeElement.click();
                          this.reloadTable();
            
                        
                          },
                           (error) => {  this.isSubmitedForm =false;  this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
                         );
                      }
                
                    
                     
                        }

      addLeaderSubmit(data:any){
        if(this.addleaderform.valid)
        {
         console.log(this.addleaderform.value,"..............hey visitor")
          this.isSubmitedForm =true;
          this.trservice.addLeader(data).subscribe((res:any) =>{ 
            this.leaders =this.leaders.filter((row:any)=>row.seq_id !==data.seq_id);
          
             if(res.serviceResult)
             this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
      
             this.IsmodelShow=false;
             this.addleaderform.reset();
             this.isSubmitedForm =false;
             this.closebuttonAddLeader.nativeElement.click();
             this.getLeaders()
            },
             (error) => { this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
           );
        }
        // console.log('hey leader............................')
      }
      getLeaders(){
        this.http.get(`/api/portal/admin/get_vms_leaders`).subscribe((res:any)=>{
          this.leaders= res.serviceResult
         console.log(this.leaders,"leaderrrrrrrrrrrrrrrrrr");
         
          // this.trservice.userData.next(res.serviceResult)

          // document.getElementById('previewData1')?.click()
      })
      }
      addReasonSubmit(data:any){
        if(this.addreasonform.valid)
        {
         console.log(this.addreasonform.value,"..............hey reason")
          this.isSubmitedForm =true;
          this.trservice.addReason(data).subscribe((res:any) =>{ 
            this.reasons =this.reasons.filter((row:any)=>row.seq_id !==data.seq_id);
          
             if(res.serviceResult)
             this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
            this.receiptData=res.serviceResult.result
            console.log(this.receiptData, '-----------------------hey visitor receipt')
          
             this.addreasonform.reset();
             this.isSubmitedForm =false;
             this.closebuttonAddReason.nativeElement.click();
             this.getReasons()
            },
             (error) => {this.isSubmitedForm =false;  this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
           );
        }
      }
      getReasons(){
        this.http.get(`/api/portal/admin/get_vms_reasons`).subscribe((res:any)=>{
          this.reasons= res.serviceResult
         console.log(this.reasons,"Reasonnnnnnnnnnnnnn");
         
          // this.trservice.userData.next(res.serviceResult)

          // document.getElementById('previewData1')?.click()
      })
      }

      visitorMeetSubmit(data:any){
        if(this.setmeetform.valid)
        {
         console.log(this.setmeetform.value,"..............hey visitor")
          this.isSubmitedForm =true;
          this.trservice.setVisitorMeet(data,this.visitorData.seq_id).subscribe((res:any) =>{ 
          
             if(res.serviceResult)
             this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
            this.receiptData=res.serviceResult.result
            console.log(this.receiptData, '-----------------------hey visitor receipt')
             this.IsmodelShow=false;
             this.reloadTable();
             this.setmeetform.reset();
             this.isSubmitedForm =false;
             this.closebuttonScheduleVisitor.nativeElement.click();
             //  this.reloadTable();
            },
             (error) => {  this.isSubmitedForm =false;  this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
           );
        } 
      }
      deleteLeader(lead:any){
        const leadValue =lead.value;
        this.trservice.deleteLeaderById(lead.value).subscribe((res:any)=>{
          this.leaders =this.leaders.filter((num:any) => { num.seq_id != Number(leadValue) });
          console.log("after Fileter",leadValue)
          if(res.serviceResult)
          this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
         },
          (error) => {  this.isSubmitedForm =false;  this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
        );
       
     }

     deleteReason(reas:any){
      console.log(reas);
      const reasonValue =reas.value
      this.trservice.deleteReasonById(reas.value).subscribe((res:any)=>{
          this.reasons =this.reasons.filter((num:any)=>{ num.seq_id != Number(reasonValue) });
          console.log("after Fileter",reasonValue)
        if(res.serviceResult)
        this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
       },
        (error) => {  this.isSubmitedForm =false;  this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
      );
      //   this.trservice.insuranceData.next(res.serviceResult)
      //  this.insuranceData=res.serviceResult
      //  document.getElementById('previewInsuranceData')?.click()
   };
     
    //  getLeadersById(lead:any){
    //   this.trservice.getLeaderById(lead).subscribe((res:any)=>{
    //     this.leadersById =this.leaders.filter((row:any)=>row.seq_id !==lead.seq_id);

    //   //  this.leadersById=res.serviceResult
    //    console.log(this.leadersById)
    //  })
    // }
      // getReasonsById(reas:any){
      //   this.trservice.getReasonById(reas).subscribe((res:any)=>{
      //   //  this.reasonsById=res.serviceResult
      //    this.reasonsById=this.reasons.filter((row:any)=>row.seq_id !==reas.seq_id);
      //  console.log(this.reasonsById,'reasons byiddddddd')

      //  })
      // }
     reloadTable(){
      this.defaultQuery ={"rules":[]}
      this.GetDataByQuery(this.reportId,this.defaultQuery)
     }
     addRow(){
      console.log(this.dynamicArray)
   this.dynamicArray.push({name : "",phone:""})
     }
     deleteRow(index:any){
this.dynamicArray.splice(index,1)

     } 

     showFooField() {
      // document.getElementById("visting_location").style.display = "block";
      // document.getElementById("visting_location").classList.toggle("hide", this.value !=== "other")
  }


  maxLengthCheck(object:any) {
    if (object.value.length > object.maxLength)
      object.value = object.value.slice(0, object.maxLength)
  }


  numberOnly(event:any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  addEmployeeSubmit(data:any){
    if(this.employeeform.valid)
    {
     console.log(this.employeeform.value,"..............hey reason")
      this.isSubmitedForm =true;
      this.trservice.addEmployee(data).subscribe((res:any) =>{ 
        // this.reasons =this.reasons.filter((row:any)=>row.seq_id !==data.seq_id);
      
         if(res.serviceResult)
         this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
        this.employeeData=res.serviceResult.result
        console.log(this.employeeData, '-----------------------hey employeeData')
      
         this.employeeform.reset();
         this.isSubmitedForm =false;
         this.closebuttonAddEmployee.nativeElement.click();
       
        },
         (error) => {this.isSubmitedForm =false;  this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
       );
    }

  }

  editEmployeeSubmit(data:any){
    if(this.editemployeeform.valid)
    {
      this.isSubmitedForm =true;
      this.trservice.updateEmployee(data,this.employeeData.emp_id).subscribe((res:any) =>{ 
      
         if(res.serviceResult)
         this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
  
this.IsmodelShow=false;
      //  this.editemployeeform.reset();
       this.isSubmitedForm =false;
       this.closebuttonEditVisitor.nativeElement.click();
        this.reloadTable();

      
        },
         (error) => {  this.isSubmitedForm =false;  this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
       );
    }
  }

  deletEmployee(){
     
    this.trservice.deleteEmployeeById(this.deleteEmployeeDataId).subscribe((res:any) => {
     if(res.serviceResult){
        this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-center' })
        this.reloadTable();
      }else {
        this.toastr.error(res.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' })
      }
    }
     
     
   );
 
      }
      updateEmployeePhoto(){
        
        // this.loading = true; 
        this.trservice.updateEmployeePhoto(this.user_photo2,this.employeeData?.emp_id,this.type, this.emp_photo).subscribe((res:any) =>
        { 
          // this.loading = false; 
          console.log(res)
             if(res.serviceResult){
              this.fileupload3 ='';
              // this.trservice.employeeData.next(res.serviceResult)
              // this.employeeData=res.serviceResult
              // this.GetDataByQuery(this.reportId,this.defaultQuery)
              // location.reload();
              // document.getElementById('previewEmployeePhotoData')?.click()
               this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-center' })
             }else {
               this.toastr.error(res.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' })
             }
           }, 
       
         ) 
      }
      photoChange(event:any) {
        let fileList: FileList = event.target.files;
        if(fileList.length > 0) {
            let file: File = fileList[0];
            let formData:FormData = new FormData();
            formData.append('uploadFile', file, file.name);
             this.user_photo2 = file;
          //  let headers = new Headers();
            /** In Angular 5, including the header Content-Type can invalidate your request */
          //  headers.append('Content-Type', 'multipart/form-data');
          //  headers.append('Accept', 'application/json');
        
        }
      }


      onFileSelect(event:any) {
        if (event.target.files.length > 0) {
          const file = event.target.files[0];
          this.uploadForm.get('profile').setValue(file);
        }
      }
      onFileSelect2(event:any, fieldName: string) {
        if (event.target.files.length > 0) {
          const file = event.target.files[0];
          this.bulkpaymentform.get(fieldName)?.setValue(file);
        }
      }
      onSubmit() {
        // debugger;
        const formData = new FormData();
        formData.append('file', this.uploadForm.get('profile').value);
    
        // this.httpClient.post<any>(this.SERVER_URL, formData).subscribe(
          
        //   (res) => this.scanResult=res.serviceResult.url,
         
        //   (err) => alert(err),
     
        
        // );
      }
      addVolunteerSubmit(data:any){
        this.submitted = true;
      
         if(this.volunteerform.valid)
        {
       
         console.log(this.volunteerform.value,"..............hey volunteer")
          this.isSubmitedForm =true;
          this.kvservice.addVolunteer(this.volunteerform.value).subscribe((res:any) =>{ 
          
             if(res.serviceResult){
              this.volunteerform.reset();
            this.receiptData=res.serviceResult.result
            this.dynamicArray = [];
            this.IsmodelShow=false;
            this.isSubmitedForm =false;
           this.closebuttonAddvolunteer.nativeElement.click();

           //  this.closebuttonAddVisitor.nativeElement.click();
            
            this.isSubmitedForm =false;
           // document.getElementById('addvisitorModalOpen')?.click()

             // this.reloadTable();
         
             this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
            }
            else {
              this.toastr.error(res.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' })
            }
            } 

           
            // ,
            //  (serviceError) => { this.toastr.error(serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
           );
        }
        
           }
           deleteVolunteerId (params: any, parent: any){
            let rowData = params; 
            document.getElementById('deleteVolunteerData')?.click();

            this.deletVolunteerDataId=rowData?.data?.id;
           
            document.getElementById('deleteVolunteerData')?.click();
        
           } 
           deletVolunteer(){
            console.log(this.deletVolunteerDataId)
            this.trservice.deletVolunteer(this.deletVolunteerDataId).subscribe((res:any) => {
             if(res.serviceResult){
                this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-center' })
                this.reloadTable();
              }else {
                this.toastr.error(res.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' })
              }
            }
             
             // (response) => {response.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' }},
             // (error) => { this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
           );
         
              }
           addBulkVolunteerSubmit(){
            // debugger;
    const formData = new FormData();
    formData.append('file', this.uploadForm.get('profile').value);

    this.httpClient.post<any>(this.BULKVOLUNTEER_URL, formData).subscribe((res:any) =>{ 
              
      if(res.serviceResult)
      this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
     this.receiptData=res.serviceResult.result
     alert()
     this.closebuttonAddBVolunteer.nativeElement.click();
     this.uploadForm.reset();
     this.volunteerform.reset();
     this.dynamicArray = [];
     this.IsmodelShow=false;
    
     this.isSubmitedForm =false;
    // document.getElementById('addvisitorModalOpen')?.click()

  
      this.reloadTable();
 
     },
      (error) => { this.toastr.error(error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
    );
      
    //   (res) => this.scanResult=res.serviceResult.url,
     
    //   (err) => alert(err),
 
    
    // );
           }

           addBulkPaymentSubmit(){
            this.submitted = true;
          
             if(this.bulkpaymentform.valid)
            {
           
             console.log(this.bulkpaymentform.value,"..............hey volunteer bulkpayment")
              this.isSubmitedForm =true;
              //requestBody.utr_no =Number(requestBody.utr_no);
              let data = new FormData();
              //{"utr_no":123456,"description":"test","payment_made_by":"sharath",
              //"approved_by":"sharath",
              //"payment_receipt":"C:\\fakepath\\VYI482TCZ22SG7VLYST55IVFAEROBJDX.jpg",
              //"file":"C:\\fakepath\\bulk_payments.csv"}

              data.append('utr_no', this.bulkpaymentform.value.utr_no);
              data.append('description', this.bulkpaymentform.value.description);
              data.append('payment_made_by', this.bulkpaymentform.value.payment_made_by);
              data.append('approved_by', this.bulkpaymentform.value.approved_by);
              
              data.append('file', this.bulkpaymentform.get('file')?.value);
              data.append('payment_receipt', this.bulkpaymentform.get('payment_receipt')?.value);

              this.kvservice.addBulkPayment(data).subscribe((res:any) =>{ 
              
                 if(res.serviceResult)
                this.receiptData=res.serviceResult.result
                // this.dynamicArray = [];
                this.IsmodelShow=false;
                this.closebuttonAddBpayment.nativeElement.click();
                this.bulkpaymentform.reset();
                this.isSubmitedForm =false;
                this.myInputVariable.nativeElement.value = '';
           
               // document.getElementById('addvisitorModalOpen')?.click()
    
                 // this.reloadTable();
             this.clearvalue
                 this.reloadTable();
                 this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
                },
                 (error) => { this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
               );
            }
            
               }

               Volunteersactivation() { 
 
                const requestPayload= {
                  ...this.selectedRules,
                  "update_query":{"status":"Active"},
                  "max-results": 2000,
                  "results-offset": 0
              };
                
                this.trservice.updateStatusFilter(requestPayload).subscribe((res:any)=>{
                  if(res.serviceResult)
                  this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
                 this.receiptData=res.serviceResult.result
                
                //  this.isSubmitedForm =false;
     
                  this.reloadTable();
                },
                (error) => { this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
                )
                // console.log("Query", this.selectedRules)
              } 
              Volunteerssupend() {  
                const requestPayload= {
                  ...this.selectedRules,
                  "update_query":{"status":"Suspend"},
                  "max-results": 2000,
                  "results-offset": 0
              };
                
                this.trservice.updateStatusFilter(requestPayload).subscribe((res:any)=>{
                  if(res.serviceResult)
                  this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
                 this.receiptData=res.serviceResult.result
                 // this.dynamicArray = [];
                //  this.IsmodelShow=false;
                //  this.closebuttonAddVisitor.nativeElement.click();
                //  this.volunteerform.reset();
                 this.isSubmitedForm =false;
     
                  this.reloadTable();
              
                },
                (error) => { this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
                )
                
                // console.log("Query", this.selectedRules)
              }
           

              // issue form services
              addIssueSubmit(data:any){
                if(this.issueform.valid)
               {
                 this.isSubmitedForm =true;
                 this.trservice.addITS(data).subscribe((res:any) =>{ 
                    if(res.serviceResult)
                    this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
                    // document.getElementById('previewAknowledgementPrint')?.click()
                   this.receiptData=res.serviceResult.result
                   console.log(this.receiptData, '-----------------------hiiiiiiireceipt')
                    this.IsmodelShow=false;
                    this.issueform.reset();
                    this.isSubmitedForm =false;
                    this.closebutton.nativeElement.click();
                     this.reloadTable();
                   },
                    (error) => {  this.isSubmitedForm =false;  this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
                  );
                  }
                  }
                 
              viewIssue(params: any, parent: any){
                let rowData = params;
             
                // console.log(rowData);=36
                // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
                  this.http.get(`/api/portal/jim/get_jim_details_by_id?seq_id=${rowData.data.seq_id}`).subscribe((res:any)=>{
                     this.trservice.issueData.next(res.serviceResult)
                    this.issueData=res.serviceResult
                    document.getElementById('previewIssueData')?.click()
                })
                
                
              }
              editIssue(params: any, parent: any){
                let rowData = params;
                  this.http.get(`/api/portal/jim/get_jim_details_by_id?seq_id=${rowData.data.seq_id}`).subscribe((res:any)=>{
                    this.trservice.issueData.next(res.serviceResult)
                   this.issueData=res.serviceResult
                    this.editissueform.patchValue({ ...res.serviceResult });
                    document.getElementById('editIssueData')?.click();
          
                })
                
               
          
                // alert("edit petition working")
              }
              editIssueSubmit(data:any){
                if(this.editissueform.valid)
                {
                  this.isSubmitedForm =true;
                  this.trservice.updateITS(data,this.issueData.seq_id).subscribe((res:any) =>{ 
                  
                     if(res.serviceResult)
                     this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
                       
                     this.IsmodelShow=false; 
                     this.isSubmitedForm =false;
                     this.closebutton.nativeElement.click();
                      this.reloadTable();
                    },
                     (error) => {  this.isSubmitedForm =false;  this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
                   );
                }
                  }

                  deleteIssue(params: any, parent: any){
 
                    let rowData = params; 
                    this.deletIssueDataId=rowData?.data?.seq_id;
                    document.getElementById('deletIssueData')?.click();
               
                  }

                  deletIssue(){
                    console.log(this.deletIssueDataId)
                    this.trservice.deletIssue(this.deletIssueDataId).subscribe((res:any) => {
                     if(res.serviceResult){
                        this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-center' })
                      }else {
                        this.toastr.error(res.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' })
                      }
                    }
                     
                     // (response) => {response.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' }},
                     // (error) => { this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
                   );
                   this.reloadTable();
                      }

                       // issue form services
              addConflictSubmit(data:any){
                if(this.conflictform.valid)
               {
               
                 this.isSubmitedForm =true;
                 this.trservice.addCMS(data).subscribe((res:any) =>{ 
                    if(res.serviceResult)
                    this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
                    // document.getElementById('previewAknowledgementPrint')?.click()
                   this.receiptData=res.serviceResult.result
                   console.log(this.receiptData, '-----------------------hiiiiiiireceipt')
                    this.IsmodelShow=false;
                    this.conflictform.reset();
                    this.isSubmitedForm =false;
                    this.closebuttoncms.nativeElement.click();
                     this.reloadTable();
                   },
                    (error) => {  this.isSubmitedForm =false;  this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
                  );
                  }else{
               this.toastr.warning('Please enter Mandetory(*) Fields','warning',{timeOut: 4000, positionClass: 'toast-top-center' })
                  
                  }
                  }
                  viewConflict(params: any, parent: any){
                    let rowData = params;
                 
                    // console.log(rowData);=36 cms/get_cms_details_by_id?seq_id=1
                    // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
                      this.http.get(`/api/portal/cms/get_cms_details_by_id?seq_id=${rowData.data.seq_id}`).subscribe((res:any)=>{
                         this.trservice.conflictData.next(res.serviceResult)
                        this.conflictData=res.serviceResult
                        document.getElementById('previewConflictData')?.click()
                    })
                    
                    
                  }
                  editConflict(params: any, parent: any){
                    
                  
                    
                      let rowData = params;
                        this.http.get(`/api/portal/cms/get_cms_details_by_id?seq_id=${rowData.data.seq_id}`).subscribe((res:any)=>{
                          this.trservice.conflictData.next(res.serviceResult)
                         this.conflictData=res.serviceResult
                          this.editconflictform.patchValue({ ...res.serviceResult });
                          document.getElementById('editConflictData')?.click();
                
                      })
                      
                     
                
                      // alert("edit petition working")
                  
                  };
                  editConflictSubmit(data:any){
                    if(this.editconflictform.valid)
                    {
                      this.isSubmitedForm =true;
                      this.trservice.updateCMS(this.conflictData.seq_id,data,this.uploadCMSFiles).subscribe((res:any) =>{ 
                      
                         if(res.serviceResult)
                         this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' })
                           
                         this.IsmodelShow=false; 
                         this.isSubmitedForm =false;
                         this.reloadTable();
                         this.closebuttoncmsedit.nativeElement.click();
                        //  this.editconflictform.reset();
                        //  this.conflictData.reset();
                        //  this.myInputVariable.nativeElement.value = '';
                         this.fileInputReference.nativeElement.value = "";
                        //  this.uploadCMSFiles = [];
                        //  this.clearvalue()
                         
                      //  this.refresh()
                        },
                         (error) => {  this.isSubmitedForm =false;  this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
                       );
                    }
                   
                      }

                  deleteConflict(params: any, parent: any){
 
                    let rowData = params; 
                    this.deleteConflictDataId=rowData?.data?.seq_id;
                    document.getElementById('deleteconflictData')?.click();
               
                  }
                  isdeleteConflict(){
                    console.log(this.deleteConflictDataId)
                    this.trservice.deletConflict(this.deleteConflictDataId).subscribe((res:any) => {
                     if(res.serviceResult){
                        this.toastr.success(res.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-center' })
                      }else {
                        this.toastr.error(res.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' })
                      }
                    }
                     
                     // (response) => {response.serviceResult.message,'Success',{timeOut: 4000, positionClass: 'toast-top-mibble' }},
                     // (error) => { this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }
                   );
                   this.reloadTable();
                  }
}




