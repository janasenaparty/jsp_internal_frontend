import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabularreportComponent } from './tabularreport.component';

describe('TabularreportComponent', () => {
  let component: TabularreportComponent;
  let fixture: ComponentFixture<TabularreportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabularreportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabularreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
