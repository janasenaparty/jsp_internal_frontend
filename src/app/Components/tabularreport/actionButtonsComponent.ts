import { HttpClient } from '@angular/common/http';
import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { PopupComponent } from 'ag-grid-community';
import { TabularreportService } from 'src/app/Services/tabularreport.service';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'cu-download-link-cell-renderer',
    template: `
   
    <span *ngIf="params?.data?.payment_url" class="btn btn-primary btn-xs" (click)="viewPayment()">View Payment <i class="fa fa-file-image-o" aria-hidden="true"></i> | </span>
    
    <span *ngIf="params?.colDef.field == 'voterdb' "  type="button" (click)="voterdb()"  class="btn btn-outline-warning" >Map Voter<i class="fas fa-edit"></i></span>
  
    <span   *ngIf="params?.colDef.field == 'Schedulemeet' && params?.data.appointment_status=='scheduled' "  type="button" (click)="scheduleMeet()"   class="btn" style="width: 110px;padding:0px;color:#0c6821;" > Reschedule <i  class="fa fa-clock-o"></i></span>
    
    
   
    

   
    
    <span *ngIf="params?.colDef.field == 'Vieweditemployee' " type="button" (click)="viewEmployee()"  type="button" class="btn btn-outline-success">  View <i class="fa fa-eye" aria-hidden="true"></i></span>
    <span *ngIf="params?.colDef.field == 'Vieweditemployee' " type="button" (click)="editEmployee()"  class="btn btn-outline-warning" > Edit  <i class="fas fa-edit"></i></span>
    <span *ngIf="params?.colDef.field == 'Deletemployee' " type="button" (click)="deleteEmployee()"  class="btn btn-outline-danger" >Delete <i  class="fa fa-trash"></i></span>
    <span *ngIf="params?.colDef.field == 'Qrcodephoto' " type="button" (click)="employeePhoto()"  class="btn btn-outline-info" >Open<i class="fa fa-image"></i></span>
    
    `
  
})
//insurance
 // <span *ngIf="params?.colDef.field == 'Vieweditinsurance' " type="button" (click)="viewInsurance()"  type="button" class="btn btn-outline-success">  View <i class="fa fa-eye" aria-hidden="true"></i></span>
    // <span *ngIf="params?.colDef.field == 'Vieweditinsurance' " type="button" (click)="editInsurance()"  class="btn btn-outline-warning" > Edit  <i class="fas fa-edit"></i></span>
    // <span *ngIf="params?.colDef.field == 'Deletinsurance' " type="button" (click)="deletInsurance2()"  class="btn btn-outline-danger" > Delete  <i  class="fa fa-trash"></i></span>
//vms
// <span   *ngIf="params?.colDef.field == 'Deletvisitor'"  type="button" (click)="deletVisitor()"    class="btn " style="padding:0px; color: #dd3c4c;"> Delete <i  class="fa fa-trash"></i></span>
// <span  *ngIf="params?.colDef.field == 'Viewvisitor'" (click)="viewVisitor()"  type="button"  class="btn " style="padding:0px; color: #28a745;"> View <i    class="fa fa-eye" aria-hidden="true"></i> </span>
//<span  *ngIf="params?.colDef.field == 'Editvisitor'" type="button" (click)="editVisitor()"  class="btn " style="padding:0px; color: #ffc107;" > Edit  <i class="fas fa-edit"></i></span>
   // <span  *ngIf="params?.colDef.field == 'Schedulemeet' && params?.data.appointment_status!== 'scheduled' "  type="button" (click)="scheduleMeet()"    class="btn" style="width: 110px;padding:0px;color:#e5750c;"> Schedule <i  class="fa fa-clock-o"></i></span>
   // <span   *ngIf="params?.colDef.field == 'Schedulemeet' && params?.data.appointment_status=='scheduled' "  type="button" (click)="scheduleMeet()"   class="btn" style="width: 110px;padding:0px;color:#0c6821;" > Reschedule <i  class="fa fa-clock-o"></i></span>
//pms
// <span *ngIf="params?.colDef.field == 'Printpetition'" (click)="printPetition()"  type="button" class="btn btn-outline-success"> Print/Download <i class="fa fa-download"></i></span>
//     <span *ngIf="params?.colDef.field == 'Vieweditpetition'" (click)="viewPetition()"  type="button" class="btn btn-outline-success"> View <i    class="fa fa-eye" aria-hidden="true"></i> </span>
//     <span *ngIf="params?.colDef.field == 'Vieweditpetition' " type="button" (click)="editPetition()"  class="btn btn-outline-warning" > Edit  <i class="fas fa-edit"></i></span>
//     <span *ngIf="params?.colDef.field == 'Deletpetition'"  type="button" (click)="deletPetition()"    class="btn btn-outline-danger"> Delete <i  class="fa fa-trash"></i></span>

//km
// <span *ngIf="params?.colDef.field == 'Action'" (click)="viewData()"  type="button" class="btn btn-outline-success"> View <i    class="fa fa-eye" aria-hidden="true"></i> </span>
//     <span *ngIf="params?.colDef.field == 'Action' " type="button" (click)="editData()"  class="btn btn-outline-warning" > Edit  <i class="fas fa-edit"></i></span>
    
//     <span *ngIf="params?.colDef.field == 'editMember' && (isRole !== 'AC')"  type="button" (click)="editMemberData()"  class="btn btn-outline-warning" > Edit Member <i class="fas fa-edit"></i></span>

// <span *ngIf="params?.colDef.field == 'Vieweditvisitor'" > <div class="btn-group" role="group" aria-label="Basic mixed styles example">
//     <button type="button" class="btn btn-warning" (click)="viewVisitor()" >view</button>
//     <button type="button" class="btn btn-success" (click)="editVisitor()">edit</button>
//   </div><i  class="fa fa-time"></i></span>
// print --><i class="fa fa-print"></i>
// <span *ngIf="params?.colDef.field == 'Qrcsodephoto' " type="button" (click)="qrCode()"  class="btn btn-outline-primary" >QR<i class="fas fa-qrcode"></i></span>

// <span *ngIf="params?.colDef.field == 'Viewpetition' "  type="button" (click)="viewPetition()"  class="btn btn-outline-warning" >View Petition<i class="fas fa-edit"></i></span>

// <span *ngIf="params?.colDef.field == 'Action' && isRole==='AC'" type="button" (click)="editData()"  class="btn btn-outline-warning" > Edit  <i class="fas fa-edit"></i></span>
// <div *ngIf="params?.data.status=='Active'" class="mt-5 submit-btn text-end">
// <h2 style="color:green">Accepted</h2>
// </div>
// <div   *ngIf="params?.data.status=='Suspended'" class="mt-5 submit-btn text-end">
// <h5 style="color:rgb(248, 171, 27)" >Rejected</h5>
// </div> 
// <span  type="button" (click)="editField()"  class="btn btn-outline-warning" > edit field<i class="fas fa-edit"></i></span>
// | <i *ngIf="params?.data?.id"  (click)="deleteRow()" class="fa fa-trash" aria-hidden="true"></i>
//<i (click)="deleteRow()" class="fa fa-eye" aria-hidden="true"></i> | <i (click)="deleteRow()" class="fa fa-trash" aria-hidden="true"></i>
//<span *ngIf="params?.colDef.field == 'Schedulemeet' "  type="button" *ngIf="(serviceResult?.results.appointment_status == 'cancelled'" (click)="scheduleMeet()"    class="btn btn-outline-success"> Scheduled <i  class="fa fa-time"></i></span>
  //  <span *ngIf="params?.colDef.field == 'Schedulemeet' "  type="button" *ngIf="(serviceResult?.results.appointment_status == 'scheduled'" (click)="scheduleMeet()"    class="btn btn-outline-success"> Scheduled <i  class="fa fa-time"></i></span> 
  //<span *ngIf="params?.colDef.field == 'Schedulemeet'&& visitorData?.appointment_status == 'scheduled' "  type="button" (click)="scheduleMeet()"    class="btn btn-outline-success"> Schedule <i  class="fa fa-time"></i></span>

  export class CellCustomComponent implements OnInit {
    data: any;
    voters:any;
    params: any;
  isRole: any;
  isMail: any;
  componentParent:any;
  memberName:any;
  memberDetails:any;
  memberMandal:any;
  voterData:any;
  petitionerData:any;
  employeeData:any;
  visitorData:any;
  insuranceData:any;
  empQrData:any;

  private _jsonURL = 'assets/voter.json';
    constructor( private toastr:ToastrService,private trservice : TabularreportService,private http: HttpClient,private router :Router) {}
    agInit(params: { value: any; }) {
      this.params = params;
      this.data = params.value;
      this.componentParent = this.params.context.componentParent;
      console.log("Parent",this.componentParent)
    }
  
    ngOnInit() {
// console.log(this.params,this.data)
if( localStorage.getItem('isRole') ){
  this.isRole = localStorage.getItem('isRole');
  this.isMail = localStorage.getItem('isMail');
  // console.log(this.isRole)
 
}
    }
  
    
    deleteRow() {
      let rowData = this.params;
      // console.log(rowData);
      this.trservice.deleteRowID =rowData.data.id
    document.getElementById('deleteData')?.click()
    }
  viewData(){
    let rowData = this.params;

    // this.trservice.userData.next(rowData.data)
    // console.log(rowData);
    this.http.get(`/api/portal/admin/get_kriya_member_details?id=${rowData.data.id}`).subscribe((res:any)=>{
        this.trservice.userData.next(res.serviceResult)
    document.getElementById('previewData')?.click()
    })
  }
  editData(){
   
    let rowData = this.params;
   
    // this.trservice.userData.next(rowData.data)
    console.log(rowData);
    this.http.get(`/api/portal/admin/get_kriya_member_details?id=${rowData.data.id}`).subscribe((res:any)=>{
        this.trservice.userData.next(res.serviceResult)
        document.getElementById('previewData1')?.click()
    })
  }
  editField(){}
    viewPayment() {
      let rowData = this.params;
      this.trservice.paymentImageUrl.next(rowData.data.payment_url)
      console.log(rowData);
      document.getElementById('paymentrecipt')?.click()
    }
    voterdb(){
      // this._jsonURL.voters.next(_jsonURL.voters)

      // let _jsonURL = this.params;
    //   return this.http.get(`this._jsonURL?id= ${this._jsonURL.voters.id}`).subscribe((res:any)=>{
    //     this.trservice.userData.next(res.serviceResult)
        
    //   })
    // }
    // ;
     
      // this.trservice.userData.next(rowData.data)
      let rowData = this.params;

      console.log(rowData);
      // this.http.get(`/api/portal/admin/get_kriya_member_mapped_voters?id=${rowData.data.id}`).subscribe((res:any)=>{
        this.http.get(`/api/portal/admin/get_kriya_member_details?id=${rowData.data.id}`).subscribe((res:any)=>{
          this.trservice.userData.next(res.serviceResult)
          this.componentParent.memberDetails = res.serviceResult;
          this.componentParent.memberId = res.serviceResult.id;
          this.componentParent.memberName = res.serviceResult.name;
          this.componentParent.memberAadharNumber = res.serviceResult.aadhar_number;
          this.componentParent.memberEmail = res.serviceResult.email;
          this.componentParent.memberMobile = res.serviceResult.mobile;
          this.componentParent.memberState = res.serviceResult.state;
          this.componentParent.memberDistrict = res.serviceResult.district_name;
          this.componentParent.memberConstituency = res.serviceResult.constituency_name;
          this.componentParent.memberAddress = res.serviceResult.address;
          this.componentParent.memberDob = res.serviceResult.dob;
          this.componentParent.memberAadharCard = res.serviceResult.aadhar_card;
          this.componentParent.memberGender = res.serviceResult.gender;
          this.componentParent.memberNomineeName = res.serviceResult.nominee_name;
          this.componentParent.memberNominiAadharNumber = res.serviceResult.nominee_aadhar_number;
          this.componentParent.memberRemarks = res.serviceResult.remarks;
          console.log(this.memberDetails)

          // document.getElementById('previewvoterdata')?.click()

      })
      
      this.http.get(`/api/portal/admin/get_kriya_member_mapped_voters?id=${rowData.data.id}`).subscribe((res:any)=>{
        this.trservice.userData.next(res.serviceResult)
        //this.trservice.voterData.next(res.serviceResult[0]);
        document.getElementById('previewvoterdata')?.click()

        // this.trservice.open(res.serviceResult)
       // this.componentParent.voterData = res.serviceResult[0];
        console.log( this.componentParent.voterData)
       
        // this.componentParent.memberDetails = res.serviceResult;
        // this.componentParent.voterName = res.serviceResult[0].name;
        // this.componentParent.voterAge = res.serviceResult[0].age;
        // this.componentParent.memberAadharNumber = res.serviceResult.aadhar_number;
        // this.componentParent.memberEmail = res.serviceResult.email;
        // this.componentParent.memberMobile = res.serviceResult.mobile;
        // this.componentParent.memberState = res.serviceResult.state;
        // this.componentParent.voterHno= res.serviceResult[0].house_no;
        // this.componentParent.voterMandal= res.serviceResult[0].mandal;
        // this.componentParent.voterDistrict = res.serviceResult[0].district_name;
        // this.componentParent.voterConstituency = res.serviceResult[0].constituency_name;
        // this.componentParent.voterPincode= res.serviceResult[0].pincode;
        // this.componentParent.voterPollingStationName = res.serviceResult[0].polling_station_name;
        // this.componentParent.voterPollingStationNo = res.serviceResult[0].polling_station_no;
        // this.componentParent.voterRelationType = res.serviceResult[0].relation_type;
        // this.componentParent.voterRelativeName = res.serviceResult[0].relative_name;
        // this.componentParent.voterSno = res.serviceResult[0].sno;
        // this.componentParent.voterVoterId = res.serviceResult[0].voter_id;
        // this.componentParent.voterVoters2022SeqId = res.serviceResult[0].voters2022_seq_id;
        // this.componentParent.memberAddress = res.serviceResult.address;
        // this.componentParent.memberDob = res.serviceResult.dob;
        // this.componentParent.memberAadharCard = res.serviceResult.aadhar_card;
        // this.componentParent.memberGender = res.serviceResult.gender;
        // this.componentParent.memberNomineeName = res.serviceResult.nominee_name;
        // this.componentParent.memberNominiAadharNumber = res.serviceResult.nominee_aadhar_number;
        // this.componentParent.memberRemarks = res.serviceResult.remarks;
        // console.log(this.memberDetails)

      })
    }
    editMemberData(){
      let rowData = this.params;
     
      // this.trservice.userData.next(rowData.data)
      console.log(rowData);
      this.http.get(`/api/portal/admin/get_kriya_member_details?id=${rowData.data.id}`).subscribe((res:any)=>{
          this.trservice.userData.next(res.serviceResult)
          // this.componentParent.volunteerName = rowData.data.name;
          this.componentParent.memberDetails = res.serviceResult;
          this.componentParent.memberName = res.serviceResult.name;
          this.componentParent.memberAadharNumber = res.serviceResult.aadhar_number;
          this.componentParent.memberEmail = res.serviceResult.email;
          this.componentParent.memberMobile = res.serviceResult.mobile;
          this.componentParent.memberState = res.serviceResult.state;
          this.componentParent.memberDistrict = res.serviceResult.district_name;
          this.componentParent.memberConstituency = res.serviceResult.constituency_name;
          this.componentParent.memberAddress = res.serviceResult.address;
          this.componentParent.memberDob = res.serviceResult.dob;
          this.componentParent.memberAadharCard = res.serviceResult.aadhar_card;
          this.componentParent.memberGender = res.serviceResult.gender;
          this.componentParent.memberNomineeName = res.serviceResult.nominee_name;
          this.componentParent.memberNominiAadharNumber = res.serviceResult.nominee_aadhar_number;
          this.componentParent.memberRemarks = res.serviceResult.remarks;
          console.log(this.memberDetails)

          document.getElementById('previewMemberDataEdit')?.click()

      })
      
    }
    printPetition(){
      let rowData = this.params;
  
   
      // console.log(rowData);=36
      // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
        this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.seq_id}`).subscribe((res:any)=>{
           this.trservice.petitionerData.next(res.serviceResult)
          this.petitionerData=res.serviceResult
          document.getElementById('previewAknowledgementPrintLater')?.click()
      })
      
      
      
    }
    deletPetition(){
 
      let rowData = this.params; 
      this.componentParent.deletPetionDataId=rowData?.data?.seq_id;
      document.getElementById('deletPetitionData')?.click();
  
   
     
      // alert("delet")
    }
    editPetition(){
      let rowData = this.params;
   
      // console.log(rowData);=36
      // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
        this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.seq_id}`).subscribe((res:any)=>{
           this.trservice.petitionerData.next(res.serviceResult)
          this.petitionerData=res.serviceResult;
          this.componentParent.editgrievanceform.patchValue({ ...res.serviceResult });
          document.getElementById('editPetitionData')?.click();

      })
      
     

      // alert("edit petition working")
    }
    viewPetition(){
      let rowData = this.params;
   
      // console.log(rowData);=36
      // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
        this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.seq_id}`).subscribe((res:any)=>{
           this.trservice.petitionerData.next(res.serviceResult)
          this.petitionerData=res.serviceResult
          document.getElementById('previewPetitonerData')?.click()
      })
      
      
    }

    
    viewVisitor(){
      let rowData = this.params;
   
      // console.log(rowData);=36
      // this.http.get(`/api/portal/admin/get_vms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
        this.http.get(`/api/portal/admin/get_vms_details_by_id?id=${rowData.data.seq_id}`).subscribe((res:any)=>{
           this.trservice.visitorData.next(res.serviceResult)

           // this.insuranceform.patchValue(
      //   {
      //     kriya_member_id:res.serviceResult.kriya_member_id,
      //     poc_mobile:res.serviceResult.poc_mobile,
      //     mobile:res.serviceResult.mobile,
      //     mobile:res.serviceResult.mobile
        // });
      // this.visitorform.patchValue(res.serviceResult);

          this.visitorData=res.serviceResult
          document.getElementById('previewVisitorData')?.click()
      })
    }  
   editVisitor(){
    let rowData = this.params;
    // document.getElementById('editVissitorData')?.click();
   
    // console.log(rowData);=36api/portal/admin/update_vms_details
    // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
      this.http.get(`/api/portal/admin/get_vms_details_by_id?id=${rowData.data.seq_id}`).subscribe((res:any)=>{
         this.trservice.visitorData.next(res.serviceResult)
        this.visitorData=res.serviceResult;
        this.componentParent.editvisitorform.patchValue({ ...res.serviceResult });
        if(this.isMail == this.visitorData.created_by){
        document.getElementById('editVisitorData')?.click()}
        else{
        //   this.toastr.showSuccess(message, title){
        //     this.toastr.success(message, title)
        // }
          this.toastr.warning('You Are Not Authorised Member','warning',{timeOut: 4000, positionClass: 'toast-top-center' })
        };

    })
   } 
   deletVisitor (){
    let rowData = this.params; 
    this.componentParent.deletVisitorDataId=rowData?.data?.seq_id;
   
    document.getElementById('deletVisitorData')?.click();

   } 
    viewInsurance(){
      let rowData = this.params;
      // alert("hey view button works ") 
   
        this.http.get(`/api/portal/claims/get_claim_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
           this.trservice.insuranceData.next(res.serviceResult)
          this.insuranceData=res.serviceResult
          document.getElementById('previewInsuranceData')?.click()
      })
      
      
    }
  //   editInsurance(){
  //     let rowData = this.params;
  
  //  alert("hey edit button works ");
  // //  console.log(rowData.data.id)
   
  //     //   this.http.get(`/api/portal/claims/update_claims_details?id=${rowData.data.id}`).subscribe((res:any)=>{
  //     //      this.trservice.insuranceData.next(res.serviceResult)
  //     //     this.insuranceData=res.serviceResult
  //     //     document.getElementById('editPetitonerData')?.click()
  //     // })
      
      
  //   }
  editInsurance(){
       let rowData = this.params;
   
        this.router.navigate(['kriya_insurance_form','edit',rowData.data.id])
        // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.seq_id}`).subscribe((res:any)=>{
        //    this.trservice.updateInsurance.next(res.serviceResult)
        //   this.petitionerData=res.serviceResult;
        //   this.componentParent.editgrievanceform.patchValue({ ...res.serviceResult });
          document.getElementById('editInsuranceData')?.click();

      // })
      
     
 
    }
    deletInsurance2(){
 
      
      let rowData = this.params;
      console.log(rowData.data)
      
     this.componentParent.deletInsuranceDataId=rowData?.data?.id;
     document.getElementById('deletInsuranceData')?.click();
 
    }

    scheduleMeet(){
      let rowData = this.params;
      // document.getElementById('editVisitorData')?.click();
     
      // console.log(rowData);=36api/portal/admin/update_vms_details
      // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
        this.http.get(`/api/portal/admin/get_vms_details_by_id?id=${rowData.data.seq_id}`).subscribe((res:any)=>{
           this.trservice.visitorData.next(res.serviceResult)
          this.visitorData=res.serviceResult;
          this.componentParent.setmeetform.patchValue({ ...res.serviceResult });
          document.getElementById('scheduleVisitorMeet')?.click();
  
      })
    }
 
    // viewData(){viewInsurance
    //   let rowData = this.params;
  
    //   // this.trservice.userData.next(rowData.data)
    //   // console.log(rowData);
    //   this.http.get(`/api/portal/admin/get_kriya_member_details?id=${rowData.data.id}`).subscribe((res:any)=>{
    //       this.trservice.userData.next(res.serviceResult)
    //   document.getElementById('previewData')?.click()
    //   })
    // }
    closePopup(){

    }
    viewEmployee(){
      let rowData = this.params;
   
      // console.log(rowData);=36
      // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
        this.http.get(`/api/portal/employees/get_employee_details_by_id?emp_id=${rowData.data.emp_id}`).subscribe((res:any)=>{
           this.trservice.employeeData.next(res.serviceResult)
          this.employeeData=res.serviceResult
          document.getElementById('previewEmployeeData')?.click()
      })
    }
editEmployee(){
  let rowData = this.params;
  // document.getElementById('editVissitorData')?.click();
 
  // console.log(rowData);=36api/portal/admin/update_vms_details
  // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
    this.http.get(`/api/portal/employees/get_employee_details_by_id?emp_id=${rowData.data.emp_id}`).subscribe((res:any)=>{
       this.trservice.employeeData.next(res.serviceResult)
      this.employeeData=res.serviceResult;
      this.componentParent.editemployeeform.patchValue({ ...res.serviceResult });
      document.getElementById('editEmployeeData')?.click()
     

  })
}
deleteEmployee(){
  let rowData = this.params;  
  
  this.componentParent.deleteEmployeeDataId=rowData?.data?.emp_id;  
  document.getElementById('deleteEmployeeData')?.click();

}

qrCode(){
  let rowData = this.params;
    this.http.get(`/api/portal/employees/get_employee_qrcode_by_id?emp_id=${rowData.data.emp_id}`).subscribe((res:any)=>{
      console.log(res.preview)
      this.trservice.empQrData.next(res.serviceResult)
      this.empQrData=res
      console.log('qr data',this.empQrData)
      document.getElementById('previewQrData')?.click()
  })
}
employeePhoto(){
  this.qrCode()
  let rowData = this.params;
    this.http.get(`/api/portal/employees/get_employee_details_by_id?emp_id=${rowData.data.emp_id}`).subscribe((res:any)=>{
       this.trservice.employeeData.next(res.serviceResult)
      this.employeeData=res.serviceResult
      document.getElementById('previewEmployeePhotoData')?.click()
  })
}


  }

  