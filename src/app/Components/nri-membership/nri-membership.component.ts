import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nri-membership',
  templateUrl: './nri-membership.component.html',
  styleUrls: ['./nri-membership.component.css']
})
export class NriMembershipComponent implements OnInit {
  reportNumber :any;
  nameOfReport: any;

  constructor() { }

  ngOnInit(): void {
    this.reportNumber = 45
    this.nameOfReport = "NRI Membership"
  }

}
