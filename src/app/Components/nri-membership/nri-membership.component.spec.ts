import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NriMembershipComponent } from './nri-membership.component';

describe('NriMembershipComponent', () => {
  let component: NriMembershipComponent;
  let fixture: ComponentFixture<NriMembershipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NriMembershipComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NriMembershipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
