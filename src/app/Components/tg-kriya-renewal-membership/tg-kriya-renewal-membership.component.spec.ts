import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TgKriyaRenewalMembershipComponent } from './tg-kriya-renewal-membership.component';

describe('TgKriyaRenewalMembershipComponent', () => {
  let component: TgKriyaRenewalMembershipComponent;
  let fixture: ComponentFixture<TgKriyaRenewalMembershipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TgKriyaRenewalMembershipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TgKriyaRenewalMembershipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
