import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tg-kriyamembership-phasetwo',
  templateUrl: './tg-kriyamembership-phasetwo.component.html',
  styleUrls: ['./tg-kriyamembership-phasetwo.component.css']
})
export class TgKriyamembershipPhasetwoComponent implements OnInit {

  reportId:any;
  reportName:any;
  constructor() { }

  ngOnInit(): void {
    this.reportId =18;
    this.reportName ="TG Kriya members Phase two";

  }
}
