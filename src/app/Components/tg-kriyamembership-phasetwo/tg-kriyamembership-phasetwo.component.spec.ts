import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TgKriyamembershipPhasetwoComponent } from './tg-kriyamembership-phasetwo.component';

describe('TgKriyamembershipPhasetwoComponent', () => {
  let component: TgKriyamembershipPhasetwoComponent;
  let fixture: ComponentFixture<TgKriyamembershipPhasetwoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TgKriyamembershipPhasetwoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TgKriyamembershipPhasetwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
