import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TgPhaseThreeMembershipComponent } from './tg-phase-three-membership.component';

describe('TgPhaseThreeMembershipComponent', () => {
  let component: TgPhaseThreeMembershipComponent;
  let fixture: ComponentFixture<TgPhaseThreeMembershipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TgPhaseThreeMembershipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TgPhaseThreeMembershipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
