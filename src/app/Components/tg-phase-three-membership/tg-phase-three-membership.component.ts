import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tg-phase-three-membership',
  templateUrl: './tg-phase-three-membership.component.html',
  styleUrls: ['./tg-phase-three-membership.component.css']
})
export class TgPhaseThreeMembershipComponent implements OnInit {

  reportId:any;
  reportName:any;
  constructor() { }

  ngOnInit(): void {
    this.reportId =33;
    this.reportName ="TG Kriya members Phase three";

  }

}
