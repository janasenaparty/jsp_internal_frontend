import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportReportLogsComponent } from './export-report-logs.component';

describe('ExportReportLogsComponent', () => {
  let component: ExportReportLogsComponent;
  let fixture: ComponentFixture<ExportReportLogsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExportReportLogsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExportReportLogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
