import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KriyamembersdashboardComponent } from './kriyamembersdashboard.component';

describe('KriyamembersdashboardComponent', () => {
  let component: KriyamembersdashboardComponent;
  let fixture: ComponentFixture<KriyamembersdashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KriyamembersdashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KriyamembersdashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
