import { Component, OnInit } from '@angular/core';
import { TabularreportService } from 'src/app/Services/tabularreport.service';
import { interval, Subscription } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'app-kriyamembersdashboard',
  templateUrl: './kriyamembersdashboard.component.html',
  styleUrls: ['./kriyamembersdashboard.component.css'],
  providers:[Document]
})

export class KriyamembersdashboardComponent implements OnInit {
  summary_counts_url='/api/portal/admin/kriya/summary_counts';
 
  grandTotalRecords:any;
  overAllRecords:any;
  phaseTwoRecords:any;
  renewalRecords:any;

  reportId:any;
  reportName:any;
  grandvarificationcompleet:any;
  grandpaymentvarificationpending :any;
  phasethreetotal:any;
  todaycount:any;
  todayTotal :any;
    ser:any;
    isRole:any;
    labels:string[]=[];
  subscription!: Subscription;

  constructor( private document: Document , private toastr:ToastrService,private trservice: TabularreportService , private http:HttpClient) { }
  // showSuccess() {
  //   this.toastr.success('Hello world!', 'Toastr fun!');
  // }
  ngOnInit(): void {
    if( localStorage.getItem('isRole') ){
      this.isRole = localStorage.getItem('isRole');
      console.log(this.isRole)
     
   }
    this.reportId =10;
    this.reportName ="Kriya Members Dashboard";
    // const source = interval(1000000);
    // this.subscription = source.subscribe(val => this.getSummaryCount());

   this.getSummaryCount();
 
    this.labels = [ 'Download Sales'];

 
  }
  goToUrl(): void {
    this.document.location.href = 'https://stackoverflow.com';
}
  getSummaryCount(){
    let url = this.summary_counts_url;
    return this.http.get(url)
    .subscribe((res: any) => {
      if (res) { 
          this.overAllRecords = res.serviceResult.overall;
          console.log(this.overAllRecords);
          this.phaseTwoRecords = res.serviceResult.phase2;
          this.todaycount =  res.serviceResult.today;
          this.todayTotal = res.serviceResult.today.new+res.serviceResult.today.renewal
          this.renewalRecords = res.serviceResult.renewal;
          this.phasethreetotal = (res.serviceResult.phase2.payment_verification_pending+res.serviceResult.phase2.verfication_completed)
          this.grandTotalRecords  = (res.serviceResult.phase2.payment_verification_pending+res.serviceResult.phase2.verfication_completed+ res.serviceResult.renewal.total)
         this.grandpaymentvarificationpending =  (res.serviceResult.phase2.payment_verification_pending+res.serviceResult.renewal.payment_verification_pending)
        //  this.grandvarificationpending =  (res.serviceResult.phase2.payment_verification_pending+res.serviceResult.renewal.payment_verification_pending)
         this.grandvarificationcompleet=  (res.serviceResult.phase2.verfication_completed+res.serviceResult.renewal.verfication_completed)
       
         
      }

  })
  }
 
 
  ngOnDestroy() {
    // For method 1
    // this.subscription && this.subscription.unsubscribe();

   
  }


}
