import { Component, OnInit } from '@angular/core';
import { TabularreportService } from 'src/app/Services/tabularreport.service';

@Component({
  selector: 'app-issue-tracking-system',
  templateUrl: './issue-tracking-system.component.html',
  styleUrls: ['./issue-tracking-system.component.css']
})
export class IssueTrackingSystemComponent implements OnInit {

  reportId:any;
  reportName:any;
  constructor() { }

  ngOnInit(): void {
    this.reportId =37;
    this.reportName ="Issue Management System ";
 
  }

}
