import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IssueTrackingSystemComponent } from './issue-tracking-system.component';

describe('IssueTrackingSystemComponent', () => {
  let component: IssueTrackingSystemComponent;
  let fixture: ComponentFixture<IssueTrackingSystemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IssueTrackingSystemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IssueTrackingSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
