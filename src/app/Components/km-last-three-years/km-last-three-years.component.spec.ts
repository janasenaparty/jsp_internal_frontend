import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KmLastThreeYearsComponent } from './km-last-three-years.component';

describe('KmLastThreeYearsComponent', () => {
  let component: KmLastThreeYearsComponent;
  let fixture: ComponentFixture<KmLastThreeYearsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KmLastThreeYearsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(KmLastThreeYearsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
