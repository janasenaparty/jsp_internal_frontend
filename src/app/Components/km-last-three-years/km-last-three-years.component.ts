import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-km-last-three-years',
  templateUrl: './km-last-three-years.component.html',
  styleUrls: ['./km-last-three-years.component.css']
})
export class KmLastThreeYearsComponent implements OnInit {
  reportId:any
  reportName:any
  constructor() { }

  ngOnInit(): void {
    this.reportId =44;
    this.reportName ="Kriya Members Last 3 Years";
  }

}
