import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YuvaShakthiComponent } from './yuva-shakthi.component';

describe('YuvaShakthiComponent', () => {
  let component: YuvaShakthiComponent;
  let fixture: ComponentFixture<YuvaShakthiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ YuvaShakthiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(YuvaShakthiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
