import { Component, OnInit } from '@angular/core';
import { TabularreportService } from 'src/app/Services/tabularreport.service';

@Component({
  selector: 'app-yuva-shakthi',
  templateUrl: './yuva-shakthi.component.html',
  styleUrls: ['./yuva-shakthi.component.css']
})
export class YuvaShakthiComponent implements OnInit {

  isRole:any;
  reportId:any;
  reportName:any;
  constructor(private trservice:TabularreportService ) { 
  
  }

  ngOnInit(): void {
    this.reportId =31;
      this.reportName ="Yuva Shakthi";
 
      if( localStorage.getItem('isRole') ){
        this.isRole = localStorage.getItem('isRole');
        console.log(this.isRole)
       
       
     }
  }

}
