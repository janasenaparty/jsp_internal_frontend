import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl } from "@angular/forms";
import {
  QueryBuilderClassNames,
  QueryBuilderConfig
} from "angular2-query-builder";
import { ToastrService } from 'ngx-toastr';
import { TabularreportService } from 'src/app/Services/tabularreport.service';
import { QuerybuilderService } from "../../Services/querybuilder.service";


@Component({
  selector: 'app-qbuilder',
  templateUrl: './qbuilder.component.html',
  styleUrls: ['./qbuilder.component.css']
})
export class QbuilderComponent implements OnInit {

  public queryCtrl: FormControl;
  public operatorMap: { [key: string]: string[] } = {
    string: ['equal', 'not-equal', 'contains','is-null','is-not-null', 'not-equal','greater-than', 'greater-or-equal', 'lesser-than', 'lesser-or-equal'],
    number: ['equal', 'not-equal','greater-than', 'greater-or-equal', 'lesser-than', 'lesser-or-equal','between','is-null','is-not-null'],
    time: ['equal', 'not-equal', 'greater-than', 'greater-or-equal', 'lesser-than', 'lesser-or-equal','between'],
    date: ['equal', 'not-equal', 'greater-than', 'greater-or-equal', 'lesser-than', 'lesser-or-equal','between','is-null','is-not-null' ],
    category: ['equal', 'not-equal', 'in', 'not in'],
    boolean: ['equal']
  };

  districts:any;
  constituents:any;
  allConstituents:any;
  allMandals:any;
  allstates: any;
  allLeaders:any;
  allReasons:any;
  allDepartments:any;
  result:any;
  isRole:any;
  isName:any;
  isMail:any;
  dist:any;
  dists:any;

  @Input("reportId") reportId: any;
  @Input ("hasVolunteerActions") hasVolunteerActions= false;
  @Output( ) onFilterClick:EventEmitter<any> =new EventEmitter<any>();
  @Output( ) onExportClick:EventEmitter<any> =new EventEmitter<any>();
  @Output( ) onResetClick:EventEmitter<any> =new EventEmitter<any>();
  @Output( ) onSetDefaultClick:EventEmitter<any> =new EventEmitter<any>();
  public selectedRules:any;
  public bootstrapClassNames: QueryBuilderClassNames = {
    removeIcon: "fa fa-minus",
    addIcon: "fa fa-plus",
    arrowIcon: "fa fa-chevron-right px-2",
    button: "btn",
    buttonGroup: "btn-group",
    rightAlign: "order-12 ml-auto",
    switchRow: "d-flex px-2",
    switchGroup: "d-flex align-items-center",
    switchRadio: "custom-control-input",
    switchLabel: "custom-control-label",
    switchControl: "custom-control custom-radio custom-control-inline",
    row: "row p-2 m-1",
    rule: "border",
    // ruleSet: "border",
    invalidRuleSet: "alert alert-danger",
    emptyWarning: "text-danger mx-auto",
    operatorControl: "form-control",
    operatorControlSize: "col-auto pr-0",
    fieldControl: "form-control",
    fieldControlSize: "col-auto pr-0",
    entityControl: "form-control",
    entityControlSize: "col-auto pr-0",
    inputControl: "form-control",
    inputControlSize: "col-auto",

  };
  config1: QueryBuilderConfig=
  {
    fields: {


    }
  };

  public query = {
    condition: "and",
    rules: [
      { field: 'added_by', operator: 'equal', value: ''},
    ]
  };



  public config: QueryBuilderConfig=    {
       fields: {
    }
  };


  public allowRuleset: boolean = false;
  // public allowCollapse: boolean;
  public persistValueOnFieldChange: boolean = false;
  http: any;


  constructor(private toastr:ToastrService, private formBuilder: FormBuilder, private qbservice :QuerybuilderService , private trservice :TabularreportService) {

    this.queryCtrl = this.formBuilder.control(this.query);




  }


  ngOnInit(): void {

    if( localStorage.getItem('isRole') ){
      this.isRole = localStorage.getItem('isRole');
      this.isName = localStorage.getItem('isName');
      this.isMail = localStorage.getItem('isMail');


   }
      if((this.reportId !=47) )
      {
      this.selectedRules = {
        condition: "and",
        rules: [
          { field: 'name', operator: 'equal', value: ''},

        ]
      }
    }
    if((this.reportId ==47) )
    {
    this.selectedRules = {
      condition: "and",
      rules: [
        { field: 'donar_name', operator: 'equal', value: ''},

      ]
    }
  }

    // }else{

      // this.selectedRules = {
      //   condition: "and",
      //   rules: [
      //     { field: 'district_name', operator: 'equal', value: ''},

      //   ]
      // }
    // }
    this.GetFilters()


    let fields:any = {};

  this.qbservice.getDristrictsOnly().subscribe(data => {
    //  for (const dist in data) {

      // if (data.prototype.hasOwnProperty.call(data, dist)) {
      //   const element = data[dist];
      //   console.log(dist,'disttttttttttttttttttttttttttt')
      // }
    //  } let (i= 0,i<=3,i++)

    // for (const dist in data) {

    //   this.dists.push(data[dist]);
    // }
    this.dists= [...data[1],...data[2]]
      //  console.log(data[1],'distttttttt');
      this.districts = this.dists.map( (data:any)=>({
         name:data.district_name,value:data.district_name


         }));

      // console.log(`data.${dist} = ${data[dist]}`);
      // console.log(dist,'disttttttttttttt');



  //   this.districts = data[1].map( (data:any)=>({
  //     name:data.district_name,value:data.district_name
  // }));
 });
 this.qbservice.getConstituencies().subscribe(data => {

   this.allConstituents =data['constituencies'].map( (data:any)=>({
        name:data.constituency_name,value:data.constituency_name
  }));

  });
  this.qbservice.getMandals().subscribe(data => {

    this.allMandals =data.map( (data:any)=>({
         name:data.mandal_name,value:data.mandal_name
   }));

   });

this.qbservice.getLeaders().subscribe(data => {
console.log(data ,"Leaders Data")
  this.allLeaders =data.serviceResult.map( (data:any)=>({
       name:data.name,value:data.name

 }));


 });

 console.log(this.allLeaders,'hey leader alllllllll');

 this.qbservice.getReasons().subscribe(data => {

  // this.allReasons =data['serviceResult[{reason}]'].map( (data:any)=>({
    this.allReasons =data.serviceResult.map( (data:any)=>({
    name:data.reason,value:data.reason

 }));
 console.log(this.allReasons,'hey allReasons alllllllll');

 });
 this.qbservice.getDepartments().subscribe(data => {

  // this.allReasons =data['serviceResult[{reason}]'].map( (data:any)=>({
    this.allDepartments =data.serviceResult.map( (data:any)=>({
    name:data.name,value:data.name

 }));
 console.log(this.allDepartments,'hey allDepartments alllllllll');

 });

//  this.queryCtrl.valueChanges.subscribe(ruleset=>{
//   console.log(ruleset.rules[0].field +""+ruleset.rules[0].value);
//   if(ruleset.rules[0].field==='state' && ruleset.rules[0].value !== null)
//   {

// let states =this.allstates.states;
// let districts;
// states.filter(function (value:any) {
//       if(value.state == ruleset.rules[0].value )
//       {
//          districts =value.districts;
//       }
//     });
//     this.entityConfig.fields['district'].options =districts;
//     //this.entityConfig.fields['district'].options =this.districts;
//   }
// })

}

GetFilters(){

  this.qbservice.getFiltersUrl(this.reportId).subscribe((res : any)=>{

    // if (res.data.status=1) { this.toastr.info('Data Filtered','Export',{timeOut: 4000, })  }
   //e.operators =this.operatorMap[e.type]
    let fields:any = {};
    let data:any =res.data.filtets;
    data.map((e:any) => {

      if(e.type =='date')
      {
        fields[e.id] = { name: e.label, type: e.type,operators: this.operatorMap[e.type],defaultValue: []};
      }else{
        if(e.label =='gender' || e.label =='visitor_gender')
        {
          fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options: [
            { name: 'Male', value: 'male' },
            { name: 'Female', value: 'female' },
            { name: 'Others', value: 'others' },
          ],defaultValue:'male'};
        } else if(e.label =='visitor_type')
        {

          fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options: [
            { name: 'Fresh', value: 'fresh' },
            { name: 'Repeat', value: 'repeat' },
          ],defaultValue:'Fresh'};
        }
        else if(e.label =='visting_location')
        {

          fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options: [
            { name: 'Central Office', value: 'Central_Office' },
            { name: 'Mangalagiri Office', value: 'Mangalagiri_Office' },
          ],defaultValue:'Fresh'};
        }
        else if(e.label =='appointment_status')
        {

          fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options: [
            { name: 'Scheduled', value: 'scheduled' },
            { name: 'Cancelled', value: 'cancelled' },
          ],defaultValue:'scheduled'};
        }
        else if(e.label =='state')
        {

          fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options: [
            { name: 'Andra Pradesh', value: '1' },
            { name: 'Telangana', value: '2' },
          ],defaultValue:'1'};
        }
        else if(e.label =='district_name')
        {

                    fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options:this.districts ,defaultValue:1};
        }
         else if(e.label =='constituency_name')
        {

           fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options:this.allConstituents ,defaultValue:1};
 }
 else if(e.label =='mandal_name')
 {

    fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options:this.allMandals ,defaultValue:1};
}
 else if(e.label =='whom_to_meet')
        {

           fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options:this.allLeaders,defaultValue:1 };
 }
 else if(e.label =='purpose_of_meeting')
        {

           fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options:this.allReasons,defaultValue:1 };
 }
 else if(e.label =='department')
        {

           fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options:this.allDepartments,defaultValue:1 };
 }
 else if(e.label =='payment_status')
        {

          fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options: [
            { name: 'Success', value: 'Success' },
            { name: 'Pending', value: 'Pending' },
          ],defaultValue:'Success'};
        }
        else if(e.label =='payment_verified')
        {

          fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options: [
            { name: 'Yes', value: 'Yes' },
            { name: 'Pending', value: 'Pending' },
          ],defaultValue:'Yes'};
        }
        else if(e.label =='membership_type')
        {

          fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options: [
            { name: 'fresh', value: 'fresh' },
            { name: 'renewal', value: 'renewal' },
          ],defaultValue:'fresh'};
        }
        else if(e.label =='renewal_pending')
        {

          fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options: [
            { name: 'false', value: 'false' },
            { name: 'true', value: 'true' },
          ],defaultValue:'true'};
        }
        else if(e.label =='renewal_payment_status')
        {

          fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options: [
            { name: 'Success', value: 'Success' },
            { name: 'Pending', value: 'Pending' },
          ],defaultValue:'Success'};
        }
        else if(e.label =='status')
        {

          fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options: [
            { name: 'Completed', value: 'Completed' },
            { name: 'Pending', value: 'Pending' },
            { name: 'Active', value: 'Active' },
            { name: 'Suspend', value: 'Suspend' },
          ],defaultValue:'Completed'};
        }
        else if(e.label =='priority')
        {

          fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options: [
            { name: 'High', value: 'High' },
            { name: 'Medium', value: 'Medium' },
            { name: 'Low', value: 'Low' },

          ],defaultValue:'High'};
        }
        else if(e.label =='approach_type')
        {

          fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options: [
            { name: 'Direct Approach', value: 'Direct' },
            { name: 'Phone Call Approach', value: 'Phone' },
          ],defaultValue:'Direct'};
        }
        else if(e.label =='request_type')
        {

          fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options: [
            { name: 'Fullfilled Request', value: 'Fullfilled Request' },
            { name: 'Repeat Request', value: 'Repeat Request' },
            { name: 'Follow up Request', value: 'Follow up Request' },
            { name: 'New Request', value: 'New Request' },

          ],defaultValue:'Fullfilled Request'};
        }
else{
          fields[e.id] = { name: e.label, type: e.type,operators: this.operatorMap[e.type]};
        }

      }

    });
    this.config = { fields: fields };
 })

}

isValidatereset(){

this.onResetClick.emit(this.selectedRules)

}
isSetdefaultrules(){
  this.selectedRules={
    condition: "and",
    rules: [
      { field: 'name', operator: 'equal', value: ''},

    ]
  } ;
};
volunteerdefaultrules(){
  this.qbservice.getFiltersUrl(this.reportId).subscribe((res : any)=>{
  let fields:any = {};
  let data:any =res.data.filtets;
  data.map((e:any) => {

    if(e.type =='date')
    {
      fields[e.id] = { name: e.label, type: e.type,operators: this.operatorMap[e.type],defaultValue: []};
    }else{
      if(e.label =='gender' || e.label =='visitor_gender')
      {
        fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options: [
          { name: 'Male', value: 'male' },
          { name: 'Female', value: 'female' },
          { name: 'Others', value: 'others' },
        ],defaultValue:'male'};
      } else if(e.label =='visitor_type')
      {

        fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options: [
          { name: 'Fresh', value: 'fresh' },
          { name: 'Repeat', value: 'repeat' },
        ],defaultValue:'Fresh'};
      }
      else if(e.label =='visting_location')
      {

        fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options: [
          { name: 'Central Office', value: 'Central_Office' },
          { name: 'Mangalagiri Office', value: 'Mangalagiri_Office' },
        ],defaultValue:'Fresh'};
      }
      else if(e.label =='appointment_status')
      {

        fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options: [
          { name: 'Scheduled', value: 'scheduled' },
          { name: 'Cancelled', value: 'cancelled' },
        ],defaultValue:'scheduled'};
      }

      else if(e.label =='district_name')
      {

                  fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options:this.districts ,defaultValue:1};
      }
       else if(e.label =='constituency_name')
      {

         fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options:this.allConstituents ,defaultValue:1};
}
else if(e.label =='mandal_name')
{

  fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options:this.allMandals ,defaultValue:1};
}
else if(e.label =='whom_to_meet')
      {

         fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options:this.allLeaders,defaultValue:1 };
}
else if(e.label =='purpose_of_meeting')
      {

         fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options:this.allReasons,defaultValue:1 };
}
else if(e.label =='department')
      {

         fields[e.id] = { name: e.label, type: 'category',operators: this.operatorMap[e.type],options:this.allDepartments,defaultValue:1 };
}

else{
        fields[e.id] = { name: e.label, type: e.type,operators: this.operatorMap[e.type]};
      }

    }

  });
  })
  // this.selectedRules={
  //   condition: "and",
  //   rules: [
  //     { field: 'name', operator: 'equal', value: ''},
  //     { field: 'mobile', operator: 'equal', value: ''},
  //     { field: 'gender', operator: 'equal', value: ''},

  //   ]
  // } ;
}
isValidateexport(){
console.log(this.selectedRules)
this.onExportClick.emit(this.selectedRules)

}

keyDownFunction(event:any) {
  if (event.keyCode === 13) {
    alert('you just pressed the enter key');
    // rest of your code
  }
}
isValidatefilter(){

//   $("#pass").keypress(function(event:any) {
//     if (event.keyCode === 13) {
//         $("#btn-get").click();
//     }
// });

// $("#btn-get").click(function() {
//     alert("Button clicked");
// });
  console.log(this.selectedRules)
  this.onFilterClick.emit(this.selectedRules)
  if (this.selectedRules.length < 1 ){
  alert("plaese select atleast one filter")
  }else{
 this.GetFilters()
  }
}
// Volunteersactivation() {

//   const requestPayload= {
//     ...this.selectedRules,
//     "update_query":{"status":"Active"},
//     "max-results": 2000,
//     "results-offset": 0
// };

//   this.trservice.updateStatusFilter(requestPayload).subscribe((data)=>{

//   })
//   console.log("Query", this.selectedRules)
// }
// Volunteerssupend() {
//   const requestPayload= {
//     ...this.selectedRules,
//     "update_query":{"status":"Suspend"},
//     "max-results": 2000,
//     "results-offset": 0
// };

//   this.trservice.updateStatusFilter(requestPayload).subscribe((data)=>{

//   })
//   console.log("Query", this.selectedRules)
// }
}
