import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleVisitorMeetComponent } from './schedule-visitor-meet.component';

describe('ScheduleVisitorMeetComponent', () => {
  let component: ScheduleVisitorMeetComponent;
  let fixture: ComponentFixture<ScheduleVisitorMeetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleVisitorMeetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleVisitorMeetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
