import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { TabularreportService } from 'src/app/Services/tabularreport.service';

@Component({
  selector: 'app-schedule-visitor-meet',
  templateUrl: './schedule-visitor-meet.component.html',
  styleUrls: ['./schedule-visitor-meet.component.css']
})
export class ScheduleVisitorMeetComponent implements OnInit {
  params: any;
  visitorData:any;
  get_data_by_query_url='/api/portal/reports/getDatabyquery/';
  constructor(private trservice : TabularreportService,private http: HttpClient) { }

  ngOnInit(): void {
    this.viewVisitor();
    // this.GetDataByQuery()
  }
  // getDataByQuery(reportId:any,body:string){
  //   let url = this.get_data_by_query_url+reportId;
  //   return this.http.post(url,body,this.headers)
  // }
  viewVisitor(){
    let rowData = this.params;
 
    // console.log(rowData);=36
    // this.http.get(`/api/portal/admin/get_vms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
      this.http.get(`/api/portal/reports/getDatabyquery/29`).subscribe((res:any)=>{
         this.trservice.visitorData.next(res.serviceResult)

         // this.insuranceform.patchValue(
    //   {
    //     kriya_member_id:res.serviceResult.kriya_member_id,
    //     poc_mobile:res.serviceResult.poc_mobile,
    //     mobile:res.serviceResult.mobile,
    //     mobile:res.serviceResult.mobile
      // });
    // this.visitorform.patchValue(res.serviceResult);

        // this.visitorData=res.serviceResult
        // document.getElementById('previewVisitorData')?.click()
    })
  } 

  GetDataByQuery(reportId:any,query:any){  
    // this.gridApi.api.showLoadingOverlay();
  
    //   this.rid =reportId;
    //   reportId=19
    //   this.eventQuery =query;
     
    //   query["max-results"]=this.recordsPerPage;
    //   query["results-offset"]=0;
      this.trservice.getDataByQuery(reportId,query).subscribe((res : any)=>{
      //   if (res.serviceResult) { 
      //     { this.toastr.info('Data Filtered','Export',{timeOut: 4000, })  }
      //     this.rowsData = res.serviceResult.results;
      //     let items =res.serviceResult.totalResults;
      //     this.totalItems =res.serviceResult.totalResults;
          
      //     this.totalRecords =Math.ceil(items / this.recordsPerPage) ;
      // }
      })
    
    }
}
