import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-donations',
  templateUrl: './donations.component.html',
  styleUrls: ['./donations.component.css']
})
export class DonationsComponent implements OnInit {

  donations_counts_url='/api/portal/admin/getDonationsCounts';
  reportId:any;
  reportName:any;
  todayDonation:any;
  totalDonation:any;
  NriTodayDonation:any;
  NriTotalDonation:any;
  constructor(private http:HttpClient) { }

  ngOnInit(): void {
    this.reportId =39;
    this.reportName ="Donations Dashboard";
    this.getdonationsCount();
  }

  getdonationsCount(){
    let url = this.donations_counts_url;
    return this.http.get(url)
    .subscribe((res: any) => {
      if (res) { 
          
          this.todayDonation  =  res.serviceResult.today_amount;
          this.totalDonation = res.serviceResult.total_amount;
       
      }

  })
  }

}
