import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PetitionManagementSystemComponent } from './petition-management-system.component';

describe('PetitionManagementSystemComponent', () => {
  let component: PetitionManagementSystemComponent;
  let fixture: ComponentFixture<PetitionManagementSystemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PetitionManagementSystemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PetitionManagementSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
