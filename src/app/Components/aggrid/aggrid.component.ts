import { Component,  Input, OnInit, Output, EventEmitter } from '@angular/core';
import { GridApi } from 'ag-grid-community';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';

@Component({
  selector: 'app-aggrid',
  templateUrl: './aggrid.component.html',
  styleUrls: ['./aggrid.component.css']
})
export class AggridComponent implements OnInit {
  metadatalist: any;
  public overlayLoadingTemplate:any;
  public noRowsTemplate;
  public loadingTemplate;
  actions :any;
 public gridApi :any;
  @Input("reportId") reportId:any;
  @Input("columnsMetadata") columnsMetadata:any;
  @Input("rowsData") rowsData:any;
    
 

  ngOnInit(): void {
  }
   
  constructor() {
    console.log("construct")
    this.loadingTemplate =
      `<span class="ag-overlay-loading-center">data is loading...</span>`;
    this.noRowsTemplate =
      `"<span">no rows to show</span>"`;
      this.actions =
      `<button (click)="btnClickedHandler($event)">Click me!</button>`
  }

  onGridReady(params:any) {
    this.gridApi= params;
     console.log(this.gridApi);
    params.api.showLoadingOverlay();

    setTimeout(() => {
      params.api.hideOverlay();
    }, 500);
  }
  
  
}
