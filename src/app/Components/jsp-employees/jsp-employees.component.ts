import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-jsp-employees',
  templateUrl: './jsp-employees.component.html',
  styleUrls: ['./jsp-employees.component.css']
})
export class JspEmployeesComponent implements OnInit {
  reportId :any;
  reportName :any;
  constructor() { }

  ngOnInit(): void {
    this.reportId =30;
    this.reportName ="JSP EMPLOYEES ";
  }

}
