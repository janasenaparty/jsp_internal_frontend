import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JspEmployeesComponent } from './jsp-employees.component';

describe('JspEmployeesComponent', () => {
  let component: JspEmployeesComponent;
  let fixture: ComponentFixture<JspEmployeesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JspEmployeesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JspEmployeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
