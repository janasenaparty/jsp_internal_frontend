import { EmandateDonationsComponent } from './Components/emandate-donations/emandate-donations.component';
import { KmLastThreeYearsComponent } from './Components/km-last-three-years/km-last-three-years.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChartsComponent } from './Components/all-charts/charts/charts.component';
import { KriyamembersdashboardComponent } from './Components/kriyamembersdashboard/kriyamembersdashboard.component';
import { PhasetwomembershipComponent } from './Components/phasetwomembership/phasetwomembership.component';
import { RenewalmembershipComponent } from './Components/renewalmembership/renewalmembership.component';
// import { PhaseTwoMembershipComponent } from './Components/phase-two-membership/phase-two-membership.component';
// import { RenewalMembershipComponent } from './Components/renewal-membership/renewal-membership.component';
import { TabularreportComponent } from './Components/tabularreport/tabularreport.component';
import { VolunteersComponent } from './Components/volunteers/volunteers.component';
import { HomeLayoutComponent } from './layouts/home-layout/home-layout.component';
import { LineChartComponent } from './Components/all-charts/line-chart/line-chart.component';
import { AuthGuard } from './Services/auth-guard.service';
// import { KriyamembersSummaryReportComponent } from './Components/kriyamembers-summary-report/kriyamembers-summary-report.component';
// import { RenewalsSummaryReporComponent } from './Components/renewals-summary-repor/renewals-summary-repor.component';
// import { KriyaVolunteersDailyReportComponent } from './Components/kriya-volunteers-daily-report/kriya-volunteers-daily-report.component';
import { SummaryReportsComponent } from './Components/summary-reports/summary-reports.component';
import { MapUpiTransactionsComponent } from './Components/map-upi-transactions/map-upi-transactions.component';
import { TgKriyamembershipPhasetwoComponent } from './Components/tg-kriyamembership-phasetwo/tg-kriyamembership-phasetwo.component';
import { TgKriyaMembersDashboardComponent } from './Components/tg-kriya-members-dashboard/tg-kriya-members-dashboard.component';
import { TgKriyaRenewalMembershipComponent } from './Components/tg-kriya-renewal-membership/tg-kriya-renewal-membership.component';
import { TgKriyaVolunteersComponent } from './Components/tg-kriya-volunteers/tg-kriya-volunteers.component';
import { TgSummaryReportsComponent } from './Components/tg-summary-reports/tg-summary-reports.component';
import { KriyaInsuranceFormComponent } from './Components/kriya-insurance-form/kriya-insurance-form.component';
import { KmVotersMappingComponent } from './Components/km-voters-mapping/km-voters-mapping.component';
import { VotersDataComponent } from './Components/voters-data/voters-data.component';
import { PetitionManagementSystemComponent } from './Components/petition-management-system/petition-management-system.component';

import { QrcodeComponent } from './Components/qrcode/qrcode.component';
import { KriyaInsuranceDashboardComponent } from './Components/kriya-insurance-dashboard/kriya-insurance-dashboard.component';
import { VisitorManagementSystemComponent } from './Components/visitor-management-system/visitor-management-system.component';
import { ScheduleVisitorMeetComponent } from './Components/schedule-visitor-meet/schedule-visitor-meet.component';
import { JspEmployeesComponent } from './Components/jsp-employees/jsp-employees.component';
import { YuvaShakthiComponent } from './Components/yuva-shakthi/yuva-shakthi.component';
import { PhaseThreeMembershipComponent } from './Components/phase-three-membership/phase-three-membership.component';
import { TgPhaseThreeMembershipComponent } from './Components/tg-phase-three-membership/tg-phase-three-membership.component';
import { BulkPaymentComponent } from './Components/bulk-payment/bulk-payment.component';
import { KVReferenceComponent } from './Components/kv-reference/kv-reference.component';
import { IssueTrackingSystemComponent } from './Components/issue-tracking-system/issue-tracking-system.component';
import { ExportReportLogsComponent } from './Components/export-report-logs/export-report-logs.component';
import { DonationsComponent } from './Components/donations/donations.component';
import { NRIDonationsComponent } from './Components/nri-donations/nri-donations.component';
import { ConflictManagementSystemComponent } from './Components/conflict-management-system/conflict-management-system.component';
import { SurveyCreatorComponent } from './Components/survey-creator/survey-creator.component';
import { JsonFormComponent } from './Components/json-form/json-form.component';
import { KmLastTwoYearsComponent } from './Components/km-last-two-years/km-last-two-years.component';
import { JanasenaOrgData } from './Components/janasena-org-data/janasena-org-data';
import { DistOrgComponent } from './Components/dist-org/dist-org.component';
import { NriMembershipComponent } from './Components/nri-membership/nri-membership.component';
import { JanasenaMembershipComponent } from './Components/janasena-membership/janasena-membership.component';

const routes: Routes = [
  {
    path: '',
    component: HomeLayoutComponent,
    children: [
      { path: '', redirectTo: '/login', pathMatch: 'full' },

      // { path: '', component:LoginComponent },
      { path: 'kriya_members_dashboard', component:KriyamembersdashboardComponent , canActivate : [AuthGuard]},
      // { path: 'kmd', component:KriyamembersdashboardComponent, canActivate : [AuthGuard]  },
      // { path: 'phase_two_membership', component:PhaseTwoMembershipComponent },
      { path: 'phase_two_membership', component:PhasetwomembershipComponent  },
      { path: 'phase_three_membership', component:PhaseThreeMembershipComponent  },
      // { path: 'renewal_mebership', component:RenewalMembershipComponent},
      { path: 'renewal_mebership', component:RenewalmembershipComponent },

      // { path: '', redirectTo: '/kriya_members_dashboard', pathMatch: 'full' },

      { path: 'tabular-report', component:TabularreportComponent },
      { path: 'map_upi_transactions', component:MapUpiTransactionsComponent },
      { path: 'volunteers', component:VolunteersComponent },
      // { path: 'Tg-Kriyamembership-Phase-two', component:TgKriyamembershipPhasetwoComponent },
      { path: 'Tg_Kriyamembership_Phase_three', component:TgPhaseThreeMembershipComponent },
      { path: 'Tg-KriyaMembers-Dashboard', component:TgKriyaMembersDashboardComponent },
      { path: 'Tg-KriyaRenewal-Membership', component:TgKriyaRenewalMembershipComponent },
      { path: 'Tg-Kriya-Volunteers', component:TgKriyaVolunteersComponent },
      { path: 'Tg-Summary-Reports', component:TgSummaryReportsComponent },
      { path: 'kriya_insurance_form', component:KriyaInsuranceFormComponent },
      { path: 'kriya_insurance_form/edit/:id', component:KriyaInsuranceFormComponent },
      { path: 'kriya_insurance_dashboard', component:KriyaInsuranceDashboardComponent },
      { path: 'kriya_voters_mapping', component:KmVotersMappingComponent },
      { path: 'voters_data', component:VotersDataComponent },
      { path: 'qrcode', component:QrcodeComponent},
      { path: 'petition_management_system', component:PetitionManagementSystemComponent },
      { path: 'visitor_management_system', component:VisitorManagementSystemComponent , canActivate : [AuthGuard]},
      { path: 'visitor_management_system/edit/:seq_id', component:VisitorManagementSystemComponent  },
      { path: 'schedule_visitor_meet', component:ScheduleVisitorMeetComponent  },
      { path: 'jsp_employees', component:JspEmployeesComponent  },
      { path: 'yuva_shakthi', component:YuvaShakthiComponent  },
      { path: 'bulk_payment', component:BulkPaymentComponent  },
      { path: 'kv_reference', component:KVReferenceComponent  },
      { path: 'issue_tracking_system', component:IssueTrackingSystemComponent  },
      { path: 'conflict_management_system', component:ConflictManagementSystemComponent  },
      { path: 'export_report_logs', component:ExportReportLogsComponent  },
      { path: 'donations_dashboard', component:DonationsComponent  },
      { path: 'nri_donations', component:NRIDonationsComponent  },
      { path: 'survey_creater', component:SurveyCreatorComponent  },
      { path: 'dynamic_form', component:JsonFormComponent  },

     { path: 'km_last_two_years', component:KmLastTwoYearsComponent },
     { path: 'km_last_three_years', component:KmLastThreeYearsComponent },
     { path: 'nri_membership', component:NriMembershipComponent },
     { path: 'emandate_donations', component:EmandateDonationsComponent },
     { path: 'general_membership', component:JanasenaMembershipComponent },
     { path: 'dist_org', component:DistOrgComponent },
    //  { path: 'jsp_org_data', component:JanasenaOrgData },
     {path: 'jsp_org_data', component: JanasenaOrgData, data: { breadcrumb: 'jsp_org_data' },
       children: [
         {
           path: '**',
           component: JanasenaOrgData,
           data: {
             breadcrumb: (data: any) => {
               console.log('data', data);
               return `${data.user.name}`;
             },
           }
          }]
          },

      // { path: 'kriyamembers_summary_report', component:KriyamembersSummaryReportComponent},
      // { path: 'renewals_summary_report', component:RenewalsSummaryReporComponent},
      // { path: 'kriya_volunteers_daily_report', component:KriyaVolunteersDailyReportComponent},
      { path: 'summary_reports', component:SummaryReportsComponent},
      { path: 'charts', component:ChartsComponent,
        children: [
          // { path: '', redirectTo: 'line-chart', pathMatch: 'full' },
          { path: 'line-chart', component: LineChartComponent },
          { path: 'bar-chart', component: LineChartComponent },
          { path: 'doughnut-chart', component: LineChartComponent },
          { path: 'pie-chart', component: LineChartComponent },
          { path: 'polararea-chart', component: LineChartComponent },
        ]
    },




    ]
  },
  { path: 'login', loadChildren: () => import('../app/login/login.module').then(m => m.LoginModule) },
  // { path: 'otp', loadChildren: () => import('./otp/otpmodule.module').then (m => m.OtpmoduleModule) },
  // { path: 'reset', loadChildren: () => import('./reset/reset.module').then(m => m.ResetModule) },
  // { path: 'newpassword', loadChildren: () => import('./newpassword/newpassword.module').then(m => m.NewpasswordModule) },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],// OrganizationChartModule, TreeModule, ToastModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
