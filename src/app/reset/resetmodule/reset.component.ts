import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/Services/auth.service';  


@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css']
})
export class ResetComponent implements OnInit {
 
  submitted = false;
  
  
  headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

  // errorMsg: string='';
  // frgtError: boolean=false
 


  constructor(private FB:FormBuilder, private http:HttpClient , private router:Router , private service:AuthService) { }


  forgotform = this.FB.group(
  {
    forgotusername: ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]]
  }
  
  );
  
  
  
  ngOnInit(): void {
  }

  get f(){
    return this.forgotform.controls;
  }

  onSubmit(): void {
    this.submitted = true;

    if (this.forgotform.invalid) {
      return;
    }

    // console.log(JSON.stringify(this.loginform.value, null, 2));
  }



  
  isValidforgot() {
  
  if (this.forgotform.valid) {

  const body =`username=${this.forgotform.value.forgotusername}
  &token=c2VsbHV0ZXNlY3JldA`
  
  // this.service.forgot_form(body).subscribe((res:any)=>{

  // console.log(this.forgotform.value);

  // if (res) {
  // console.log(res)
  // this.router.navigate(['/otp'])
  // }
  // })

  } else{
    // console.log('invalid credentials');
    // this.forgotform.controls.forgotusername.status==="INVALID"?this.frgtError=true:this.frgtError=false

  }
  };
  
  
}


