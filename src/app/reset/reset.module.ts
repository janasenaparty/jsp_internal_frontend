import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { ResetRoutingModule } from './resetrouting.module';
import { ResetComponent } from './resetmodule/reset.component';

@NgModule({
  imports: [
    CommonModule,
    ResetRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
    // HttpModule
  ],
  declarations: [ResetComponent]
})
export class ResetModule { }