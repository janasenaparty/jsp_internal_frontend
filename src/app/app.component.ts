import { Component } from '@angular/core';
import { Router } from '@angular/router';

declare var $: any;
import { QueryBuilderClassNames, QueryBuilderConfig } from 'angular2-query-builder';
// import { GridOptions } from "@ag-grid-community/all-modules";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  classNames: QueryBuilderClassNames = {
    removeIcon: 'fa fa-minus',
    addIcon: 'fa fa-plus',
    arrowIcon: 'fa fa-chevron-right px-2',
    button: 'btn',
    buttonGroup: 'btn-group',
    rightAlign: 'order-12 ml-auto',
    switchRow: 'd-flex px-2',
    switchGroup: 'd-flex align-items-center',
    switchRadio: 'custom-control-input',
    switchLabel: 'custom-control-label',
    switchControl: 'custom-control custom-radio custom-control-inline',
    row: 'row p-2 m-1',
    rule: 'border',
    ruleSet: 'border',
    invalidRuleSet: 'alert alert-danger',
    emptyWarning: 'text-danger mx-auto',
    operatorControl: 'form-control',
    operatorControlSize: 'col-auto pr-0',
    fieldControl: 'form-control',
    fieldControlSize: 'col-auto pr-0',
    entityControl: 'form-control',
    entityControlSize: 'col-auto pr-0',
    inputControl: 'form-control',
    inputControlSize: 'col-auto'
  }

  columnDefs = [
		{headerName: 'Make', field: 'make' },
		{headerName: 'Model', field: 'model' },
		{headerName: 'Price', field: 'price'}
	];

	rowData = [
		{ make: 'Toyota', model: 'Celica', price: 35000 },
		{ make: 'Ford', model: 'Mondeo', price: 32000 },
		{ make: 'Porsche', model: 'Boxter', price: 72000 }
	];
  query = {
    condition: 'and',
    rules: [
      {field: 'age', operator: '<=', value: 'Bob'},
      {field: 'gender', operator: '>=', value: 'm'},
      // {field: 'state', operator: '>=', value: 'ap'},
      // {field: 'district', operator: '>=', value: 'knr'}
    ]
  };

  config: QueryBuilderConfig = {
    fields: {
      age: {name: 'Age', type: 'number'},
      gender: {
        name: 'Gender',
        type: 'category',
        options: [
          {name: 'Male', value: 'm'},
          {name: 'Female', value: 'f'},
          // {name: 'others', value: 'o'}
        ]
      },
      // state: {
      //   name: 'State',
      //   type: 'category',
      //   options:[
      //     {name:'andrapradhesh', value:'ap'},
      //     {name: 'telangana', value:'tg'}
      //   ]
      // },
      // districr: {
      //   name: 'District',
      //   type: 'category',
      //   options:[
      //     {name:'warangal', value:'wgl'},
      //     {name: 'karimnagar', value:'knr'}
      //   ]
      // }

    }
  }
  title = 'admin dashboard';
  constructor( private router:Router){
    
  }
  ngOnInit() {
    if(!localStorage.getItem('isValidUser')){
      // this.service.isLoggedin.next()
      //cmnt
      this.router.navigate(['/login'])
      //end

      // this.service.openLogin.next('inValidUser');

      
    }
    
 }
}



