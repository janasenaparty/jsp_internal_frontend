// -----modules-----
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { DataTablesModule } from 'angular-datatables';
// import 'datatables.net';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { QueryBuilderModule } from "angular2-query-builder";
// import { ChartsModule } from 'ng2-charts';
import { NgChartsModule } from 'ng2-charts';

//import { ChartsModule } from 'ng2-charts';
// -------------components----------
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeLayoutComponent } from './layouts/home-layout/home-layout.component';
import { HeaderComponent } from './layouts/header/header.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { SidenavComponent } from './layouts/sidenav/sidenav.component';
import { OtpComponent } from './Components/otp/otp.component';
import { NewpasswordComponent } from './Components/newpassword/newpassword.component';
import { ResetComponent } from './Components/reset/reset.component';
import { KriyaInsuranceFormComponent } from './Components/kriya-insurance-form/kriya-insurance-form.component';


// ------services------
import { AuthService } from './Services/auth.service';
import { KriyavolunteersService } from './Services/kriyavolunteers.service';
import { KriyamembersService } from './Services/kriyamembers.service';

import { KriyamembersdashboardComponent } from './Components/kriyamembersdashboard/kriyamembersdashboard.component';
// import { AgGridModule } from 'ag-grid-angular';
import { CommonModule } from '@angular/common';

import { QbuilderComponent } from './Components/qbuilder/qbuilder.component';
import { TabularreportComponent } from './Components/tabularreport/tabularreport.component';
import { AggridComponent } from './Components/aggrid/aggrid.component';
import { AgGridModule } from 'ag-grid-angular';
import { CustomLoadingOverlay } from './Components/custom-loading-overlay/custom-loading-overlay.component';
import { PaginationComponent } from './pagination/pagination.component';
import { VolunteersComponent } from './Components/volunteers/volunteers.component';
import { ChartsComponent } from './Components/all-charts/charts/charts.component';
import { BtnCellRenderer } from './Components/btn-cell-renderer/btn-cell-renderer.component';
import { RenewalmembershipComponent } from './Components/renewalmembership/renewalmembership.component';
import { PhasetwomembershipComponent } from './Components/phasetwomembership/phasetwomembership.component';
import { LineChartComponent } from './Components/all-charts/line-chart/line-chart.component';
import { CellCustomComponent } from './Components/tabularreport/actionButtonsComponent';
// import { CellCustomComponent1 } from './Components/tabularreport/actionButtonsComponent';
import { SummaryReportsComponent } from './Components/summary-reports/summary-reports.component';
import { MapUpiTransactionsComponent } from './Components/map-upi-transactions/map-upi-transactions.component';
import { ToastrModule } from 'ngx-toastr';
import { CellCustomComponent1 } from './Components/tabularreport/button-cell-renderer/buttoncellrendererComponent';
import { TgKriyamembershipPhasetwoComponent } from './Components/tg-kriyamembership-phasetwo/tg-kriyamembership-phasetwo.component';
import { TgSummaryReportsComponent } from './Components/tg-summary-reports/tg-summary-reports.component';
import { TgKriyaVolunteersComponent } from './Components/tg-kriya-volunteers/tg-kriya-volunteers.component';
import { TgKriyaRenewalMembershipComponent } from './Components/tg-kriya-renewal-membership/tg-kriya-renewal-membership.component';
import { TgKriyaMembersDashboardComponent } from './Components/tg-kriya-members-dashboard/tg-kriya-members-dashboard.component';
import { DoughnutChartComponent } from './Components/all-charts/doughnut-chart/doughnut-chart.component';
import { ChartsService } from './Services/charts.service';
import { KmVotersMappingComponent } from './Components/km-voters-mapping/km-voters-mapping.component';
import { VotersDataComponent } from './Components/voters-data/voters-data.component';
import { PetitionManagementSystemComponent } from './Components/petition-management-system/petition-management-system.component';
import { NgxPrintModule } from 'ngx-print';
import { QrcodeComponent } from './Components/qrcode/qrcode.component';
import { KriyaInsuranceDashboardComponent } from './Components/kriya-insurance-dashboard/kriya-insurance-dashboard.component';
import { DynamicFormBuilderModule } from './Components/dynamic-form-builder/dynamic-form-builder.module';
import { VisitorManagementSystemComponent } from './Components/visitor-management-system/visitor-management-system.component';
import { ScheduleVisitorMeetComponent } from './Components/schedule-visitor-meet/schedule-visitor-meet.component';
import { JspEmployeesComponent } from './Components/jsp-employees/jsp-employees.component';
import { YuvaShakthiComponent } from './Components/yuva-shakthi/yuva-shakthi.component';
import { PhaseThreeMembershipComponent } from './Components/phase-three-membership/phase-three-membership.component';
import { TgPhaseThreeMembershipComponent } from './Components/tg-phase-three-membership/tg-phase-three-membership.component';

import {ClickableModule} from "./Components/tabularreport/clickable.module";

import {ClickableParentComponent} from "./Components/tabularreport/clickable.parent.component";
import { BulkPaymentComponent } from './Components/bulk-payment/bulk-payment.component';
import { KVReferenceComponent } from './Components/kv-reference/kv-reference.component';
import { IssueTrackingSystemComponent } from './Components/issue-tracking-system/issue-tracking-system.component';
import { ExportReportLogsComponent } from './Components/export-report-logs/export-report-logs.component';
import { DonationsComponent } from './Components/donations/donations.component';
import { NRIDonationsComponent } from './Components/nri-donations/nri-donations.component';
import { ConflictManagementSystemComponent } from './Components/conflict-management-system/conflict-management-system.component';
import { SurveyCreatorComponent } from './Components/survey-creator/survey-creator.component';
// import { SurveyModule } from "survey-angular-ui"
import { SurveyCreatorModule } from 'survey-creator-angular';
import { JsonFormComponent } from './Components/json-form/json-form.component';
import { KmLastTwoYearsComponent } from './Components/km-last-two-years/km-last-two-years.component';
import { KmLastThreeYearsComponent } from './Components/km-last-three-years/km-last-three-years.component';
import { JanasenaOrgDataModule } from './Components/janasena-org-data/janasena-org-data.module';
import { BreadcrumbService } from './Services/breadcrumb.service';
import { DistOrgComponent } from './Components/dist-org/dist-org.component';
import { NriMembershipComponent } from './Components/nri-membership/nri-membership.component';
import { EmandateDonationsComponent } from './Components/emandate-donations/emandate-donations.component';
import { JanasenaMembershipComponent } from './Components/janasena-membership/janasena-membership.component';
// import { BreadcrumbComponent } from './Components/janasena-org-data/breadcrumb/breadcrumb.component';

// import { AgGridModule } from "ag-grid-community";






@NgModule({
  declarations: [
    AppComponent,
    CellCustomComponent,
    HomeLayoutComponent,
    HeaderComponent,
    FooterComponent,
    SidenavComponent,
    OtpComponent,
    NewpasswordComponent,
    ResetComponent,
    DoughnutChartComponent,
    // KriyavolunteersComponent,
    // KriyamembersComponent,

    KriyamembersdashboardComponent,
    QbuilderComponent,
    TabularreportComponent,
    AggridComponent,
    CustomLoadingOverlay,
    PaginationComponent,
    VolunteersComponent,
    LineChartComponent,
    ChartsComponent,
    BtnCellRenderer,
    RenewalmembershipComponent,
    PhasetwomembershipComponent,
    SummaryReportsComponent,
    MapUpiTransactionsComponent,
    CellCustomComponent1,
    TgKriyamembershipPhasetwoComponent,
    TgSummaryReportsComponent,
    TgKriyaVolunteersComponent,
    TgKriyaRenewalMembershipComponent,
    TgKriyaMembersDashboardComponent,
    KriyaInsuranceFormComponent,
    KmVotersMappingComponent,
    VotersDataComponent,
    PetitionManagementSystemComponent,
    QrcodeComponent,
    KriyaInsuranceDashboardComponent,
    VisitorManagementSystemComponent,
    ScheduleVisitorMeetComponent,
    JspEmployeesComponent,
    YuvaShakthiComponent,
    PhaseThreeMembershipComponent,
    TgPhaseThreeMembershipComponent,
    BulkPaymentComponent,
    KVReferenceComponent,
    IssueTrackingSystemComponent,
    ExportReportLogsComponent,
    DonationsComponent,
    NRIDonationsComponent,
    ConflictManagementSystemComponent,
    SurveyCreatorComponent,
    JsonFormComponent,
    KmLastTwoYearsComponent,
    KmLastThreeYearsComponent,
    DistOrgComponent,
    NriMembershipComponent,
    EmandateDonationsComponent,
    JanasenaMembershipComponent,
    // JsonFormComponent
    // BreadcrumbComponent

  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // DataTablesModule,
    BrowserAnimationsModule, DynamicFormBuilderModule,
    // FlexLayoutModule,
    // HttpClient,
    HttpClientModule,
    QueryBuilderModule,
  // IonicModule.forRoot(AppComponent),
  // DynamicFormBuilderModule,
  NgxPrintModule,
    AgGridModule.withComponents([KriyamembersdashboardComponent,CustomLoadingOverlay, ClickableParentComponent]),
    NgChartsModule,
    ToastrModule.forRoot(),
    ClickableModule,
    SurveyCreatorModule,

  JanasenaOrgDataModule,

    ],
  providers: [
    AuthService,
    ChartsService,
    KriyavolunteersService,
    KriyamembersService,
    BreadcrumbService
    // { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true }

  ],
  entryComponents: [CellCustomComponent,BtnCellRenderer,CellCustomComponent1],
   exports: [
    ] ,
  bootstrap: [AppComponent]
})
export class AppModule { }
export class SurveyModule { }
