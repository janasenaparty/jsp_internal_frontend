import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/Services/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
  showMessage: any;
  errorMsg: string = '';
  submited = false;
  isRole: any;
  x:any;
  passType: string = 'password';
  show_button: Boolean = false;
  show_eye: Boolean = false;
  // nameError: boolean=false
  // passError: boolean=false

  // errorMsg: string='';
  // nameError: boolean=false
  // passError: boolean=false
  // loginform: FormGroup;
  submitted = false;
  isValidUser = false;

  constructor(private toastr: ToastrService, private FB: FormBuilder, private http: HttpClient, private router: Router, private service: AuthService) { }


  // loginform = this.FB.group(
  // {
  //   username: ['', Validators.required],
  //   password: ['', Validators.required],

  // }

  // );

  loginform = this.FB.group(
    {
      username: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(16)]],
    },

  );

  ngOnInit(): void {
    this.isValidUser = localStorage.getItem("isValidUser") ? true : false


  }

  get f() {
    return this.loginform.controls;
  }

  onSubmit(): void {

    console.log(this.f)
    this.submitted = true;

    if (this.loginform.invalid) {
      return;
    }

    // console.log(JSON.stringify(this.loginform.value, null, 2));
  }

  // GetDataByQuery(reportId:any,query:any){
  //   this.eventQuery =query;

  //   query["max-results"]=this.recordsPerPage;
  //   query["results-offset"]=0;
  //   this.trservice.getDataByQuery(reportId,query).subscribe((res : any)=>{
  //     if (res.serviceResult) {
  //       this.rowsData = res.serviceResult.results;
  //       let items =res.serviceResult.totalResults;

  //       this.totalRecords =Math.ceil(items / this.recordsPerPage) ;

  //   }
  //   })

  // }

//  myFunction() {
//      this.x = document.getElementById("myInput");
//     if (this.x.type === "password") {
//       this.x.type = "text";
//     } else {
//       this.x.type = "password";
//     }
//   }
showPassword() {
  this.show_button = !this.show_button;
  this.show_eye = !this.show_eye;
}

  isValidlogin() {
    // this.router.navigate(['/kriya_members_dashboard'])

    if (this.loginform.valid) {
      const body = { "username": this.loginform.value.username, "password": this.loginform.value.password }
      // const body =`username:&password:${this.loginform.value.password}`

      this.service.isLoggedIn(body).subscribe((res: any) => {

        if (!res.serviceError) {
          //  alert()
          // this.toastr.success('Logged In Successfully',res.serviceResult.name,{timeOut: 4000, })
          this.toastr.success('Logged In Successfully', res.serviceResult.name, { timeOut: 4000, })
          console.log(res)
          localStorage.setItem('isRole', res.serviceResult.role)
          localStorage.setItem('isName', res.serviceResult.name)
          localStorage.setItem('isMail', res.serviceResult.user)
          // this.toastr.success(res.serviceResult.name,'Welcome',{timeOut: 8000, })
          // this.toastr.success('Welcome',res.serviceResult.name,{timeOut:4000, })
         this.service.userSubject.next( res.serviceResult.role);
          console.log(localStorage.getItem('isValidUser'))
          localStorage.setItem('isValidUser', "true")

          this.isValidUser = true;
          if (res.serviceResult.role == 'GS') {
            this.router.navigate(['/petition_management_system'])
          }
          else if (res.serviceResult.role == 'VS'|| res.serviceResult.role == 'PA')  {
            this.router.navigate(['/visitor_management_system'])
          }
          else if (res.serviceResult.role == 'IS')  {
            this.router.navigate(['/issue_tracking_system'])
          }
          else if (res.serviceResult.role == 'CS')  {
            this.router.navigate(['/conflict_management_system'])
          }
          else if (res.serviceResult.role == 'OS') {
            this.router.navigate(['/jsp_org_data'])
          }
          else if (res.serviceResult.role == 'ED') {
            this.router.navigate(['/emandate_donations'])
          }

          else  {
            this.router.navigate(['/kriya_members_dashboard'])
          }


        }else{
          this.toastr.error(res.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' })

        }

        //  (response) => { this.departMents =response.serviceResult; },
        // (error) => {    this.toastr.error(error.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' }) }

      },(err)=>{
        this.toastr.error(err.error.serviceError,'Error',{timeOut: 4000, positionClass: 'toast-top-center' })
      })
    }
  };


}


