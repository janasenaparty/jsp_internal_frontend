
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { LoginRoutingModule } from './loginrouting.module';
import { LoginComponent } from './loginmodule/login.component';

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    // HttpClient
    // HttpModule
    
  ],
  declarations: [LoginComponent]
})
export class LoginModule { }