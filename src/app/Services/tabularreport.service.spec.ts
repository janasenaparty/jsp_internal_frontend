import { TestBed } from '@angular/core/testing';

import { TabularreportService } from './tabularreport.service';

describe('TabularreportService', () => {
  let service: TabularreportService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TabularreportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
