import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  userSubject:BehaviorSubject<any>;
  userRoleId:Observable<any>;
  // meta_data_url=`${environment.base_ulr}/api/portal/reports/getTablemetadata/`;

  // loginurl='http://audience.tvishasystems.com/all-demos/sellute/public/api/web/admin-login'
  // logouturl='http://audience.tvishasystems.com/all-demos/sellute/public/api/web/admin-logout'
  // forgoturl='http://audience.tvishasystems.com/all-demos/sellute/public/api/web/admin-forgot-password'
  // otpurl='http://audience.tvishasystems.com/all-demos/sellute/public/api/web/admin-otp-verify'
  // newpassurl='http://audience.tvishasystems.com/all-demos/sellute/public/api/web/admin-change-password'
    is_user_authenticated=`/api/portal/admin/isUserAuthenticated/`;
    login_url='/api/portal/admin/login';
    logout_url='/api/portal/admin/logout';
  constructor( private http: HttpClient ) { 
 this.userSubject = new BehaviorSubject<any>(localStorage.getItem('isRole'));
  this.userRoleId =this.userSubject.asObservable();

  }
  public get getRoleId():any{
    this.checkAuthentication;
    return this.userSubject.value;
  }
   
  headers = {headers: new HttpHeaders({ 'Content-Type': 'application/json'})};

  // headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
 
  get checkAuthentication(){
    let url = this.is_user_authenticated;
    this.http.get<any>('/api/portal/admin/isUserAuthenticated').subscribe(res=>{
      this.userSubject.next(res.serviceResult.role)
     if(res){
       return true
     }else{
       return false
     }
    })
    return true
  };
  isLoggedIn(body:any){
    let url = this.login_url;
    return this.http.post(url,body,this.headers)
  };
  isLoggedOut(){
    let url = this.logout_url;
    return this.http.get(url)
  }

  // login_form(body: any){
  //   return this.http.post(this.loginurl, body, { headers: this.headers })
    
  // };

  // forgot_form(body: any){
  //     return this.http.post(this.forgoturl, body, { headers: this.headers })
      
  // };
    
  // otp_form(body: any){
  //       return this.http.post(this.otpurl, body, { headers: this.headers })
        
  // };

  // newpass_form(body: any){
  //   return this.http.post(this.newpassurl, body, { headers: this.headers })
    
  // };
    
        
    }




