import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { environment } from 'src/environments/environment';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TabularreportService {
  blobToDataURL(blob: Blob) {
    throw new Error('Method not implemented.');
  }
  updateStatusFilter(requestPayload: any) {
     return this.http.post('/api/portal/admin/update_bulk_volunteers_status',requestPayload);
  }
  // getLeaders() {
  //   throw new Error('Method not implemented.');
  // }




  // meta_data_url=`${environment.base_ulr}/api/portal/reports/getTablemetadata/`;
  meta_data_url='api/portal/reports/getTablemetadata/';
  // get_data_by_query_url=`${environment.base_ulr}/api/portal/reports/getDatabyquery/`;
  get_data_by_query_url='/api/portal/reports/getDatabyquery/';
  deleterowUrl='/api/portal/admin/delete_kriya_member_details';
  summary_counts_url='/api/portal/admin/kriya/summary_counts';
  nri_donations_count='/api/portal/admin/getNRIDonationsCounts';
  donations_count='/api/portal/admin/getDonationsCounts';
  org_charts ='api/portal/admin/get_committee_data?committee_name=State Committee&committee_level=State'
  // update_aadhar_photo_url='/api/portal/admin/update_kriya_member_files';
  // https://prod.janasenaparty.org/api/portal/admin/update_kriya_member_files


headers = {headers: new HttpHeaders({ 'Content-Type': 'application/json'})};
headers1 = {headers: new HttpHeaders({ 'Content-Type': 'multipart/form-data'})};
headers2 = new HttpHeaders({ 'Content-Type':'multipart/form-data'});
    paymentImageUrl=new Subject();
  userData=new Subject();
  voterData= new Subject();
  volunteerData= new Subject();
  petitionerData= new Subject();
  issueData= new Subject();
  conflictData= new Subject();
  visitorData= new Subject();
  insuranceData= new  Subject();
  employeeData= new  Subject();
  empQrData=new  Subject();
  deleteRowID: any;
  utrId:any;

  constructor(private http:HttpClient) { }
  getMetaData(reportId:any){
    let url = this.meta_data_url+reportId;
    return this.http.get(url)
  }
  getDataByQuery(reportId:any,body:string){
    let url = this.get_data_by_query_url+reportId;
    return this.http.post(url,body,this.headers)
  }
  getDataExport(reportId:any,body:any){
    let url = `/api/portal/reports/exportReport`;
    const finalBody={"report_id":reportId,"rules":body.rules}
    return this.http.post(url,finalBody,this.headers)
    // this.trservice.getDataByQuery(reportId,query).subscribe((res : any)=>{
  }

  deleteRow(){
   return this.http.get(`${this.deleterowUrl}?id=${this.deleteRowID}`)
  };
  editUtr(){
    return this.http.get(`${this.get_data_by_query_url}?id=${this.utrId}`)
  };

  getSummaryCount(){
    let url = this.summary_counts_url;
    return this.http.get(url)
  };


  updateUtrId(newpayment_id:any,id:any,payment_id:any){
    let url = `/api/portal/admin/update_kriya_member_payment_details`;
    const finalBody={"id":id,"payment_id":payment_id,"new_payment_id":newpayment_id}
    return this.http.post(url,finalBody,this.headers)
    // this.trservice.getDataByQuery(reportId,query).subscribe((res : any)=>{
  };

  updateUtrRemarks( remarks : any, id:any){
    let url = `/api/portal/admin/update_kriya_member_remarks`;

    const finalBody={"id":id,"remarks":remarks}
    return this.http.post(url,finalBody,this.headers)
   };

  updateVoterId( id:any, voter_id:any){
    let url = `/api/portal/admin/map_kriya_member_voter_id`;
    // voter_id = id.voter_id();
    const finalBody={"id":id,"voter_id":voter_id}
    return this.http.post(url,finalBody,this.headers)
  }
  //use it if  they want later
  // state:any,district_id:any,constituency_id:any
  // "state":state,"district_id":district_id,"constituency_id":constituency_id,
  updateMemberDetails( remarks : any, id:any,dob:any ,fullname:any,aadhar_number:any,email:any,mobile:any, gender:any,address:any ){
    let url = `/api/portal/admin/update_member_details`;
    aadhar_number = aadhar_number.toString();
    mobile = mobile.toString();
    // mobile = mobile.toString();
    // mobile = mobile.toString();
    const finalBody={"id": id,"mobile":mobile,"fullname":fullname,"remarks":remarks,"address":address,"gender":gender,"dob":dob,"aadhar_number":aadhar_number,"email":email}
    return this.http.post(url,finalBody,this.headers)
  }
  updateNomineeMemberDetails(id:any,nominee_name:any ,nominee_aadhar_number:any){
    let url = `/api/portal/admin/update_member_nominee_details`;
    nominee_aadhar_number = nominee_aadhar_number.toString();
    const finalBody={"id": id,"nominee_name":nominee_name,"nominee_aadhar_number":nominee_aadhar_number}
    return this.http.post(url,finalBody,this.headers)
  }

  updateUserPhoto(id:any,type:any,user_photo:any){
    console.log(user_photo);
    let url = `/api/portal/admin/update_kriya_member_files`;
    // nominee_aadhar_number = nominee_aadhar_number.toString();
    const finalBody={"id": id,"type": 'user_photo',"user_photo":user_photo}
    let headers = new HttpHeaders();
    /** In Angular 5, including the header Content-Type can invalidate your request */
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    let options =  { headers: headers };
    let formData:FormData = new FormData();
    formData.append('id', id);
    formData.append('type', "user_photo");
    formData.append('user_photo', user_photo, user_photo.name);
    return this.http.post(url,formData,options)
  }
  updateAadharCard(  id:any,type:any,aadhar_card:any ){
    console.log(aadhar_card.name);
    let url = `/api/portal/admin/update_kriya_member_files`;
    // nominee_aadhar_number = nominee_aadhar_number.toString();
    const finalBody={"id": id,"type": 'aadhar',"aadhar_card":aadhar_card}
    let headers = new HttpHeaders();
    /** In Angular 5, including the header Content-Type can invalidate your request */
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    let options =  { headers: headers };
    let formData1:FormData = new FormData();
    formData1.append('id', id);
    formData1.append('type', "aadhar");
    formData1.append('aadhar_card', aadhar_card, aadhar_card.name);
    return this.http.post(url,formData1,options)
  }

  getDepartments(): Observable<any> {
    let url = `/api/portal/admin/get_pms_departments`;
    return this.http.get(url);
  }
  getCategoriesByDep(department:string): Observable<any> {
    let url = `/api/portal/admin/get_pms_categories?department=`+department;
    return this.http.get(url);
  }

  updatePetitionStatus( seq_id:number,status:any): Observable<any>{

    const url = `/api/portal/admin/update_pms_status`;
   const requestBody ={"id":seq_id,"status":status};

    return this.http.post(url,requestBody,this.headers)
  }
  getDistricts(): Observable<any>{
    let url = `/assets/districts_only.json`;
    return this.http.get(url);
  }
  getConstituencies(): Observable<any>{
    let url = `/assets/constituencies_district.json`;
    return this.http.get(url);
  }
  getVillages(state_id:number,constituency_id:number): Observable<any>{
    let url = `api/portal/admin/get_mandals_list?state_id=${1}&constituency_id=${constituency_id}`;
    return this.http.get(url);
  }
  getBooths(state_id:number,constituency_id:number,mandal_id:number,village_id:number): Observable<any>{
    let url = `api/portal/admin/get_booths_by_village?state_id=${1}&constituency_id=${constituency_id}&mandal_id=${mandal_id}&village_id=${village_id}`;
    return this.http.get(url);
  }
  getModals(): Observable<any>{
    // let url = `/assets/districts_mandals.json`;
    let url = `/assets/mandals_only.json`;
    return this.http.get(url);
  }
  getAPMandals(): Observable<any>{
    // let url = `/assets/districts_mandals.json`;
    let url = `/assets/ap_mandals_only.json`;
    return this.http.get(url);
  }
  deletPetition(seq_id:number){
    let url = `/api/portal/admin/delete_pms_details_by_id`
    //  // console.log(rowData);=36
    //   // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
        return this.http.get(`/api/portal/admin/delete_pms_details_by_id?id=${seq_id}`);
    //  document.getElementById('deletPetitionData')?.click();

    //  })
  }
  deletVisitor(seq_id:number){
    let url = `/api/portal/admin/delete_vms_details_by_id?id=${seq_id}`
    const requestBody ={"id":seq_id};
    //  // console.log(rowData);=36
    //   // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
        // return this.http.post(`/api/portal/admin/delete_vms_details_by_id?id=${seq_id}`);
        return this.http.post(url,requestBody,this.headers)
        //  document.getElementById('deletPetitionData')?.click();

    //  })
  }
  // deletVolunteer(id:number){
  //   let url = `/api/portal/admin/delete_kriya_volunteer_details?id=${id}`
  //   const requestBody ={"id":id};
  //   //  // console.log(rowData);=36
  //   //   // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
  //       // return this.http.post(`/api/portal/admin/delete_vms_details_by_id?id=${seq_id}`);
  //       return this.http.get(url,requestBody)
  //       //  document.getElementById('deletPetitionData')?.click();

  //   //  })
  // }
  deletVolunteer(id:number){
    // let url = `/api/portal/admin/delete_kriya_volunteer_details`
    //  // console.log(rowData);=36
    //   // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
        return this.http.get(`/api/portal/admin/delete_kriya_volunteer_details?id=${id}`);
    //  document.getElementById('deletPetitionData')?.click();

    //  })
  }
  addPMS( requestBody:any): Observable<any>{
    let url = `/api/portal/admin/add_pms`;
    requestBody.constituency_id = Number(requestBody.constituency_id);
    requestBody.district_id = Number(requestBody.district_id);
    requestBody.mandal = Number(requestBody.mandal);
    // voter_id = id.voter_id();
  //    requestBody.files =[
  //     "https://janasenabackup.blob.core.windows.net/kriyamembers/XVE1LX2KCRV9C8CNLXDHWYS169SEAAN120220627-184539.png"
  // ];

    return this.http.post(url,requestBody,this.headers)
  }
  updatePms( requestBody:any,id:number): Observable<any>{
    let url = `/api/portal/admin/update_pms_details`;
     requestBody.title =requestBody.subject;
     requestBody.id =id;
    // requestBody.constituency_id = Number(requestBody.constituency_id);
    // requestBody.district_id = Number(requestBody.district_id);
    // requestBody.mandal = Number(requestBody.mandal);

    // voter_id = id.voter_id();
  //    requestBody.files =[
  //     "https://janasenabackup.blob.core.windows.net/kriyamembers/XVE1LX2KCRV9C8CNLXDHWYS169SEAAN120220627-184539.png"
  // ];

    return this.http.post(url,requestBody,this.headers)
  }
  updateVms( requestBody:any,seq_id:number): Observable<any>{
    let url = `/api/portal/admin/update_vms_details`;
    //  requestBody.title =requestBody.subject;
     requestBody.seq_id =seq_id;
    // requestBody.constituency_id = Number(requestBody.constituency_id);
    // requestBody.district_id = Number(requestBody.district_id);
    // requestBody.mandal = Number(requestBody.mandal);

    // voter_id = id.voter_id();
  //    requestBody.files =[
  //     "https://janasenabackup.blob.core.windows.net/kriyamembers/XVE1LX2KCRV9C8CNLXDHWYS169SEAAN120220627-184539.png"
  // ];

    return this.http.post(url,requestBody,this.headers)
  }

  suspendKMS( id:any,suspended_by:any,reason:any,file_url:any): Observable<any>{
    let url = `/api/portal/admin/suspend_kriya_member`;

    const requestBody ={"id":id,"file_url":file_url,"suspended_by":suspended_by,"reason":reason};
    return this.http.post(url,requestBody,this.headers)
  }
  uploadKMSFiles(formData1:any): Observable<any>{
    let url = `/api/portal/admin/upload_kms_files`;
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    let options =  { headers: headers };

    return this.http.post(url,formData1,options)
  }
  uploadCMSFiles(formData:any): Observable<any>{
    let url = `/api/portal/admin/upload_kms_files`;
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    let options =  { headers: headers };

    return this.http.post(url,formData,options)
  }
  uploadPtitionFiles(id:any,uploadfiles:any): Observable<any>{
    let url = `/api/portal/admin/update_pms_files`;
    let requestBody ={
      'id' : id,
      'files' :uploadfiles
    }

    return this.http.post(url,requestBody,this.headers)
  }


  uploadFiles(formData1:any): Observable<any>{
    let url = `/api/portal/admin/upload_pms_files`;
    let headers = new HttpHeaders();
    /** In Angular 5, including the header Content-Type can invalidate your request */
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    let options =  { headers: headers };

    return this.http.post(url,formData1,options)
  }
  addInsurenceForm( requestBody:any): Observable<any>{
    const url = `/api/portal/claims/add_claim`;
    requestBody.kriya_member_id =Number( requestBody.id);
    // voter_id = id.voter_id();
  //    requestBody.files =[
  //     "https://janasenabackup.blob.core.windows.net/kriyamembers/XVE1LX2KCRV9C8CNLXDHWYS169SEAAN120220627-184539.png"
  // ];

    return this.http.post(url,requestBody,this.headers)
  }

  getInsDetailsById(id: string,value: string): Observable<any> {
    const url = `/api/portal/claims/search_kriya_member`;
    return this.http.get(url+'?search_type='+id+'&search_value='+value)
  }


      deletInsurance(id:number){
        let url = `/api/portal/claims/delete_claim_details_by_id`
        //  // console.log(rowData);=36
        //   // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
            return this.http.get(`/api/portal/claims/delete_claim_details_by_id?id=${id}`);
        //  document.getElementById('deletPetitionData')?.click();

        //  })
      }

      updateInsurance( requestBody:any,id:number): Observable<any>{
        let url = `/api/portal/claims/update_claims_details`;
        const payload ={
                    remarks:requestBody.remarks,
                accident_date:requestBody.accident_date,
                intimation_date:requestBody.intimation_date,
                id:id,
                claim_status: requestBody.claim_status
        }
        return this.http.post(url,payload,this.headers)
      }
      getInsurencDetailsById(id:number): Observable<any>{
       return this.http.get(`/api/portal/claims/get_claim_details_by_id?id=${id}`);
      }


      addVMS( requestBody:any): Observable<any>{
        let url = `/api/portal/admin/add_visitor`;
        requestBody.constituency_id = Number(requestBody.constituency_id);
        requestBody.district_id = Number(requestBody.district_id);
        requestBody.mandal_id = Number(requestBody.mandal_id);
        requestBody.state_id = Number(requestBody.state_id);
        requestBody.visitor_phone = String(requestBody.visitor_phone);

        // voter_id = id.voter_id();
      //    requestBody.files =[
      //     "https://janasenabackup.blob.core.windows.net/kriyamembers/XVE1LX2KCRV9C8CNLXDHWYS169SEAAN120220627-184539.png"
      // ];

        return this.http.post(url,requestBody,this.headers)
      }
      addLeader( requestBody:any): Observable<any>{
        let url = `/api/portal/admin/add_vms_leader`;

        return this.http.post(url,requestBody,this.headers)
      }


      // getLeader(): Observable<any>{
      //   return this.http.get(`/api/portal/admin/get_vms_leaders`);
      //  }
       addReason( requestBody:any): Observable<any>{
        let url = `/api/portal/admin/add_vms_reson`;

        return this.http.post(url,requestBody,this.headers)
      }
      // getLeaderById(seq_id:number): Observable<any>{
      //   return this.http.get(`/api/portal/admin/get_vms_leader_details_by_id?seq_id=${seq_id}`);
      //  }
       deleteLeaderById(seq_id:number): Observable<any>{
        return this.http.post(`/api/portal/admin/delete_vms_leader_details_by_id?seq_id=${seq_id}`,'');
       }
      //  getReasonById(seq_id:number): Observable<any>{
      //   return this.http.get(`/api/portal/admin/get_vms_reason_details_by_id?seq_id=${seq_id}`);
      //  }

        deleteReasonById(seq_id:number): Observable<any>{
        return this.http.post(`/api/portal/admin/delete_vms_reasons_by_id?seq_id=${seq_id}`,'');

       }
      setVisitorMeet( requestBody:any,seq_id:number): Observable<any>{
        let url = `/api/portal/admin/schedule_appointment`;
        requestBody.seq_id =seq_id;
        return this.http.post(url,requestBody,this.headers)
      }
      addEmployee( requestBody:any): Observable<any>{
        let url = `/api/portal/employees/add_employee`;

        return this.http.post(url,requestBody,this.headers)
      }
      getEmployee(emp_id:number): Observable<any>{
        return this.http.get(`/api/portal/employees/get_employee_details_by_id?emp_id=${emp_id}`);
       }

       updateEmployee( requestBody:any,emp_id:number): Observable<any>{
        let url = `/api/portal/employees/update_employee_details`;
        //  requestBody.title =requestBody.subject;
         requestBody.emp_id =emp_id;
        // requestBody.constituency_id = Number(requestBody.constituency_id);
        // requestBody.district_id = Number(requestBody.district_id);
        // requestBody.mandal = Number(requestBody.mandal);

        // voter_id = id.voter_id();
      //    requestBody.files =[
      //     "https://janasenabackup.blob.core.windows.net/kriyamembers/XVE1LX2KCRV9C8CNLXDHWYS169SEAAN120220627-184539.png"
      // ];

        return this.http.post(url,requestBody,this.headers)
      }
      // deleteEmployeeById1(seq_id:number): Observable<any>{
      //   return this.http.post(`/api/portal/employees/delete_employee_details_by_id?id=${seq_id}`,'');

      //  }




       deleteEmployeeById(seq_id:number){
        let url = `/api/portal/employees/delete_employee_details_by_id?emp_id=${seq_id}`
        const requestBody ={"emp_id":seq_id};

            return this.http.post(url,requestBody,this.headers)

      }

      updateEmployeePhoto(userPhono:any,emp_id:any,type:any,photo:any){
        console.log(photo);
        let url = `/api/portal/employees/change_employee_photo`;
        // nominee_aadhar_number = nominee_aadhar_number.toString();
        const finalBody={"emp_id": emp_id,"photo":photo}
        let headers = new HttpHeaders();
        /** In Angular 5, including the header Content-Type can invalidate your request */
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        let options =  { headers: headers };
        let formData:FormData = new FormData();
        formData.append('emp_id', emp_id);
        formData.append('photo', userPhono);
        return this.http.post(url,formData,options)
      }
      getBlobData(emp_id:any): Observable<Blob> {
        const apiUrl = `/api/portal/employees/get_employee_qrcode_by_id?emp_id=${emp_id}`; // Replace with your API endpoint
        const headers = new HttpHeaders({
          'Content-Type': 'application/json',
          // You may need to set additional headers as per your API requirements
        });

        return this.http.get(apiUrl, { responseType: 'blob'});
      }
      getBlobDataFromAPI(emp_id:any): Observable<Blob> {
        const apiUrl = `/api/portal/employees/get_employee_qrcode_by_id?emp_id=${emp_id}`;
        return this.http.get(apiUrl, { responseType: 'blob' });
      }
      // updateAadharCard1(  id:any,type:any,aadhar_card:any ){
      //   console.log(aadhar_card.name);
      //   let url = `/api/portal/admin/update_kriya_member_files`;
      //   // nominee_aadhar_number = nominee_aadhar_number.toString();
      //   const finalBody={"id": id,"type": 'aadhar',"aadhar_card":aadhar_card}
      //   let headers = new HttpHeaders();
      //   /** In Angular 5, including the header Content-Type can invalidate your request */
      //   headers.append('Content-Type', 'multipart/form-data');
      //   headers.append('Accept', 'application/json');
      //   let options =  { headers: headers };
      //   let formData1:FormData = new FormData();
      //   formData1.append('id', id);
      //   formData1.append('type', "aadhar");
      //   formData1.append('aadhar_card', aadhar_card, aadhar_card.name);
      //   return this.http.post(url,formData1,options)
      // }
      addITS( requestBody:any): Observable<any>{
        let url = `/api/portal/jim/add_jim`;
        requestBody.constituency_id = Number(requestBody.constituency_id);
        requestBody.district_id = Number(requestBody.district_id);
        requestBody.mandal = Number(requestBody.mandal);
        // voter_id = id.voter_id();
      //    requestBody.files =[
      //     "https://janasenabackup.blob.core.windows.net/kriyamembers/XVE1LX2KCRV9C8CNLXDHWYS169SEAAN120220627-184539.png"
      // ];

        return this.http.post(url,requestBody,this.headers)
      }
      updateITS( requestBody:any,id:number): Observable<any>{
        let url = `/api/portal/jim/update_jim_details`;
         requestBody.title =requestBody.subject;
         requestBody.id =id;
        // requestBody.constituency_id = Number(requestBody.constituency_id);
        // requestBody.district_id = Number(requestBody.district_id);
        // requestBody.mandal = Number(requestBody.mandal);

        // voter_id = id.voter_id();
      //    requestBody.files =[
      //     "https://janasenabackup.blob.core.windows.net/kriyamembers/XVE1LX2KCRV9C8CNLXDHWYS169SEAAN120220627-184539.png"
      // ];

        return this.http.post(url,requestBody,this.headers)
      }

      deletIssue(seq_id:number){
        // let url = `/api/portal/jim/delete_jim_details_by_id?seq_id=6`
        //  // console.log(rowData);=36
        //   // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
            return this.http.get(`/api/portal/jim/delete_jim_details_by_id?seq_id=${seq_id}`);
         document.getElementById('deletIssueData')?.click();

        //  })
      }
      addCMS( requestBody:any): Observable<any>{
        let url = `/api/portal/cms/add_cms`;
        requestBody.constituency_id = Number(requestBody.constituency_id);
        requestBody.district_id = Number(requestBody.district_id);
        requestBody.mandal_id = Number(requestBody.mandal_id);
        requestBody.village_id = Number(requestBody.village_id);
        requestBody.polling_station_id = Number(requestBody.polling_station_id);
        // voter_id = id.voter_id();
      //    requestBody.files =[
      //     "https://janasenabackup.blob.core.windows.net/kriyamembers/XVE1LX2KCRV9C8CNLXDHWYS169SEAAN120220627-184539.png"
      // ];

        return this.http.post(url,requestBody,this.headers)
      }

      deletConflict(seq_id:number){
        // let url = `/api/portal/jim/delete_jim_details_by_id?seq_id=6`
        //  // console.log(rowData);=36
        //   // this.http.get(`/api/portal/admin/get_pms_details_by_id?id=${rowData.data.id}`).subscribe((res:any)=>{
            return this.http.get(`/api/portal//cms/delete_cms_details_by_id?seq_id=${seq_id}`);

        //  })
      }
      updateCMS ( seq_id:number,requestBody:any,attachment_urls:any): Observable<any>{
        let url = `/api/portal/cms/update_cms_details`;
         requestBody.title =requestBody.subject;
         requestBody.seq_id =seq_id;
         requestBody.attachment_urls=attachment_urls;

        // requestBody.constituency_id = Number(requestBody.constituency_id);
        // requestBody.district_id = Number(requestBody.district_id);
        // requestBody.mandal = Number(requestBody.mandal);

        // voter_id = id.voter_id();
      //    requestBody.files =[
      //     "https://janasenabackup.blob.core.windows.net/kriyamembers/XVE1LX2KCRV9C8CNLXDHWYS169SEAAN120220627-184539.png"
      // ];

        return this.http.post(url,requestBody,this.headers)
      }

      getTelangana(): Observable<any>{
        let url = `/api/portal/kriya/masterdata?state=2`;
        return this.http.get(url);
      }

      getnridonationsCount(){
        let url = this.nri_donations_count;
        return this.http.get(url)
      };
      getdonationsCount(){
        let url = this.donations_count;
        return this.http.get(url)
      };

      getCommitteeData(committeeName: string, committeeLevel: string): Observable<any> {
        const url = `/api/portal/admin/get_committee_data?committee_name=${committeeName}&committee_level=${committeeLevel}`;
        return this.http.get(url);
      }
}
