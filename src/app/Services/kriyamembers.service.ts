import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class KriyamembersService {
  kriya_member_url="https://nivedika.janasenaparty.org/get_kriya_members";
  // table_metadata_url="/admin/getTablemetadata/" + 'report_id';
  table_metadata_url="https://nivedika.janasenaparty.org/admin/getTablemetadata/kriya_members"
  kriya_members_filters_url="https://nivedika.janasenaparty.org/admin/kriyaMembersFilters"
  constructor(  private http: HttpClient) { }
  
  headers = new HttpHeaders({ 'Content-Type': 'application/form-data' });

   
  kriyaMember(body: any){
    return this.http.get(this.kriya_member_url, body)
  };
  tableMetadata(body: any){
    return this.http.get(this.table_metadata_url, body)
  };
  kriyaMembersFilters(body: any){
    return this.http.get(this.kriya_members_filters_url, body)
  };
}
