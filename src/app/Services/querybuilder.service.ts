import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
export interface Districts{
  district_name: string;
  constituenci_name: string;
  id:  number;
}
@Injectable({
  providedIn: 'root'
})
export class QuerybuilderService {
 base_ulr="api/portal/reports"
 reasons:any;
//   kriya_members_filters_url="https://nivedika.janasenaparty.org/admin/kriyaMembersFilters"
// master_data_url ="https://janasenaparty.org/api/masterdata"
// new apis
get_filters_url = `${this.base_ulr}/getFilters/`

// get_filters_url="http://139.59.40.239/api/portal/reports/getFilters/"
headers = {headers: new HttpHeaders({ 'Content-Type': 'application/json'})};

  constructor( private http:HttpClient) { }

  // kriyaMembersFilters(body: any){
  //   return this.http.get(this.kriya_members_filters_url, body)
  // };
  // masterDataUrl(body:any){
  //   return this.http.get(this.master_data_url,body)
  // }

 
  getFiltersUrl(reportId:any){
    let url = this.get_filters_url+reportId
    return this.http.get(url)
  }
  public getJSON(): Observable<any> {
    return this.http.get("./assets/dist.json");
}
public getDristrictsOnly(): Observable<any> {
  return this.http.get("./assets/districts_only.json");
}
public getConstituencies(): Observable<any> {
  return this.http.get("./assets/constituencies_district.json");
}
public getMandals(): Observable<any> {
  return this.http.get("./assets/districts_mandals.json");
}
public getMETAJSON(): Observable<any> {
  return this.http.get("./assets/metadata.json");
}
getLeaders(): Observable<any> {
  return this.http.get("./api/portal/admin/get_vms_leaders");
}
getReasons(): Observable<any> {
  return this.http.get("./api/portal/admin/get_vms_reasons");
}
getDepartments(): Observable<any> {
  let url = `/api/portal/admin/get_pms_departments`;
  return this.http.get(url);
}

// {
//   this.http.get(`/api/portal/admin/get_vms_leaders`).subscribe((res:any)=>{
//   //   this.leaders= res.serviceResult
//   //  console.log(this.leaders,"leaderrrrrrrrrrrrrrrrrr");
   
//     // this.trservice.userData.next(res.serviceResult)

//     // document.getElementById('previewData1')?.click()
// })
// }
// public getDistrictJson(): Observable<any> {
//   return this.http.get("./assets/districts_only.json");
// }
}
