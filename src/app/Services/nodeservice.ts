import { Injectable } from '@angular/core';

@Injectable()
export class NodeService {
    getTreeNodesData() {
        return [
          {
            "label": "District Committee",
            "expanded": false,
            "children": [
              {
                "label": "Vice President",
                "expanded": false,
                "children": [
                  {
                    "label": "Vice President",
                    "expanded": false,
                    "children": [],
                    "type": "person",
                    "styleClass": "p-person",
                    "data": {
                      "name": "Sri Sirigineedi Venkateswara Rao",
                      "title": "Vice President",
                      "avatar": "https://rb.gy/5dd3es",
                      "bg_color": "#90EE90"
                    }
                  },
                  {
                    "label": "Vice President",
                    "expanded": false,
                    "children": [],
                    "type": "person",
                    "styleClass": "p-person",
                    "data": {
                      "name": "Sri Sanaboyena Mallikharjuna Rao",
                      "title": "Vice President",
                      "avatar": "https://rb.gy/5dd3es",
                      "bg_color": "#90EE90"
                    }
                  },
                  {
                    "label": "Vice President",
                    "expanded": false,
                    "children": [],
                    "type": "person",
                    "styleClass": "p-person",
                    "data": {
                      "name": "Smt Sunkara Krishna Veni",
                      "title": "Vice President",
                      "avatar": "https://rb.gy/5dd3es",
                      "bg_color": "#90EE90"
                    }
                  }
                ],
                "type": "role",
                "data": {
                  "bg_color": "#90EE90",
                  "count": 3
                }
              },
              {
                "label": "General Secretary",
                "expanded": false,
                "children": [
                  {
                    "label": "General Secretary",
                    "expanded": false,
                    "children": [],
                    "type": "person",
                    "styleClass": "p-person",
                    "data": {
                      "name": "Sri Srirangu Srinivasa Rao",
                      "title": "General Secretary",
                      "avatar": "https://rb.gy/5dd3es",
                      "bg_color": "#008B8B"
                    }
                  },
                  {
                    "label": "General Secretary",
                    "expanded": false,
                    "children": [],
                    "type": "person",
                    "styleClass": "p-person",
                    "data": {
                      "name": "Sri Sandadi Srinivas (Srinu Babu)",
                      "title": "General Secretary",
                      "avatar": "https://rb.gy/5dd3es",
                      "bg_color": "#008B8B"
                    }
                  },
                  {
                    "label": "General Secretary",
                    "expanded": false,
                    "children": [],
                    "type": "person",
                    "styleClass": "p-person",
                    "data": {
                      "name": "Sri Talatam Naga Venkata Satayanarayana",
                      "title": "General Secretary",
                      "avatar": "https://rb.gy/5dd3es",
                      "bg_color": "#008B8B"
                    }
                  },
                  {
                    "label": "General Secretary",
                    "expanded": false,
                    "children": [],
                    "type": "person",
                    "styleClass": "p-person",
                    "data": {
                      "name": "Sri Sarakula Abbulu",
                      "title": "General Secretary",
                      "avatar": "https://rb.gy/5dd3es",
                      "bg_color": "#008B8B"
                    }
                  },
                  {
                    "label": "General Secretary",
                    "expanded": false,
                    "children": [],
                    "type": "person",
                    "styleClass": "p-person",
                    "data": {
                      "name": "Smt Bodapati Rajeswari",
                      "title": "General Secretary",
                      "avatar": "https://rb.gy/5dd3es",
                      "bg_color": "#008B8B"
                    }
                  },
                  {
                    "label": "General Secretary",
                    "expanded": false,
                    "children": [],
                    "type": "person",
                    "styleClass": "p-person",
                    "data": {
                      "name": "Sri Thalla  Butchi David Jeshon Raju",
                      "title": "General Secretary",
                      "avatar": "https://rb.gy/5dd3es",
                      "bg_color": "#008B8B"
                    }
                  },
                  {
                    "label": "Secretary",
                    "expanded": false,
                    "children": [
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Smt Shaik Ameena",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Madda Chanti Babu",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Chikkala Satyanarayana",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Donga Venkata Subbarao",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Sangeeta Subhash",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Sampathi Satyanarayana Murthy (Sampath)",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Tejomurtula Narasimha Murthy",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Velamarthi Chitti Brahamam",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Bunga Raju",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Myreddi Gangadharam",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Degala Venkata Satish",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Jami V V Satayanarayana",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Beera Prakash",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Atla Satyanarayana",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Sodhi  Musalayya",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Mogili Apparao",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Pitta Janaki Ramarao",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Buddireddi Srinivas",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Nallala Ramakirshna",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Bandaru Sambha Sivarao",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Vangalapudi Nagendra",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Bandi Yesu Babu",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Kommisetti Naga Suri Babu",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Secretary",
                        "expanded": false,
                        "children": [],
                        "type": "person",
                        "styleClass": "p-person",
                        "data": {
                          "name": "Sri Gundabattula Tataji",
                          "title": "Secretary",
                          "avatar": "https://rb.gy/5dd3es",
                          "bg_color": "#40E0D0"
                        }
                      },
                      {
                        "label": "Joint Secretary",
                        "expanded": false,
                        "children": [
                          {
                            "label": "Joint Secretary",
                            "expanded": false,
                            "children": [],
                            "type": "person",
                            "styleClass": "p-person",
                            "data": {
                              "name": "Sri Chikkam Draksharam Bhimeswara Rao",
                              "title": "Joint Secretary",
                              "avatar": "https://rb.gy/5dd3es",
                              "bg_color": "#5F9EA0"
                            }
                          },
                          {
                            "label": "Joint Secretary",
                            "expanded": false,
                            "children": [],
                            "type": "person",
                            "styleClass": "p-person",
                            "data": {
                              "name": "Sri Gubbala Ravi Kiran",
                              "title": "Joint Secretary",
                              "avatar": "https://rb.gy/5dd3es",
                              "bg_color": "#5F9EA0"
                            }
                          },
                          {
                            "label": "Joint Secretary",
                            "expanded": false,
                            "children": [],
                            "type": "person",
                            "styleClass": "p-person",
                            "data": {
                              "name": "Sri Bhokka Adhi Narayana",
                              "title": "Joint Secretary",
                              "avatar": "https://rb.gy/5dd3es",
                              "bg_color": "#5F9EA0"
                            }
                          },
                          {
                            "label": "Joint Secretary",
                            "expanded": false,
                            "children": [],
                            "type": "person",
                            "styleClass": "p-person",
                            "data": {
                              "name": "Sri Thalluri Prasad",
                              "title": "Joint Secretary",
                              "avatar": "https://rb.gy/5dd3es",
                              "bg_color": "#5F9EA0"
                            }
                          },
                          {
                            "label": "Joint Secretary",
                            "expanded": false,
                            "children": [],
                            "type": "person",
                            "styleClass": "p-person",
                            "data": {
                              "name": "Sri Yalla Venu Gopalarao",
                              "title": "Joint Secretary",
                              "avatar": "https://rb.gy/5dd3es",
                              "bg_color": "#5F9EA0"
                            }
                          },
                          {
                            "label": "Joint Secretary",
                            "expanded": false,
                            "children": [],
                            "type": "person",
                            "styleClass": "p-person",
                            "data": {
                              "name": "Sri Vasamsetti Adhi Sivakumar",
                              "title": "Joint Secretary",
                              "avatar": "https://rb.gy/5dd3es",
                              "bg_color": "#5F9EA0"
                            }
                          },
                          {
                            "label": "Joint Secretary",
                            "expanded": false,
                            "children": [],
                            "type": "person",
                            "styleClass": "p-person",
                            "data": {
                              "name": "Sri Dhalaparthi Srinivas",
                              "title": "Joint Secretary",
                              "avatar": "https://rb.gy/5dd3es",
                              "bg_color": "#5F9EA0"
                            }
                          },
                          {
                            "label": "Joint Secretary",
                            "expanded": false,
                            "children": [],
                            "type": "person",
                            "styleClass": "p-person",
                            "data": {
                              "name": "Sri Namala Satyanarayana",
                              "title": "Joint Secretary",
                              "avatar": "https://rb.gy/5dd3es",
                              "bg_color": "#5F9EA0"
                            }
                          },
                          {
                            "label": "Joint Secretary",
                            "expanded": false,
                            "children": [],
                            "type": "person",
                            "styleClass": "p-person",
                            "data": {
                              "name": "Sri Medisetti Sivaram",
                              "title": "Joint Secretary",
                              "avatar": "https://rb.gy/5dd3es",
                              "bg_color": "#5F9EA0"
                            }
                          },
                          {
                            "label": "Joint Secretary",
                            "expanded": false,
                            "children": [],
                            "type": "person",
                            "styleClass": "p-person",
                            "data": {
                              "name": "Sri Y V D Prasad",
                              "title": "Joint Secretary",
                              "avatar": "https://rb.gy/5dd3es",
                              "bg_color": "#5F9EA0"
                            }
                          },
                          {
                            "label": "Joint Secretary",
                            "expanded": false,
                            "children": [],
                            "type": "person",
                            "styleClass": "p-person",
                            "data": {
                              "name": "Sri Geddam Nagaraju",
                              "title": "Joint Secretary",
                              "avatar": "https://rb.gy/5dd3es",
                              "bg_color": "#5F9EA0"
                            }
                          },
                          {
                            "label": "Joint Secretary",
                            "expanded": false,
                            "children": [],
                            "type": "person",
                            "styleClass": "p-person",
                            "data": {
                              "name": "Sri Bade Venkata Krishna",
                              "title": "Joint Secretary",
                              "avatar": "https://rb.gy/5dd3es",
                              "bg_color": "#5F9EA0"
                            }
                          },
                          {
                            "label": "Joint Secretary",
                            "expanded": false,
                            "children": [],
                            "type": "person",
                            "styleClass": "p-person",
                            "data": {
                              "name": "Sri Ganja Durga Prasad",
                              "title": "Joint Secretary",
                              "avatar": "https://rb.gy/5dd3es",
                              "bg_color": "#5F9EA0"
                            }
                          },
                          {
                            "label": "Joint Secretary",
                            "expanded": false,
                            "children": [],
                            "type": "person",
                            "styleClass": "p-person",
                            "data": {
                              "name": "Sri Chitikela Narayanamurthy",
                              "title": "Joint Secretary",
                              "avatar": "https://rb.gy/5dd3es",
                              "bg_color": "#5F9EA0"
                            }
                          },
                          {
                            "label": "Joint Secretary",
                            "expanded": false,
                            "children": [],
                            "type": "person",
                            "styleClass": "p-person",
                            "data": {
                              "name": "Sri Kanchumarthi Rajanna",
                              "title": "Joint Secretary",
                              "avatar": "https://rb.gy/5dd3es",
                              "bg_color": "#5F9EA0"
                            }
                          },
                          {
                            "label": "Joint Secretary",
                            "expanded": false,
                            "children": [],
                            "type": "person",
                            "styleClass": "p-person",
                            "data": {
                              "name": "Sri Cheekatla Syamkumar",
                              "title": "Joint Secretary",
                              "avatar": "https://rb.gy/5dd3es",
                              "bg_color": "#5F9EA0"
                            }
                          },
                          {
                            "label": "Joint Secretary",
                            "expanded": false,
                            "children": [],
                            "type": "person",
                            "styleClass": "p-person",
                            "data": {
                              "name": "SriSwamina Muni Bhaskar",
                              "title": "Joint Secretary",
                              "avatar": "https://rb.gy/5dd3es",
                              "bg_color": "#5F9EA0"
                            }
                          },
                          {
                            "label": "Joint Secretary",
                            "expanded": false,
                            "children": [],
                            "type": "person",
                            "styleClass": "p-person",
                            "data": {
                              "name": "Sri Dosapati Subbarao",
                              "title": "Joint Secretary",
                              "avatar": "https://rb.gy/5dd3es",
                              "bg_color": "#5F9EA0"
                            }
                          },
                          {
                            "label": "Joint Secretary",
                            "expanded": false,
                            "children": [],
                            "type": "person",
                            "styleClass": "p-person",
                            "data": {
                              "name": "Sri Dasam Sesharao",
                              "title": "Joint Secretary",
                              "avatar": "https://rb.gy/5dd3es",
                              "bg_color": "#5F9EA0"
                            }
                          },
                          {
                            "label": "Joint Secretary",
                            "expanded": false,
                            "children": [],
                            "type": "person",
                            "styleClass": "p-person",
                            "data": {
                              "name": "Sri Palivela Lovaraju",
                              "title": "Joint Secretary",
                              "avatar": "https://rb.gy/5dd3es",
                              "bg_color": "#5F9EA0"
                            }
                          },
                          {
                            "label": "Joint Secretary",
                            "expanded": false,
                            "children": [],
                            "type": "person",
                            "styleClass": "p-person",
                            "data": {
                              "name": "Sri Narsi Yerryya",
                              "title": "Joint Secretary",
                              "avatar": "https://rb.gy/5dd3es",
                              "bg_color": "#5F9EA0"
                            }
                          }
                        ],
                        "type": "role",
                        "data": {
                          "bg_color": "#5F9EA0",
                          "count": 21
                        }
                      }

                    ],
                    "type": "role",
                    "data": {
                      "bg_color": "#40E0D0",
                      "count": 24
                    }
                  }

                ],
                "type": "role",
                "data": {
                  "bg_color": "#008B8B",
                  "count": 6
                }
              }


            ],
            "type": "person",
            "styleClass": "p-person",
            "data": {
              "name": "Sri Kandula Durgesh",
              "title": "President",
              "avatar": "https://live.staticflickr.com/2924/14177675716_c29f06261b_b.jpg",
              "bg_color": "#3CB371"
            }
          },
            // {
            //     key: '1',
            //     label: 'Events',
            //     data: 'Events Folder',
            //     icon: 'pi pi-fw',
            //     children: [
            //         { key: '1-0', label: 'Meeting', icon: 'pi pi-fw' },
            //         { key: '1-1', label: 'Product Launch', icon: 'pi pi-fw' },
            //         { key: '1-2', label: 'Report Review', icon: 'pi pi-fw' }
            //     ]
            // },
            // {
            //     key: '2',
            //     label: 'Movies',
            //     data: 'Movies Folder',
            //     icon: 'pi pi-fw',
            //     children: [
            //         {
            //             key: '2-0',
            //             icon: 'pi pi-fw',
            //             label: 'Al Pacino',
            //             data: 'Pacino Movies',
            //             children: [
            //                 { key: '2-0-0', label: 'Scarface', icon: 'pi pi-fw' },
            //                 { key: '2-0-1', label: 'Serpico', icon: 'pi pi-fw' }
            //             ]
            //         },
            //         {
            //             key: '2-1',
            //             label: 'Robert De Niro',
            //             icon: 'pi pi-fw',
            //             data: 'De Niro Movies',
            //             children: [
            //                 { key: '2-1-0', label: 'Goodfellas', icon: 'pi pi-fw' },
            //                 { key: '2-1-1', label: 'Untouchables', icon: 'pi pi-fw' }
            //             ]
            //         }
            //     ]
            // }
        ];
    }

    getTreeTableNodesData() {
        return [
            {
                key: '0',
                data: {
                    name: 'Applications',
                    size: '100kb',
                    type: 'Folder'
                },
                children: [
                    {
                        key: '0-0',
                        data: {
                            name: 'React',
                            size: '25kb',
                            type: 'Folder'
                        },
                        children: [
                            {
                                key: '0-0-0',
                                data: {
                                    name: 'react.app',
                                    size: '10kb',
                                    type: 'Application'
                                }
                            },
                            {
                                key: '0-0-1',
                                data: {
                                    name: 'native.app',
                                    size: '10kb',
                                    type: 'Application'
                                }
                            },
                            {
                                key: '0-0-2',
                                data: {
                                    name: 'mobile.app',
                                    size: '5kb',
                                    type: 'Application'
                                }
                            }
                        ]
                    },
                    {
                        key: '0-1',
                        data: {
                            name: 'editor.app',
                            size: '25kb',
                            type: 'Application'
                        }
                    },
                    {
                        key: '0-2',
                        data: {
                            name: 'settings.app',
                            size: '50kb',
                            type: 'Application'
                        }
                    }
                ]
            },
            {
                key: '1',
                data: {
                    name: 'Cloud',
                    size: '20kb',
                    type: 'Folder'
                },
                children: [
                    {
                        key: '1-0',
                        data: {
                            name: 'backup-1.zip',
                            size: '10kb',
                            type: 'Zip'
                        }
                    },
                    {
                        key: '1-1',
                        data: {
                            name: 'backup-2.zip',
                            size: '10kb',
                            type: 'Zip'
                        }
                    }
                ]
            },
            {
                key: '2',
                data: {
                    name: 'Desktop',
                    size: '150kb',
                    type: 'Folder'
                },
                children: [
                    {
                        key: '2-0',
                        data: {
                            name: 'note-meeting.txt',
                            size: '50kb',
                            type: 'Text'
                        }
                    },
                    {
                        key: '2-1',
                        data: {
                            name: 'note-todo.txt',
                            size: '100kb',
                            type: 'Text'
                        }
                    }
                ]
            },
            {
                key: '3',
                data: {
                    name: 'Documents',
                    size: '75kb',
                    type: 'Folder'
                },
                children: [
                    {
                        key: '3-0',
                        data: {
                            name: 'Work',
                            size: '55kb',
                            type: 'Folder'
                        },
                        children: [
                            {
                                key: '3-0-0',
                                data: {
                                    name: 'Expenses.doc',
                                    size: '30kb',
                                    type: 'Document'
                                }
                            },
                            {
                                key: '3-0-1',
                                data: {
                                    name: 'Resume.doc',
                                    size: '25kb',
                                    type: 'Resume'
                                }
                            }
                        ]
                    },
                    {
                        key: '3-1',
                        data: {
                            name: 'Home',
                            size: '20kb',
                            type: 'Folder'
                        },
                        children: [
                            {
                                key: '3-1-0',
                                data: {
                                    name: 'Invoices',
                                    size: '20kb',
                                    type: 'Text'
                                }
                            }
                        ]
                    }
                ]
            },
            {
                key: '4',
                data: {
                    name: 'Downloads',
                    size: '25kb',
                    type: 'Folder'
                },
                children: [
                    {
                        key: '4-0',
                        data: {
                            name: 'Spanish',
                            size: '10kb',
                            type: 'Folder'
                        },
                        children: [
                            {
                                key: '4-0-0',
                                data: {
                                    name: 'tutorial-a1.txt',
                                    size: '5kb',
                                    type: 'Text'
                                }
                            },
                            {
                                key: '4-0-1',
                                data: {
                                    name: 'tutorial-a2.txt',
                                    size: '5kb',
                                    type: 'Text'
                                }
                            }
                        ]
                    },
                    {
                        key: '4-1',
                        data: {
                            name: 'Travel',
                            size: '15kb',
                            type: 'Text'
                        },
                        children: [
                            {
                                key: '4-1-0',
                                data: {
                                    name: 'Hotel.pdf',
                                    size: '10kb',
                                    type: 'PDF'
                                }
                            },
                            {
                                key: '4-1-1',
                                data: {
                                    name: 'Flight.pdf',
                                    size: '5kb',
                                    type: 'PDF'
                                }
                            }
                        ]
                    }
                ]
            },
            {
                key: '5',
                data: {
                    name: 'Main',
                    size: '50kb',
                    type: 'Folder'
                },
                children: [
                    {
                        key: '5-0',
                        data: {
                            name: 'bin',
                            size: '50kb',
                            type: 'Link'
                        }
                    },
                    {
                        key: '5-1',
                        data: {
                            name: 'etc',
                            size: '100kb',
                            type: 'Link'
                        }
                    },
                    {
                        key: '5-2',
                        data: {
                            name: 'var',
                            size: '100kb',
                            type: 'Link'
                        }
                    }
                ]
            },
            {
                key: '6',
                data: {
                    name: 'Other',
                    size: '5kb',
                    type: 'Folder'
                },
                children: [
                    {
                        key: '6-0',
                        data: {
                            name: 'todo.txt',
                            size: '3kb',
                            type: 'Text'
                        }
                    },
                    {
                        key: '6-1',
                        data: {
                            name: 'logo.png',
                            size: '2kb',
                            type: 'Picture'
                        }
                    }
                ]
            },
            {
                key: '7',
                data: {
                    name: 'Pictures',
                    size: '150kb',
                    type: 'Folder'
                },
                children: [
                    {
                        key: '7-0',
                        data: {
                            name: 'barcelona.jpg',
                            size: '90kb',
                            type: 'Picture'
                        }
                    },
                    {
                        key: '7-1',
                        data: {
                            name: 'primeng.png',
                            size: '30kb',
                            type: 'Picture'
                        }
                    },
                    {
                        key: '7-2',
                        data: {
                            name: 'prime.jpg',
                            size: '30kb',
                            type: 'Picture'
                        }
                    }
                ]
            },
            {
                key: '8',
                data: {
                    name: 'Videos',
                    size: '1500kb',
                    type: 'Folder'
                },
                children: [
                    {
                        key: '8-0',
                        data: {
                            name: 'primefaces.mkv',
                            size: '1000kb',
                            type: 'Video'
                        }
                    },
                    {
                        key: '8-1',
                        data: {
                            name: 'intro.avi',
                            size: '500kb',
                            type: 'Video'
                        }
                    }
                ]
            }
        ];
    }

    getLazyNodesData() {
        return [
            {
                "label": "Lazy Node 0",
                "data": "Node 0",
                "expandedIcon": "pi pi-folder-open",
                "collapsedIcon": "pi pi-folder",
                "leaf": false
            },
            {
                "label": "Lazy Node 1",
                "data": "Node 1",
                "expandedIcon": "pi pi-folder-open",
                "collapsedIcon": "pi pi-folder",
                "leaf": false
            },
            {
                "label": "Lazy Node 1",
                "data": "Node 2",
                "expandedIcon": "pi pi-folder-open",
                "collapsedIcon": "pi pi-folder",
                "leaf": false
            }
        ]
    }

    getFileSystemNodesData() {
        return [
            {
                "data":{
                    "name":"Applications",
                    "size":"200mb",
                    "type":"Folder"
                },
                "children":[
                    {
                        "data":{
                            "name":"Angular",
                            "size":"25mb",
                            "type":"Folder"
                        },
                        "children":[
                            {
                                "data":{
                                    "name":"angular.app",
                                    "size":"10mb",
                                    "type":"Application"
                                }
                            },
                            {
                                "data":{
                                    "name":"cli.app",
                                    "size":"10mb",
                                    "type":"Application"
                                }
                            },
                            {
                                "data":{
                                    "name":"mobile.app",
                                    "size":"5mb",
                                    "type":"Application"
                                }
                            }
                        ]
                    },
                    {
                        "data":{
                            "name":"editor.app",
                            "size":"25mb",
                            "type":"Application"
                        }
                    },
                    {
                        "data":{
                            "name":"settings.app",
                            "size":"50mb",
                            "type":"Application"
                        }
                    }
                ]
            },
            {
                "data":{
                    "name":"Cloud",
                    "size":"20mb",
                    "type":"Folder"
                },
                "children":[
                    {
                        "data":{
                            "name":"backup-1.zip",
                            "size":"10mb",
                            "type":"Zip"
                        }
                    },
                    {
                        "data":{
                            "name":"backup-2.zip",
                            "size":"10mb",
                            "type":"Zip"
                        }
                    }
                ]
            },
            {
                "data": {
                    "name":"Desktop",
                    "size":"150kb",
                    "type":"Folder"
                },
                "children":[
                    {
                        "data":{
                            "name":"note-meeting.txt",
                            "size":"50kb",
                            "type":"Text"
                        }
                    },
                    {
                        "data":{
                            "name":"note-todo.txt",
                            "size":"100kb",
                            "type":"Text"
                        }
                    }
                ]
            },
            {
                "data":{
                    "name":"Documents",
                    "size":"75kb",
                    "type":"Folder"
                },
                "children":[
                    {
                        "data":{
                            "name":"Work",
                            "size":"55kb",
                            "type":"Folder"
                        },
                        "children":[
                            {
                                "data":{
                                    "name":"Expenses.doc",
                                    "size":"30kb",
                                    "type":"Document"
                                }
                            },
                            {
                                "data":{
                                    "name":"Resume.doc",
                                    "size":"25kb",
                                    "type":"Resume"
                                }
                            }
                        ]
                    },
                    {
                        "data":{
                            "name":"Home",
                            "size":"20kb",
                            "type":"Folder"
                        },
                        "children":[
                            {
                                "data":{
                                    "name":"Invoices",
                                    "size":"20kb",
                                    "type":"Text"
                                }
                            }
                        ]
                    }
                ]
            },
            {
                "data": {
                    "name":"Downloads",
                    "size":"25mb",
                    "type":"Folder"
                },
                "children":[
                    {
                        "data": {
                            "name":"Spanish",
                            "size":"10mb",
                            "type":"Folder"
                        },
                        "children":[
                            {
                                "data":{
                                    "name":"tutorial-a1.txt",
                                    "size":"5mb",
                                    "type":"Text"
                                }
                            },
                            {
                                "data":{
                                    "name":"tutorial-a2.txt",
                                    "size":"5mb",
                                    "type":"Text"
                                }
                            }
                        ]
                    },
                    {
                        "data":{
                            "name":"Travel",
                            "size":"15mb",
                            "type":"Text"
                        },
                        "children":[
                            {
                                "data":{
                                    "name":"Hotel.pdf",
                                    "size":"10mb",
                                    "type":"PDF"
                                }
                            },
                            {
                                "data":{
                                    "name":"Flight.pdf",
                                    "size":"5mb",
                                    "type":"PDF"
                                }
                            }
                        ]
                    }
                ]
            },
            {
                "data": {
                    "name":"Main",
                    "size":"50mb",
                    "type":"Folder"
                },
                "children":[
                    {
                        "data":{
                            "name":"bin",
                            "size":"50kb",
                            "type":"Link"
                        }
                    },
                    {
                        "data":{
                            "name":"etc",
                            "size":"100kb",
                            "type":"Link"
                        }
                    },
                    {
                        "data":{
                            "name":"var",
                            "size":"100kb",
                            "type":"Link"
                        }
                    }
                ]
            },
            {
                "data":{
                    "name":"Other",
                    "size":"5mb",
                    "type":"Folder"
                },
                "children":[
                    {
                        "data":{
                            "name":"todo.txt",
                            "size":"3mb",
                            "type":"Text"
                        }
                    },
                    {
                        "data":{
                            "name":"logo.png",
                            "size":"2mb",
                            "type":"Picture"
                        }
                    }
                ]
            },
            {
                "data":{
                    "name":"Pictures",
                    "size":"150kb",
                    "type":"Folder"
                },
                "children":[
                    {
                        "data":{
                            "name":"barcelona.jpg",
                            "size":"90kb",
                            "type":"Picture"
                        }
                    },
                    {
                        "data":{
                            "name":"primeng.png",
                            "size":"30kb",
                            "type":"Picture"
                        }
                    },
                    {
                        "data":{
                            "name":"prime.jpg",
                            "size":"30kb",
                            "type":"Picture"
                        }
                    }
                ]
            },
            {
                "data":{
                    "name":"Videos",
                    "size":"1500mb",
                    "type":"Folder"
                },
                "children":[
                    {
                        "data":{
                            "name":"primefaces.mkv",
                            "size":"1000mb",
                            "type":"Video"
                        }
                    },
                    {
                        "data":{
                            "name":"intro.avi",
                            "size":"500mb",
                            "type":"Video"
                        }
                    }
                ]
            }
        ]
    }

    getTreeTableNodes() {
        return Promise.resolve(this.getTreeTableNodesData());
    }

    getTreeNodes() {
        return Promise.resolve(this.getTreeNodesData());
    }

    getFiles() {
        return Promise.resolve(this.getTreeNodesData());
    }

    getLazyFiles() {
        return Promise.resolve(this.getLazyNodesData());
    }

    getFilesystem() {
        return Promise.resolve(this.getFileSystemNodesData());
    }

};
