import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChartsService {
  public chartType: string = '';

  // charts_url=`${environment.base_ulr}/api/portal/admin/analytics/`;
  charts_url='api/portal/admin/analytics/';

  constructor(private http: HttpClient) { }


  getCharts(reportId:any){
    let url = this.charts_url+reportId;
    return this.http.get(url)
  }
}
