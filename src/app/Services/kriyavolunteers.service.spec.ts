import { TestBed } from '@angular/core/testing';

import { KriyavolunteersService } from './kriyavolunteers.service';

describe('KriyavolunteersService', () => {
  let service: KriyavolunteersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KriyavolunteersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
