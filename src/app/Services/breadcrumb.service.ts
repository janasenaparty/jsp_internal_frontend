import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Data, NavigationEnd, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';

import { Breadcrumb } from '../model/breadcrumb.model';

@Injectable({
  providedIn: 'root'
})
export class BreadcrumbService {

  // Subject emitting the breadcrumb hierarchy
  private readonly _breadcrumbs$ = new BehaviorSubject<Breadcrumb[]>([]);

  // Observable exposing the breadcrumb hierarchy
  readonly breadcrumbs$ = this._breadcrumbs$.asObservable();

  constructor(private router: Router) {
    this.router.events.pipe(
      // Filter the NavigationEnd events as the breadcrumb is updated only when the route reaches its end
      filter((event) => event instanceof NavigationEnd)
    ).subscribe(event => {
      // Construct the breadcrumb hierarchy
      const urlSegments = this.router.routerState.snapshot.url.split("/").filter(f => f);
      let parentUrl: string[] = [];
      const breadcrumbs: Breadcrumb[] = [];
      urlSegments.map(urlSegment => {
        const routeUrl = parentUrl.concat(urlSegment);
        //this.addBreadcrumb(urlSegment, [], breadcrumbs);
          const breadcrumb = {
            label: urlSegment,
            url: '/' + routeUrl.join('/')
          };
          parentUrl.push(urlSegment);
          breadcrumbs.push(breadcrumb);
      });

      // Emit the new hierarchy
      this._breadcrumbs$.next(breadcrumbs);
    });
  }
}
