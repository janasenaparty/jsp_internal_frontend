import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class KriyavolunteersService {
  volunteerData= new Subject();

  // kriya_volunteer_url="https://nivedika.janasenaparty.org/get_kriya_volunteers"

  constructor(  private http: HttpClient) { }
headers = {headers: new HttpHeaders({ 'Content-Type': 'application/json'})};
  
  // headers = new HttpHeaders({ 'Content-Type': 'application/form-data' });
  SendLink( id:any){
    let url = `/api/portal/admin/send_kriya_volunteer_app`;
    id = id.toString();
    const finalBody={"id":id}
  console.log(finalBody);
    return this.http.post(url,finalBody,this.headers)
  }

  Activate( id:any,status:any){
    let url = `/api/portal/admin/activate_deactivate_kriya_volunteer`;
    // id = id.toString();
    const finalBody={"id":id,"status":status}
  console.log(finalBody);
    return this.http.post(url,finalBody,this.headers)
  }
  Suspend( id:any,status:any){
    let url = `/api/portal/admin/activate_deactivate_kriya_volunteer`;
    // id = id.toString();
    const finalBody={"id":id,"status":status}
  console.log(finalBody);
    return this.http.post(url,finalBody,this.headers)
  }
  addVolunteer( requestBody:any): Observable<any>{
    let url = `/api/portal/admin/addkriyavolunteer`;
    requestBody.mobile = String(requestBody.mobile);
    requestBody.name = String(requestBody.name);
    requestBody.gender = String(requestBody.gender);
    requestBody.constituency_id = Number(requestBody.constituency_id);
    requestBody.district_id = Number(requestBody.district_id);
    // requestBody.mandal_id = Number(requestBody.mandal_id);
    requestBody.state = Number(requestBody.state);

    // voter_id = id.voter_id();
  //    requestBody.files =[
  //     "https://janasenabackup.blob.core.windows.net/kriyamembers/XVE1LX2KCRV9C8CNLXDHWYS169SEAAN120220627-184539.png"
  // ];
     
    return this.http.post(url,requestBody,this.headers)
  }
  getVolunteer(id:number): Observable<any>{
    return this.http.get(`/api/portal/admin/get_kriya_volunteer_details?id=${id}`);
   }

   updateVoluneer( requestBody:any,id:number): Observable<any>{
    let url = `/api/portal/admin/update_kriya_volunteer_details`;
    //  requestBody.title =requestBody.subject;
     requestBody.id =id;
    // requestBody.constituency_id = Number(requestBody.constituency_id);
    // requestBody.district_id = Number(requestBody.district_id);
    // requestBody.mandal = Number(requestBody.mandal);
    
    // voter_id = id.voter_id();
  //    requestBody.files =[
  //     "https://janasenabackup.blob.core.windows.net/kriyamembers/XVE1LX2KCRV9C8CNLXDHWYS169SEAAN120220627-184539.png"
  // ];
     
    return this.http.post(url,requestBody,this.headers)
  }
  addBulkPayment(requestBody:any){
   
    let headers = new HttpHeaders({'Content-Type': 'multipart/form-data'});
    /** In Angular 5, including the header Content-Type can invalidate your request */
    // headers.set('Content-Type', 'multipart/form-data');
   // headers.append('Accept', 'application/json');
    let options =  { headers: headers };
    const url = `/api/portal/admin/bulk_payments`;
    return this.http.post(url,requestBody)
  }
  // VoterDb(id:any,status:any){
  //   let url = `/api/portal/admin/get_kriya_member_mapped_voters`;
  //   id = id.toString();
  //   const finalBody={"id":id}
  // console.log(finalBody);
  //   return this.http.post(url,finalBody,this.headers) 
  // }
  // kriyavolunteer(body: any){/api/portal/admin/activate_deactivate_kriya_volunteer
  //   return this.http.get(this.kriya_volunteer_url, body)
    
  // };
}
