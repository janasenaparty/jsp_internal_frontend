import { TestBed } from '@angular/core/testing';

import { QuerybuilderService } from './querybuilder.service';

describe('QuerybuilderService', () => {
  let service: QuerybuilderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuerybuilderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
