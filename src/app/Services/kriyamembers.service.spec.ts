import { TestBed } from '@angular/core/testing';

import { KriyamembersService } from './kriyamembers.service';

describe('KriyamembersService', () => {
  let service: KriyamembersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KriyamembersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
